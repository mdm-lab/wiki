#!/bin/bash

DATA_DIR=../../../data
CONTENT_DIR=../../../content
PUBLIC_DIR=../../../public


# system operon structure
df-wiki-cli meilisearch delete-all-documents systemoperonstruct
df-wiki-cli content system-operon-structure --structure  ${DATA_DIR}/all_predictions_statistics.tsv --output ${DATA_DIR}/system-structures.csv
df-wiki-cli meilisearch update system-operon-structure --file ${DATA_DIR}/system-structures.csv


# REFSEQ
df-wiki-cli meilisearch delete-all-documents refseq
df-wiki-cli meilisearch update refseq --file ${DATA_DIR}/refseq_res.csv


# REF SEQ TAXO
df-wiki-cli content refseq-group-per-assembly --input ${DATA_DIR}/refseq_res.csv --output /tmp/refseqtaxo.csv
df-wiki-cli meilisearch delete-all-documents refseqtaxo
df-wiki-cli meilisearch update refseqtaxo --file /tmp/refseqtaxo.csv

# REFSEQ TAXO TYPE

df-wiki-cli content refseq-group-per-assembly-and-type --input ${DATA_DIR}/refseq_res.csv --output /tmp/refseqtaxotype.csv
df-wiki-cli meilisearch delete-all-documents refseqtaxotype
df-wiki-cli meilisearch update refseqtaxotype --file /tmp/refseqtaxotype.csv


# SANITIZED REFSEQ
df-wiki-cli content refseq-sanitized-hits --input ${DATA_DIR}/refseq_res.csv --output /tmp/refseq-sanitized.csv

df-wiki-cli meilisearch delete-all-documents refseqsanitized
df-wiki-cli meilisearch update refseqsanitized --file /tmp/refseq-sanitized.csv

# systems
df-wiki-cli content systems --dir ${CONTENT_DIR}/3.defense-systems/ --pfam ${PUBLIC_DIR}/pfam-a-hmm.csv --output /tmp/list-systems.json
df-wiki-cli meilisearch update systems --file /tmp/list-systems.json

# STRUCTURE 
df-wiki-cli meilisearch delete-all-documents structure
df-wiki-cli meilisearch update structure --file ${DATA_DIR}/all_predictions_statistics.tsv

# ARTICLES
# df-wiki-cli meilisearch delete-all-documents article
# df-wiki-cli meilisearch

