import json
from pyzotero import zotero
from pathlib import Path
from habanero import Crossref
from rich.console import Console

console = Console()


def fetch_articles_from_zotero(
    key: str, batch_size: int = 100, output: Path = "articles.json"
):
    zot = zotero.Zotero("5151022", "group", key)
    collection = zot.collection("BSWL96X3")
    tot_items = collection["meta"]["numItems"]

    def gen_fetch(tot_items, batch_size):
        starts = range(0, tot_items, batch_size)
        for i in starts:
            items = zot.collection_items(
                "BSWL96X3",
                format="csljson",
                limit=batch_size,
                start=i,
                itemType="journalArticle || Preprint",
                sort="title",
            )["items"]
            for item in items:
                yield item

    items = list(gen_fetch(tot_items, batch_size))
    json_object = json.dumps(items, indent=2)
    with open(output, "w") as outfile:
        outfile.write(json_object)


def add_articles_to_zotero_from_doi(doi, key):
    cr = Crossref(mailto="defense-finder@pasteur.fr")
    res = cr.works(ids=[doi])

    zot = zotero.Zotero("5151022", "group", key)

    # pyzotero grabs the template dict from the server

    # console.print(zitem)
    message = res["message"]
    console.print(f"add doi {doi}, type {message['type']}")
    if message["type"] == "posted-content":
        # {
        #     "itemType": "Preprint",
        #     "title": "",
        #     "creators": [{"creatorType": "author", "firstName": "", "lastName": ""}],
        #     "abstractNote": "",
        #     "genre": "",
        #     "repository": "",
        #     "archiveID": "",
        #     "place": "",
        #     "date": "",
        #     "series": "",
        #     "seriesNumber": "",
        #     "DOI": "",
        #     "citationKey": "",
        #     "url": "",
        #     "accessDate": "",
        #     "archive": "",
        #     "archiveLocation": "",
        #     "shortTitle": "",
        #     "language": "",
        #     "libraryCatalog": "",
        #     "callNumber": "",
        #     "rights": "",
        #     "extra": "",
        #     "tags": [],
        #     "collections": [],
        #     "relations": {},
        # }
        itemtype = "Preprint"
    elif message["type"] == "journal-article":
        #     {
        #     'itemType': 'journalArticle',
        #     'title': '',
        #     'creators': [{'creatorType': 'author', 'firstName': '', 'lastName': ''}],
        #     'abstractNote': '',
        #     'publicationTitle': '',
        #     'volume': '',
        #     'issue': '',
        #     'pages': '',
        #     'date': '',
        #     'series': '',
        #     'seriesTitle': '',
        #     'seriesText': '',
        #     'journalAbbreviation': '',
        #     'language': '',
        #     'DOI': '',
        #     'ISSN': '',
        #     'shortTitle': '',
        #     'url': '',
        #     'accessDate': '',
        #     'archive': '',
        #     'archiveLocation': '',
        #     'libraryCatalog': '',
        #     'callNumber': '',
        #     'rights': '',
        #     'extra': '',
        #     'tags': [],
        #     'collections': [],
        #     'relations': {}
        # }
        itemtype = "journalArticle"
    else:
        raise NotImplementedError(f"type {message['type']} need to be implemented")
    zitem = zot.item_template(itemtype)
    # console.print(message)

    # console.print(message["title"])
    zitem["title"] = message["title"][0]
    if "page" in message:
        zitem["pages"] = message["page"]
    if "abstract" in message:
        zitem["abstractNote"] = message["abstract"]

    if "container-title" in message and len(message["container-title"]) > 0:
        zitem["publicationTitle"] = message["container-title"][0]
    if "short-container-title" in message and len(message["short-container-title"]) > 0:
        zitem["journalAbbreviation"] = message["short-container-title"][0]
    zitem["creators"] = [
        {
            "creatorType": "author",
            "firstName": author["given"],
            "lastName": author["family"],
        }
        for author in message["author"]
    ]
    zitem["libraryCatalog"] = "DOI.org (Crossref)"
    if "ISSN" in message:
        zitem["ISSN"] = ", ".join(message["ISSN"])
    zitem["url"] = message["resource"]["primary"]["URL"]
    zitem["date"] = "/".join([str(d) for d in message["published"]["date-parts"][0]])
    for key in ["DOI", "volume", "issue", "language"]:
        if key in message:
            zitem[key] = message[key]

    items_to_add = [zitem]

    zot.check_items(items_to_add)
    # res = zot.create_items(items_to_add)
    # new_item = res["successful"]["0"]
    # zot.addto_collection("BSWL96X3", new_item)
    console.print("[green]done")
