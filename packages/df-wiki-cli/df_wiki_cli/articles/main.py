import typer
from typing_extensions import Annotated
from pathlib import Path
from df_wiki_cli.articles import (
    add_articles_to_zotero_from_doi,
    fetch_articles_from_zotero,
)
from pyzotero import zotero
from habanero import Crossref
from rich.console import Console
import frontmatter
import re
import json

app = typer.Typer()
console = Console()


@app.command()
def fetch_from_zotero(
    output: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=True,
        ),
    ] = "articles.json",
    key: Annotated[str, typer.Option(help="Zotero api key")] = "",
    batch_size: Annotated[
        int, typer.Option(help="Number articles get per request")
    ] = 100,
):
    """
    Get articles metadata from Zotero collection

    """
    if key != "":
        fetch_articles_from_zotero(key, batch_size, output)
    else:
        print("You must provide a zotero api key")
        raise typer.Exit(code=1)


@app.command()
def missing_doi(
    dir: Annotated[
        Path,
        typer.Option(exists=False, file_okay=False, readable=True, dir_okay=True),
    ],
    key: Annotated[str, typer.Option(help="Zotero api key")] = "",
    batch_size: Annotated[
        int, typer.Option(help="Number articles get per request")
    ] = 100,
):
    # parse content to look at dois

    # get current list of dois in zotero
    zotero_list = "/tmp/zotero-dois.json"
    fetch_articles_from_zotero(key, batch_size, zotero_list)
    zotero_dois_set = set()
    with open(zotero_list) as zotero_f:
        zotero_data = json.load(zotero_f)
        for d in zotero_data:
            if "DOI" in d:
                doi_lower = d["DOI"].lower()
                zotero_dois_set.add(doi_lower)

    dois_set = set()
    for file in dir.rglob("*"):
        if file.suffix == ".md":
            console.rule(f"[bold blue]{file.name}", style="blue")
            with open(file) as f:
                metadata, content = frontmatter.parse(f.read())
                if (
                    "relevantAbstracts" in metadata
                    and len(metadata["relevantAbstracts"]) > 0
                ):
                    dois = [d["doi"].lower() for d in metadata["relevantAbstracts"]]
                    dois_set.update(dois)

                # handle content
                group = re.findall(r":ref{doi=(.*?)}", content)
                for g in group:
                    splitted = [doi.lower() for doi in re.split(",", g)]
                    dois_set.update(splitted)

    for doi in dois_set - zotero_dois_set:
        add_articles_to_zotero_from_doi(doi, key)


@app.command()
def from_apicrossref(
    doi: Annotated[str, typer.Option(help="DOI identifier")],
    key: Annotated[str, typer.Option(help="Zotero api key")] = "",
):

    add_articles_to_zotero_from_doi(doi, key)
