import typer
import re
from rich.console import Console
import meilisearch
from pathlib import Path
import csv
import json
from typing import Annotated, List, Optional
from pydantic import BaseModel, Field, BeforeValidator
from enum import Enum

console = Console()

app = typer.Typer()


def emptyStringToNone(val: str) -> None | int:
    if val == "":
        return None
    return int(float(val))


class RefSeqCsv(BaseModel):
    sys_id: str
    Assembly: str
    replicon: str
    type: str
    subtype: str
    sys_beg: str
    sys_end: str
    protein_in_syst: Optional[List[str]]
    genes_count: Annotated[int | None, BeforeValidator(emptyStringToNone)]
    name_of_profiles_in_sys: Optional[List[str]]
    accession_in_sys: Optional[List[str]]
    Superkingdom: str
    phylum: str
    class_: str = Field(..., alias="class")
    order: str
    family: str
    genus: str
    species: str


class RefSeqTaxo(BaseModel):
    index: int
    Assembly: str
    Superkingdom: str
    phylum: str
    class_: str = Field(..., alias="class")
    order: str
    family: str
    genus: str
    species: str
    size: int


class RefSeqTaxoType(RefSeqTaxo):
    type: str


class RefSeqTypeCount(BaseModel):
    type: str
    size: int


class StructureTypes(str, Enum):
    Validated = "Validated"
    DF = "DF"
    na = "na"


def na_to_none(v: Optional[float]) -> Optional[float]:
    if v == "na":
        return None
    else:
        return v


NaFloat = Annotated[Optional[float], BeforeValidator(na_to_none)]


class StrucutreStatistics(BaseModel):
    id: int
    structure_system: str
    system: str
    subsystem: str
    proteins_in_the_prediction: Optional[List[str]]
    prediction_type: str
    batch: int
    nb_sys: str
    type: StructureTypes
    system_number_of_genes: int
    system_genes: Optional[List[str]]
    pdb: str
    pae_table: str
    plddt_table: str
    fasta_file: str
    completed: bool
    iptm_ptm: NaFloat = Field(..., alias="iptm+ptm")
    pDockQ: Optional[NaFloat]
    plddts: Optional[NaFloat]
    Abi2: str
    OrbA: str
    Anti_BREX: str


class SystemOperonStructure(BaseModel):
    id: int
    system: str
    subsystem: str
    version: str
    gene: str
    size: int
    exchangeables: Optional[List[str]]


@app.command()
def refseqtaxo(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "refseqtaxo"
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            doc = RefSeqTaxo(**row)
            documents.append(doc.model_dump(by_alias=True))
    tasks = index.add_documents_in_batches(documents, primary_key="Assembly")
    for task in tasks:
        console.print(task)
    index.update_pagination_settings({"maxTotalHits": 1000000})
    index.update_filterable_attributes(
        body=[
            "Superkingdom",
            "phylum",
            "class",
            "order",
            "family",
            "genus",
            "species",
            "Assembly",
        ]
    )
    index.update_sortable_attributes(
        [
            "Superkingdom",
            "phylum",
            "class",
            "order",
            "family",
            "genus",
            "species",
            "Assembly",
            "size",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)


@app.command()
def refseqtaxotype(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "refseqtaxotype"
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            doc = RefSeqTaxoType(**row)
            documents.append(doc.model_dump(by_alias=True))
    tasks = index.add_documents_in_batches(documents, primary_key="index")
    for task in tasks:
        console.print(task)
    index.update_pagination_settings({"maxTotalHits": 1000000})
    index.update_filterable_attributes(
        body=[
            "Superkingdom",
            "phylum",
            "class",
            "order",
            "family",
            "genus",
            "species",
            "Assembly",
        ]
    )
    index.update_sortable_attributes(
        [
            "Superkingdom",
            "phylum",
            "class",
            "order",
            "family",
            "genus",
            "species",
            "Assembly",
            "type",
            "size",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)


@app.command()
def refseqtypecount(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "refseqtypecount"
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            doc = RefSeqTypeCount(**row)
            documents.append(doc.model_dump(by_alias=True))
    tasks = index.add_documents_in_batches(documents, primary_key="type")
    for task in tasks:
        console.print(task)
    index.update_pagination_settings({"maxTotalHits": 1000000})
    index.update_filterable_attributes(body=["type"])
    index.update_sortable_attributes(
        [
            "type",
            "size",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)


@app.command()
def refseq(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "refseq"
    update_refseq(client, document, file)


@app.command()
def refseqsanitized(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "refseqsanitized"
    update_refseq(client, document, file)


@app.command()
def structure(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "structure"
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter="\t")
        for id, row in enumerate(csvreader):
            row["proteins_in_the_prediction"] = split_on_comma(
                row["proteins_in_the_prediction"]
            )
            row["system_genes"] = split_on_comma(row["system_genes"])
            doc = StrucutreStatistics(
                **row,
                id=id,
            )
            # console.print(doc)

            documents.append(doc.model_dump(by_alias=True))
        tasks = index.add_documents_in_batches(documents, primary_key="id")
        for task in tasks:
            print(task)
    pagination_settings_task = index.update_pagination_settings(
        {"maxTotalHits": 100000}
    )
    print(pagination_settings_task)
    attr_task = index.update_filterable_attributes(
        body=[
            "system",
            "subsystem",
            "completed",
            "prediction_type",
            "plddts",
            "iptm+ptm",
            "proteins_in_the_prediction",
            "system_genes",
            "pDockQ",
            "Abi2",
            "OrbA",
            "Anti_BREX",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)

    print(attr_task)
    index.update_sortable_attributes(
        [
            "system",
            "subsystem",
            "completed",
            "plddts",
            "nb_sys",
            "completed",
            "prediction_type",
            "system_number_of_genes",
            "iptm+ptm",
            "pDockQ",
            "Abi2",
            "OrbA",
            "Anti_BREX",
        ]
    )
    index.update_typo_tolerance({"enabled": False})


@app.command()
def systems(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "systems"
    index = client.index(document.lower())
    with open(file, "r") as jsonfile:
        json_object = json.load(jsonfile)
        tasks = index.add_documents_in_batches(json_object, primary_key="title")
        for task in tasks:
            print(task)
    pagination_settings_task = index.update_pagination_settings(
        {"maxTotalHits": 100000}
    )
    print(pagination_settings_task)
    attr_task = index.update_filterable_attributes(
        body=[
            "title",
            "Sensor",
            "Activator",
            "Effector",
            "PFAM.AC",
            "PFAM.DE",
            "contributors",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)

    print(attr_task)
    index.update_sortable_attributes(["title", "Sensor", "Activator", "Effector"])
    index.update_typo_tolerance({"enabled": False})


@app.command()
def articles(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "article"
    index = client.index(document.lower())
    with open(file, "r") as jsonfile:
        json_object = json.load(jsonfile)
        for obj in json_object:
            obj["ms_id"] = obj["id"].replace("/", "_")
        tasks = index.add_documents_in_batches(json_object, primary_key="ms_id")
        for task in tasks:
            print(task)

    pagination_settings_task = index.update_pagination_settings(
        {"maxTotalHits": 100000}
    )
    print(pagination_settings_task)
    attr_task = index.update_filterable_attributes(
        body=[
            "DOI",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)

    print(attr_task)


@app.command()
def system_operon_structure(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=False,
        ),
    ],
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    document = "systemoperonstruct"
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            row["exchangeables"] = split_on_comma(row["exchangeables"])
            doc = SystemOperonStructure(**row)
            documents.append(doc.model_dump(by_alias=True))
    tasks = index.add_documents_in_batches(documents, primary_key="id")
    for task in tasks:
        console.print(task)
    index.update_pagination_settings({"maxTotalHits": 1000000})
    index.update_filterable_attributes(
        body=[
            "system",
            "subsystem",
            "gene",
            "exchangeables",
        ]
    )
    index.update_sortable_attributes(
        [
            "system",
            "subsystem",
            "gene",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)


def split_on_comma(str_val: Optional[str]) -> Optional[List[str]]:
    if str_val is not None and str_val != "":
        for val in str_val.split(","):
            yield val.strip()
    else:
        return None


def update_refseq(client, document, file, primary_key="sys_id"):
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            row["protein_in_syst"] = split_on_comma(row["protein_in_syst"])
            row["name_of_profiles_in_sys"] = split_on_comma(
                row["name_of_profiles_in_sys"]
            )
            row["accession_in_sys"] = split_on_comma(row["accession_in_sys"])
            doc = RefSeqCsv(**row)
            documents.append(doc.model_dump(by_alias=True))
        tasks = index.add_documents_in_batches(documents, primary_key=primary_key)
        for task in tasks:
            console.print(task)
    index.update_pagination_settings({"maxTotalHits": 1000000})
    index.update_filterable_attributes(
        body=[
            "replicon",
            "Assembly",
            "type",
            "subtype",
            "Superkingdom",
            "phylum",
            "class",
            "order",
            "family",
            "genus",
            "species",
        ]
    )
    index.update_sortable_attributes(
        [
            "replicon",
            "Assembly",
            "type",
            "subtype",
            "Superkingdom",
            "phylum",
            "class",
            "order",
            "family",
            "genus",
            "species",
        ]
    )
    params = {
        "maxValuesPerFacet": 1000000,
        "sortFacetValuesBy": {"*": "count"},
    }
    index.update_faceting_settings(params)
    index.update_typo_tolerance(
        {
            "enabled": False
            # "minWordSizeForTypos": {"oneTypo": 50, "twoTypos": 100}
        }
    )
