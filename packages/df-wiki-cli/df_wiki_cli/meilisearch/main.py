import typer
import meilisearch
from typing_extensions import Annotated
from pathlib import Path

from df_wiki_cli.meilisearch.update import main as update_main


from enum import Enum
from types import SimpleNamespace
from rich.console import Console


console = Console()

app = typer.Typer()
app.add_typer(update_main.app, name="update")


class Documents(str, Enum):
    refseqtaxo = "refseqtaxo"
    refseqtaxotype = "refseqtaxotype"
    refseqtypecount = "refseqtypecount"
    refseqsanitized = "refseqsanitized"
    refseq = "refseq"
    structure = "structure"
    systems = "systems"
    article = "article"


@app.callback()
def main(
    ctx: typer.Context,
    host: Annotated[
        str, typer.Option(help="Meilisearch host", envvar="MEILI_HOST")
    ] = "http://localhost:7700",
    key: Annotated[
        str, typer.Option(help="Api master key", envvar="MEILI_MASTER_KEY")
    ] = "MASTER_KEY",
):
    """
    Handle meilisearch server.

    commands to manage meilisearch server

    """
    ctx.obj = SimpleNamespace(key=key, host=host)
    # ctx.obj = SimpleNamespace()


@app.command()
def delete_all_documents(ctx: typer.Context, id: str):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    index = client.index(id)
    tasks = index.delete_all_documents()
    console.print(tasks)


@app.command()
def indexes(ctx: typer.Context):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    indexes = client.get_indexes()["results"]
    for index in indexes:
        print(index.uid)


@app.command()
def index_update(ctx: typer.Context, index: str, primary_key: str):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    task = client.index(index).update(primary_key=primary_key)
    console.print(task)


@app.command()
def index_delete(ctx: typer.Context, index: str):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    client.index(index).delete()


@app.command()
def task(ctx: typer.Context, id: str):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    print(client.get_task(id))


@app.command("get-env-var")
def get_env_var(
    ctx: typer.Context,
    output: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=True,
        ),
    ] = "build.env",
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    keys = client.get_keys()

    api_key = [res.key for res in keys.results if res.name == "Default Search API Key"]
    print(ctx.obj.host)
    if len(api_key) == 1:
        with open(output, "a") as outfile:
            outfile.write(f"MEILI_HOST={ctx.obj.host}\n")
            outfile.write(f"MEILI_API_KEY={api_key[0]}\n")
