import typer
from pathlib import Path
from typing_extensions import Annotated
from df_wiki_cli.pfam import fetch_pfam
from df_wiki_cli.meilisearch import main as ms_main
from df_wiki_cli.content import main as content_main
from df_wiki_cli.articles import main as articles_main

# from df_wiki_cli.ms import main as ms_main
app = typer.Typer()
app.add_typer(ms_main.app, name="meilisearch")
app.add_typer(content_main.app, name="content")
app.add_typer(articles_main.app, name="articles")


@app.callback()
def callback():
    """
    Manage Defense Finder Wiki data.

    This is a list of commands to perform admin tasks.

    """

@app.command()
def pfam(
    output: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=True,
        ),
    ]
):
    fetch_pfam(output)
