import { md3 } from 'vuetify/blueprints'
// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  // ssr: false,
  modules: [
    '@unocss/nuxt',
    '@nuxt/content',
    'vuetify-nuxt-module',
    '@vueuse/nuxt',
    '@pinia/nuxt',
    'nuxt-meilisearch',
    '@nuxtjs/plausible',
    '@nuxtjs/seo',
    '@nuxt/eslint',
  ],
  app: {
    head: {
      script: [
        {
          defer: true,
          "data-domaim": "defense-finder.dev.pasteur.cloud",
          src: "https://plausible.pasteur.cloud/js/script.js"
        }
      ]
    }
  },
  router: {
    strict: true
  },
  site: {
    url: 'https://defensefinder.mdmlab.fr',
    name: 'DefenseFinder webservice and knowledge base',
    description: 'On this site, you can freely use (without any login) the DefenseFinder webservice (see below) and get help to navigate the ever expanding world of defense systems.There is a collaborative knowledge base of defense systems that provide essential information on any given defense systems',
    defaultLocale: 'en', // not needed if you have @nuxtjs/i18n installed
  },
  content: {
    documentDriven: {
      injectPage: false,
    },
    highlight: {
      theme: 'github-light',
      preload: [
        'sh',
      ]
    },
  },
  vuetify: {
    vuetifyOptions: {
      labComponents: true,
      icons: {
        defaultSet: 'unocss-mdi',
        sets: ['mdi', 'fa', 'md'],
        unocssIcons: {
          // default is i-mdi:close-circle
          // database: 'i-tabler:database',
          // even from another collection, default is i-mdi:chevron-up
          // generalconcept: 'i-mdi:book-education-outline',
          // help: 'i-tabler:help'
        }
      },
      blueprint: md3


    }
  },
  meilisearch: {
    hostUrl: 'https://my-meilisearch-server.example.com',
    searchApiKey: 'api_key',
    serverSideUsage: false // default false
  },
  plausible: {
    apiHost: 'https://plausible.pasteur.cloud',
    enableAutoOutboundTracking: true
  },
  devtools: {
    enabled: false
  },
  runtimeConfig: {

    public: {
      shortSha: 'xxxxx',
      defenseFinderWebservice: '/',
      hostUrl: "http://localhost:8082",
      meilisearchClient: {
        hostUrl: 'http://localhost:7700',
        searchApiKey: 'api_key',
      },
      // meiliHost: 'http://localhost:7700',
      // meiliApiKey: 'api_key'
    }
  },

  vue: {
    compilerOptions: {
      isCustomElement: (tag) => ['pdbe-molstar'].includes(tag),
    },
  },
  eslint: {
    /* module options */
    lintOnStart: false,
    config: {
      stylistic: true // <---
    }
  },


})