export type OperonStructureIndexName = 'systemoperonstruct'


export interface StructureOperonGene {
    gene: string
    id: number
    subsystem: string
    version: string
    system: string
    exchangeables: string[]
    size: number
    position: number
    highlight?: boolean

}

export interface StructureOperonGeneWithImg extends StructureOperonGene {
    structImg?: string
    structPath?: string
}

export interface StructureOperonGeneWithCoordinate extends StructureOperonGeneWithImg {
    width: number
    height: number
    x: number
    y: number
    labelHeight: number,
}

export interface StructureOperonGeneLabels extends StructureOperonGeneWithCoordinate {
    sanitizedLabel: string
}

export interface StructureGeneLinks {
    source: [number, number]
    target: [number, number]
    highlight: boolean
}


export type StructurePredictionType = "monomer" | "multimer" | "multimer(dimer)" | "multimer(homodimer)"
export type StructureType = "Validated" | "DF" | "na"

export interface StructureItem {
    id: number
    proteins_in_the_prediction: string[]
    prediction_type: StructurePredictionType
    batch: number
    nb_sys: number
    type: StructureType
    system_number_of_genes: number
    system_genes: string[]
    pdb: string
    pae_table: string
    plddt_table: string
    fasta_file: string
    completed: boolean
    'iptm+ptm': number | null
    pDockQ: number | null
    plddts: number | null
    structure_system: string
    system: string
    subsystem: string
    Abi2: string
    OrbA: string
    Anti_BREX: string
    // "structuresUrls": ["/wiki/Avs/AVAST_I.AVAST_I__Avs1A.0.V.cif", "/wiki/Avs/AVAST_I.AVAST_I__Avs1A.0.V.pdb"]
}

export interface StructureItemWithImage extends StructureItem {
    structImg: string
    structPath: string
}