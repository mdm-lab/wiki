import type { MarkdownParsedContent } from '@nuxt/content/dist/runtime/types'



interface DefenseFinderContent extends MarkdownParsedContent {
    tableColumns: {
        article: {
            doi: string
            abstract?: string
        }
        Sensor: string
        Activator: string
        Effector: string
        PFAM: string
    }
    contributors?: string[]
    relevantAbstracts: { doi: string }[]
}


export interface WikiArticle {
    DOI: string
    title?: string
    subtitle?: string
    author?: Array<{ family: string; given: string }>
    containerTitle?: string
    abstract?: string
    year?: string
    href?: string
    target?: string
    prependIcon?: string
}

export interface CslJson {
    // id: string
    type: string
    title: string
    "container-title": string
    // page: string,
    // volume: string,
    abstract: string
    // URL: string
    DOI: string
    // journalAbbreviation: string
    // language: string
    author: Array<{ family: string, given: string }>
    issued: {
        "date-parts": Array<string>
    },
}

export interface CrossrefArticle {
    DOI: string;
    issue: number;
    type: string;
    title: string[];
    author: Array<{ family: string; given: string }>;
    // "container-title-short": string;
    "short-container-title": string;
    "container-title": string;
    abstract?: string;
    published: {
        "date-parts": string[];
    };
    issued: {
        "date-parts": string[];
    };
}

export interface RawArticle {
    message: CrossrefArticle

}

export type SrcArticle = CrossrefArticle | CslJson
