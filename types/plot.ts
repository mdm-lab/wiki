export interface PlotMargin {
    marginTop: number
    marginRight: number
    marginBottom: number
    marginLeft: number
}



