import YAML from 'yaml'
import * as d3 from "d3";




export default defineNitroPlugin((nitroApp) => {

    nitroApp.hooks.hook(
        'content:file:afterParse',
        (file) => {
            if (file?._id?.startsWith('content') && file._id.endsWith('.md')) {
                if (file?.body?.children?.length > 0) {
                    // has contributors
                    if (file?.contributors?.length > 0) {
                        const h1Index = file.body.children.findIndex(({ tag }) => tag === "h1")
                        if (h1Index >= 0) {
                            file.body.children.splice(h1Index + 1, 0, {
                                type: "element",
                                tag: 'contributors',
                                props: {},
                                children: []
                            })
                        }
                    }


                    const root = d3.hierarchy(file.body)
                    const refNodes = []
                    root.eachAfter(({ data }) => {
                        if (data?.tag === 'ref') refNodes.push(data)
                    })
                    const refTags = new Set(
                        refNodes
                            .flatMap(({ props }) => {
                                return props?.doi ? props.doi.split(',').map(d => d.trim()) : null
                            })
                            .filter(doi => doi !== null)
                    )
                    if (refTags.size > 0) file.references = Array.from(refTags).map(doi => ({ doi }))
                    // Update the TOC
                    // if relevant abstract available
                    if (file?.relevantAbstracts?.length > 0) {
                        // check if relevant Abstracts exists

                        // Add isRelevant flag
                        file.relevantAbstracts = file.relevantAbstracts.map(article => ({ ...article, isRelevant: true }))
                    } else { file.relevantAbstracts = [] }

                    if (file?.references?.length > 0) {
                        // create a set of relevant abstract:
                        const relevantAbstractsSet = new Set(file.relevantAbstracts.map(art => art.doi))
                        for (const ref of file.references) {
                            if (!relevantAbstractsSet.has(ref.doi)) {
                                file.relevantAbstracts.push(ref)
                            }
                        }

                    }
                }
                if (file?.relevantAbstracts?.length > 0) {
                    file.body.toc.links.push({ id: "references", depth: 2, text: 'References' })
                    file.body.children.push({
                        type: "element",
                        tag: 'relevant-abstracts',
                        props: {},
                        children: []
                    })


                }

                // add pdock matrix

                // find structure 

                // const index = file.body.children.findIndex(child => child.tag === "h2" && child.props.id === "structure")
                // console.log(index)
                // if (index !== -1) {
                //     file.body.children.splice(index + 1, 0, {
                //         type: "element",
                //         tag: 'pdockq-matrix',
                //         props: {},
                //         children: []
                //     })
                // }
            }
        })
})