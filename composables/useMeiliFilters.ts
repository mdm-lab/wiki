import type { FilterItem } from '@/components/AutocompleteMeiliFacets.vue'
import { filter } from '@observablehq/plot'
import type { Filter } from "meilisearch"

export function useMeiliFilters(filters: MaybeRef<FilterItem[] | undefined>, numericalFilters: Ref<string[] | undefined>) {

  const arrayFilters = ref<Filter | undefined>(undefined)
  const nf = toRef(numericalFilters)


  watchEffect(() => {
    const toValFilters = toValue(filters)
    const toValNumericalFilter = toValue(nf)
    let categoricalFilters: FilterItem[] | undefined = undefined
    if (toValFilters !== undefined && toValFilters.length >= 3) {
      const cachedFilters = [...toValFilters]

      // Remove last element if it is an outerOperator
      if (cachedFilters.length % 4 === 0 && cachedFilters.slice(-1)[0].type === 'outerOperator') {
        cachedFilters.splice(-1)
      }


      let previousOperator: 'AND' | 'OR' | undefined = undefined
      const arrayFilters = cachedFilters.reduce<FilterItem[]>((acc, curr, index) => {
        const sanitizedValue = curr.value.split("-").slice(0, -1).join("-")
        const position = index + 1
        switch (position % 4) {
          case 0:
            // if this is the first time pass by an outeroperator
            if (previousOperator === undefined) {
              const newFilter = acc.splice(-3).join("")
              if (sanitizedValue === 'OR') {
                acc = [[newFilter]]
              }
              else {
                acc.push(newFilter)
              }
            }
            previousOperator = sanitizedValue
            break;
          case 3:
            acc.push(`'${sanitizedValue}'`)
            const newFilter = acc.splice(-3).join("")
            if (previousOperator === 'AND') {
              acc.push(newFilter)
            }
            else {
              const previousElem = acc.slice(-1)[0]
              if (Array.isArray(previousElem)) {
                acc.slice(-1)[0].push(newFilter)
              }
              else if (previousElem !== undefined) {
                const previousElem = acc.splice(-1)[0]
                acc.push([previousElem, newFilter])
              }
              else {
                acc.push(newFilter)
              }
            }
            break
          default:
            acc.push(sanitizedValue)
            break;
        }
        return acc
      }, [])
      categoricalFilters = arrayFilters.length > 0 ? arrayFilters : undefined
    }

    function isFilterItem(item: FilterItem[] | string[] | undefined): item is FilterItem[] | string[] {
      return item !== undefined

    }
    const filterItemArray = [categoricalFilters, toValNumericalFilter].filter(isFilterItem).flat()

    arrayFilters.value = filterItemArray
  })

  // const arrayFilters: ComputedRef<Filter | undefined> = computed(() => {
  // })

  return { arrayFilters }
}
