import * as d3 from "d3";


export interface TextRotateInput {
    availableWidth: MaybeRef<number>
    text: MaybeRef<string>
}

export interface TextRotateOuput {
    rotate: ComputedRef<number>
    width: ComputedRef<number>
    height: ComputedRef<number>
    wrappedText: ComputedRef<string[]>
    setTextElem: (newTextElement: MaybeRef<SVGTextElement>) => void
    textElement: MaybeRef<SVGTextElement | undefined>
}

export function useTextRotate({ availableWidth, text }: TextRotateInput): TextRotateOuput {
    const textElement = ref<SVGTextElement | undefined>(undefined)
    const availableWidthVal = toValue(availableWidth)

    const words = computed(() => {
        const textVal = toValue(text)
        if (textVal !== null) {
            return textVal.split(" ")
        }
        return []
    })

    const wrappedText = computed(() => {
        const textElementVal = toValue(textElement)

        if (textElementVal !== undefined) {
            const wordsVal = toValue(words)
            const wordList = wordsVal.map(d => d).reverse()
            const availableWidthVal = toValue(availableWidth)
            let wrappedTextList = []
            let line = []
            let word: string | undefined = undefined
            let maxWidth = 0
            while (word = wordList.pop()) {
                line.push(word)
                textElementVal.textContent = line.join(" ")
                const width = textElementVal.getComputedTextLength()
                textElementVal.textContent = ''
                if (width > maxWidth) {
                    maxWidth = width
                }
                if (width > availableWidthVal) {
                    if (line.length === 1) {
                        return [toValue(text)]
                    }
                    else {
                        const w = line.pop()
                        if (w !== undefined) {
                            wordList.push(w)
                            wrappedTextList.push(line.join(" "))
                            line = []
                        }
                    }
                }
                else {
                    if (wordList.length === 0) {
                        wrappedTextList.push(line.join(" "))
                    }
                }
            }
            textElementVal.textContent = ''
            return wrappedTextList
        }
        return ['']
    })


    const textWidth = computed(() => {
        const textElementVal = toValue(textElement)
        if (textElementVal !== undefined) {
            textElementVal.textContent = toValue(text)
            const width = textElementVal.getComputedTextLength()
            textElementVal.textContent = ""
            return width
        }
        return 0
    })

    const width = computed(() => {
        const textElementVal = toValue(textElement)
        const wrappedTextVal = toValue(wrappedText)
        const rotateRadVal = toValue(rotateRad)

        if (rotateRadVal === Math.PI / 2) return 0

        if (textElementVal !== undefined) {
            if (wrappedTextVal.length === 1) {
                textElementVal.textContent = wrappedTextVal[0]
                const w = textElementVal.getComputedTextLength()
                textElementVal.textContent = ''
                return w
            }
            else {
                // textElemSelection.selectAll()
                let maxWidth = 0
                for (let i = 0; i < wrappedTextVal.length; i++) {
                    textElementVal.textContent = wrappedTextVal[i]
                    const width = textElementVal.getComputedTextLength()
                    if (width > maxWidth) {
                        maxWidth = width
                    }
                }
                textElementVal.textContent = ''
                return maxWidth
            }
        }
        else { return 0 }
    })

    const height = computed(() => {
        const rotateRadVal = toValue(rotateRad)
        const wrappedTextVal = toValue(wrappedText)
        if (wrappedTextVal.length > 1) {
            return 16 * wrappedTextVal.length + 10
        }
        else {
            if (rotateRadVal === Math.PI / 2) {
                return toValue(textWidth)
            }
            else {
                return 16
            }

        }
    })

    function setTextElem(newTextElement: MaybeRef<SVGTextElement>) {
        textElement.value = toValue(newTextElement)
    }

    const rotateRad = computed(() => {
        const wrappedTextVal = toValue(wrappedText)
        const textWidthVal = toValue(textWidth)
        if (wrappedTextVal.length === 1 && availableWidthVal < textWidthVal) {
            return Math.PI / 2
        }
        return 0
    })

    const rotate = computed(() => {
        return rotateRad.value * 180 / Math.PI
    })

    return { rotate, width, height, wrappedText, setTextElem, textElement }
}