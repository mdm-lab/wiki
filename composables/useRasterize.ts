import { useDownloadBlob } from './useDownloadBlob';
import { useSerialize } from './useSerialize';
import { useSvgPlot } from './useSvgPlot';
const { serialize } = useSerialize()

export function useRasterize() {

    function rasterize(component: MaybeRef<ComponentPublicInstance | null>, filename: MaybeRef<string>) {
        const toValueCompo = toValue(component)

        if (toValueCompo !== null) {
            const { svg } = useSvgPlot(toValueCompo)
            const toValueSvg = toValue(svg)
            if (toValueSvg !== null) {
                let resolve, reject;
                const promise: Promise<Blob> = new Promise((y, n) => (resolve = y, reject = n));
                const image = new Image;
                image.onerror = reject;

                image.onload = () => {
                    const rect = toValueSvg.getBoundingClientRect();
                    const canvas = document.createElement("canvas");
                    canvas.width = rect.width
                    canvas.height = rect.height
                    const ctx = canvas.getContext("2d")


                    if (ctx !== null) {
                        ctx.drawImage(image, 0, 0, rect.width, rect.height);
                        ctx.canvas.toBlob(resolve);
                    }
                }
                const blob = toValue(serialize(component))
                if (blob !== undefined) {
                    image.src = URL.createObjectURL(blob);
                }
                return promise;
            }
        }

    }
    return { rasterize }

}