import Papa from 'papaparse';
// import { saveAs } from "file-saver";
import { useDownloadBlob } from './useDownloadBlob';
const { download } = useDownloadBlob()

export function useCsvDownload(
  rawData: MaybeRef<Record<string, any>>,
  columns: MaybeRef<string[] | undefined> = undefined,
  baseName: MaybeRef<string> = 'data'
) {
  const filename = ref(`df-${toValue(baseName)}.csv`)
  const data = ref()
  const blob = ref()
  if (toValue(rawData)?.hits?.length > 0) {
    data.value = toValue(rawData).hits.map(row => {
      let sanitizedRow = { ...row }
      if (sanitizedRow?.PFAM?.length > 0) {
        sanitizedRow = {
          ...sanitizedRow,
          PFAM: sanitizedRow.PFAM.map(({ AC }) => AC).join(", ")
        }
      }
      if (sanitizedRow?.contributors?.length > 0) {
        sanitizedRow = {
          ...sanitizedRow,
          contributors: sanitizedRow.contributors.join(", ")
        }
      }
      return sanitizedRow

    })
    const csvContent = Papa.unparse(toValue(data), { columns: toValue(columns) });
    blob.value = new Blob([csvContent], { type: "text/csv" });
    download(blob, filename)
  }
  return { data, filename }
}
