export function useNumericalFilter(
    id: MaybeRef<string>,
    min: MaybeRef<number>,
    max: MaybeRef<number>,
) {



    const minRef = toRef(min)
    const maxRef = toRef(max)

    const range: Ref<[number, number]> = ref([toValue(min), toValue(max)])
    const stringifyFilter: Ref<string | undefined> = ref(undefined)



    watchEffect(() => {
        if (range.value[0] === toValue(minRef) && range.value[1] === toValue(maxRef)) {
            stringifyFilter.value = undefined
        } else {
            stringifyFilter.value = `'${toValue(id)}' ${range.value[0]} TO ${range.value[1]}`
        }
    })
    function reset() {
        range.value = [toValue(minRef), toValue(maxRef)]
    }
    return { range, stringifyFilter, reset }
}