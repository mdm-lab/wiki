export function useSvgPlot(component: MaybeRef<ComponentPublicInstance>) {
    const svg = ref<SVGElement | null>(null)
    const toValueCompo = toValue(component)
    const rootElem = toValueCompo.$el
    svg.value = rootElem.querySelector("svg.plot")

    return { svg }
}