import { withoutTrailingSlash, withLeadingSlash, joinURL } from "ufo";

export function useRefinedUrl(url: MaybeRef<string>) {
    const refinedUrl = computed(() => {
        const sanitzedUrl = toValue(url)

        if (sanitzedUrl.startsWith("/") && !sanitzedUrl.startsWith("//")) {
            const _base = withLeadingSlash(
                withoutTrailingSlash(useRuntimeConfig().app.baseURL)
            );
            if (_base !== "/" && !sanitzedUrl.startsWith(_base)) {
                return joinURL(_base, sanitzedUrl);
            }
        }
        return sanitzedUrl;
    });
    return { refinedUrl }
}