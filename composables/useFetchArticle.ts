import { ref, toValue } from "vue"
import { StorageSerializers } from "@vueuse/core"
import type { CslJson, WikiArticle } from '@/types/articles';
import type { SearchParams } from 'meilisearch'

export function useFetchArticle(doi: string = "") {
    const doiBaseUrl = ref(new URL("https://doi.org/"));
    const article = useSessionStorage<WikiArticle | undefined>(doi, null, { serializer: StorageSerializers.object })
    // const article = ref<WikiArticle | undefined>(null)
    const client = useMeiliSearchRef()
    const index = ref("article")
    const params = ref<SearchParams>({
        facets: ["*"],
        filter: [`DOI='${doi}'`],
        limit: 1
    })
    function getReferenceUrl(doi: string) {
        return new URL(doi, doiBaseUrl.value).href;
    }
    function toAuthorsString(authors: Array<{ family: string; given: string }>) {
        return authors
            .map((curr) => {
                return `${curr.family} ${curr.given}`;
            })
            .join(", ");
    }
    function zoteroArticleToArticle(zoteroArticle: CslJson): WikiArticle | undefined {
        if (zoteroArticle != undefined) {
            const {
                DOI,
                title,
                "container-title": ct,
                abstract,
                issued,
                author,
                ...rest
            } = zoteroArticle;
            return {
                DOI,
                title,
                subtitle: toAuthorsString(author || []),
                author,
                containerTitle: ct,
                abstract,
                year: issued?.["date-parts"][0][0] ?? '',
                href: getReferenceUrl(DOI),
                target: "_blank",
                prependIcon: "mdi-newspaper-variant-outline",
            }
        } else { return undefined }
    }

    if (!article.value) {
        client.index(toValue(index)).search<CslJson>("", toValue(params)).then((data) => {
            if (data !== undefined && data?.hits.length >= 1) {
                const sanitizedData = zoteroArticleToArticle(data.hits[0])
                article.value = sanitizedData
            }
        }).catch((error) => {
            article.value = { DOI: doi }
        })
    }
    return { article }
}







