export interface FilterItem {
    type: 'facet' | 'operator' | 'value' | 'text'
    value: string
    title: string
    count?: number
    deletable: boolean
    props: Record<string, any>
}

export function useFacetFilters(inputFilters: MaybeRef<FilterItem[] | undefined>) {



}