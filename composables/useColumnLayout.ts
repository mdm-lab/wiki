
import { useDisplay, useLayout } from "vuetify";

interface ParametersColumnLayout {
    contentWidth: MaybeRef<number>,
    widthThreshold: MaybeRef<number> | ((width: Ref<number>) => number)
}

export function useColumnLayout({ contentWidth, widthThreshold }: ParametersColumnLayout) {
    const { getLayoutItem } = useLayout()
    const { width } = useDisplay()
    const widthTresholdRef = computed(() => {
        if (typeof widthThreshold === "function") {
            return widthThreshold(width)
        }
        else {
            return toValue(widthThreshold)
        }
    })

    const computedWidth = computed(() => {
        const drawerLayout = getLayoutItem("drawer")
        if (drawerLayout && drawerLayout.size > 0) {
            return toValue(width) - drawerLayout.size
        }
        return toValue(width)
    })

    const wholeRow = computed(() => {
        return toValue(contentWidth) > toValue(computedWidth) / 2
    })
    return { widthTresholdRef, wholeRow }
}