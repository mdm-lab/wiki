#!/usr/bin/env python3
import pandas as pd
d = {}
start = False
with open("Pfam-A.hmm.dat", "r") as pf:
    for i, line in enumerate(pf):
        line = line.strip()
        if "# STOCKHOLM 1.0" in line or line == "//":
            start = True
        else:
            if start:
                pfID = line.split()[2]
                d[pfID] = {}
                start = False
            else:

                kk, v = line.split("   ")
                g, k = kk.split()
                d[pfID][k] = v

df = pd.DataFrame(d).T
df.index.name = "ID"
df.reset_index().to_csv("Pfam-A.hmm.dat.csv", index=False, sep=",")

