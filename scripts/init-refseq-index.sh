#!/bin/sh

############################################################
# Help                                                     #
############################################################
Help() {
    # Display Help
    echo "load refseq data to meilisearch instance"
    echo
    echo "Syntax: scriptTemplate [-h|n|r|s|k]"
    echo "options:"
    echo "n   meilisearch hostname"
    echo "r   refseq csv file"
    echo "s   structure tsv file"
    echo "k   meili_api_key"
    echo "h   Print this Help."
    echo
}

############################################################
############################################################
# Main program                                             #
############################################################
############################################################

# Set variables
Name="localhost"
Refseq="refseq.csv"
Structure="struct.tsv"
ApiKey="MASTER_KEY"
############################################################
# Process the input options. Add options as needed.        #
############################################################
# Get the options
while getopts ":n:k:r:s:h:" option; do
    case $option in
    h) # display Help
        Help
        exit
        ;;
    n) # set host
        Name=$OPTARG ;;

    r) # refseq filename
        Refseq=$OPTARG ;;
       
    s) # structure prediction summary file
        Structure=$OPTARG ;;

    k) # api key
        ApiKey=$OPTARG ;;

    \?) # Invalid option
        echo "Error: Invalid option"
        exit
        ;;
    esac
done
echo $Structure

# load data
curl \
    -X POST "${Name}/indexes/refseq/documents?primaryKey=id" \
    -H 'Content-Type: text/csv' \
    -H "Authorization: Bearer ${ApiKey}" \
    --data-binary "@${Refseq}"
echo "\n"
# set maxTotalHits
curl \
    -X PATCH "${Name}/indexes/refseq/settings/pagination" \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${ApiKey}" \
    --data-binary '{"maxTotalHits": 1000000}'
echo "\n"

# set filters
curl \
    -X PUT "${Name}/indexes/refseq/settings/filterable-attributes" \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${ApiKey}" \
    --data-binary '[
    "type",
    "subtype",
    "Superkingdom",
    "phylum",
    "order",
    "family",
    "genus",
    "species"
  ]'
echo "\n"


#############
# Structure #
#############
# load data
curl \
    -X POST "${Name}/indexes/structure/documents?primaryKey=proteins_in_the_prediction" \
    -H 'Content-Type: text/csv' \
    -H "Authorization: Bearer ${ApiKey}" \
    --data-binary "@${Structure}"
echo "\n"
# set maxTotalHits


