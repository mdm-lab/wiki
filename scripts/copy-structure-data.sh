#!/bin/bash 

# find * -type d -exec sh -c 'for d in "$@"; do (cd "/usr/src/app/public/$d"; cp --archive --recursive --symbolic-link /structure-data/sanitized-dump/$d/* .) done' argv0 {} +
find * -type d -exec sh -c 'for d in "$@"; do (cd "/usr/share/nginx/html/$d"; cp --archive --recursive --symbolic-link /structure-data/sanitized-dump/$d/* .) done' argv0 {} +