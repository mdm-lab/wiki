import { defineStore } from 'pinia'
import { ref } from 'vue'
// import jsonArticles from '@/assets/articles.json'
import type { CslJson } from "@/types/articles"


export const useArticlesStore = defineStore('articles', () => {
    const articleMap = new Map([])
    const articles = ref(articleMap)
    function add(article: CslJson) {
        const doi = article.DOI
        if (articles.value.has(doi)) return
        articles.value.set(article.DOI, article)
    }



    return { articles, add }
})