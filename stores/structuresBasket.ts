import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useStructuresBasket = defineStore('structuresBasket', () => {
    const rawStructures = ref<string[] | undefined>(undefined)

    function pdbToCif(pdbPath: string) {
        const cifPath = pdbPath.split(".").slice(0, -1).join(".")
        return `${cifPath}.cif`
    }
    const structures = computed(() => {
        const rawStructuresVal = toValue(rawStructures)
        if (rawStructuresVal !== undefined) {
            return rawStructuresVal
                // add cif
                .flatMap((struct) => {
                    return [pdbToCif(struct), struct]
                })
                // refinedUrl
                .map((struct) => {
                    const { refinedUrl } = useRefinedUrl(struct)
                    return toValue(refinedUrl)
                })
        }
        return rawStructuresVal
    })

    function set(structuresToSet: MaybeRef<string[] | undefined>) {
        rawStructures.value = toValue(structuresToSet)
    }
    return { structures, set }
})