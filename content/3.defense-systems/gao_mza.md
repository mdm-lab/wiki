---
title: Gao_Mza
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00023, PF04542, PF04545, PF10592, PF10593, PF13589, PF13606, PF14390
contributors:
    - Hugo Vaysset
relevantAbstracts:
    - doi: 10.1126/science.aba0372
---

# Gao_Mza

## Description
Mza (MutL, Z1, DUF, AIPR) is a defense system composed of five proteins. Its antiphage activity was assessed by heterologous expression in *E. coli* against phages T2, T4, T5, lambda and M13 (ssDNA phage) :ref{doi=10.1126/science.aba0372}.  

## Molecular mechanism
As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The Gao_Mza is composed of 5 proteins: MzaA, MzaB, MzaC, MzaD and MzaE.

Here is an example found in the RefSeq database:

![gao_mza](/gao_mza/Gao_Mza.svg){max-width=750px}

The Gao_Mza system in *Massilia sp. Se16.2.3* (GCF_014171595.1, NZ_CP050451) is composed of 6 proteins MzaA (WP_182990577.1) MzaB (WP_229425381.1) MzaC (WP_182990578.1) MzaD (WP_182990579.1) MzaE (WP_182990580.1) MzaE (WP_182990581.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Salmonella enterica 
<a href='https://ncbi.nlm.nih.gov/protein/VEA06816.1'>VEA06816.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/VEA06814.1'>VEA06814.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/VEA06812.1'>VEA06812.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/VEA06810.1'>VEA06810.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/VEA06808.1'>VEA06808.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & Lambda & M13
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T5
        Lambda
        M13
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


