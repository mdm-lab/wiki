---
title: AbiV
layout: article
tableColumns:
    article:
      doi: 10.1128/AEM.00780-08
      abstract: |
        Insertional mutagenesis with pGhost9::ISS1 resulted in independent insertions in a 350-bp region of the chromosome of Lactococcus lactis subsp. cremoris MG1363 that conferred phage resistance to the integrants. The orientation and location of the insertions suggested that the phage resistance phenotype was caused by a chromosomal gene turned on by a promoter from the inserted construct. Reverse transcription-PCR analysis confirmed that there were higher levels of transcription of a downstream open reading frame (ORF) in the phage-resistant integrants than in the phage-sensitive strain L. lactis MG1363. This gene was also found to confer phage resistance to L. lactis MG1363 when it was cloned into an expression vector. A subsequent frameshift mutation in the ORF completely eliminated the phage resistance phenotype, confirming that the ORF was necessary for phage resistance. This ORF provided resistance against virulent lactococcal phages belonging to the 936 and c2 species with an efficiency of plaquing of 10?4, but it did not protect against members of the P335 species. A high level of expression of the ORF did not affect the cellular growth rate. Assays for phage adsorption, DNA ejection, restriction/modification activity, plaque size, phage DNA replication, and cell survival showed that the ORF encoded an abortive infection (Abi) mechanism. Sequence analysis revealed a deduced protein consisting of 201 amino acids which, in its native state, probably forms a dimer in the cytosol. Similarity searches revealed no homology to other phage resistance mechanisms, and thus, this novel Abi mechanism was designated AbiV. The mode of action of AbiV is unknown, but the activity of AbiV prevented cleavage of the replicated phage DNA of 936-like phages.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF18728
contributors:
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1023/A:1002027321171
  - doi: 10.1128/AEM.00780-08
---

# AbiV

## Description
AbiV was discovered was discovered in 2008 in *Lactococcus lactis* ref:{doi=10.1023/A:1002027321171}.

AbiO is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. Even though their name corresponds to an abortive infection, their mechanism of action does not necessarily correspond to an abortive infection. AbiV system is classified as an abortive infection in :ref{doi=10.1016/j.mib.2023.102312}

This system is composed of one single protein containing a HEPN domain (HEPN_AbiV PF18728).

## Molecular mechanism
As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The AbiV is composed of 1 protein: AbiV.

Here is an example found in the RefSeq database:

![abiv](/abiv/AbiV.svg){max-width=750px}

The AbiV system in *Bacillus mycoides* (GCF_018739365.1, NZ_CP035984) is composed of 1 protein: AbiV (WP_070128168.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Haaber_2008[<a href='https://doi.org/10.1128/AEM.00780-08'>Haaber et al., 2008</a>] --> Origin_0
    Origin_0[Lactococcus lactis 
<a href='https://ncbi.nlm.nih.gov/protein/AAK16428.1'>AAK16428.1</a>] --> Expressed_0[Lactococcus lactis]
    Expressed_0[Lactococcus lactis] ----> sk1 & p2 & jj50 & P008 & bIL170 & c2 & bIL67 & ml3 & eb1
    subgraph Title1[Reference]
        Haaber_2008
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        sk1
        p2
        jj50
        P008
        bIL170
        c2
        bIL67
        ml3
        eb1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

