---
title: PfiAT
layout: article
tableColumns:
    article:
      doi: 10.1111/1751-7915.13570
      abstract: |
        Pf prophages are ssDNA filamentous prophages that are prevalent among various Pseudomonas aeruginosa strains. The genomes of Pf prophages contain not only core genes encoding functions involved in phage replication, structure and assembly but also accessory genes. By studying the accessory genes in the Pf4 prophage in P. aeruginosa PAO1, we provided experimental evidence to demonstrate that PA0729 and the upstream ORF Rorf0727 near the right attachment site of Pf4 form a type II toxin/antitoxin (TA) pair. Importantly, we found that the deletion of the toxin gene PA0729 greatly increased Pf4 phage production. We thus suggest the toxin PA0729 be named PfiT for Pf4 inhibition toxin and Rorf0727 be named PfiA for PfiT antitoxin. The PfiT toxin directly binds to PfiA and functions as a corepressor of PfiA for the TA operon. The PfiAT complex exhibited autoregulation by binding to a palindrome (5'-AATTCN5 GTTAA-3') overlapping the -35 region of the TA operon. The deletion of pfiT disrupted TA autoregulation and activated pfiA expression. Additionally, the deletion of pfiT also activated the expression of the replication initiation factor gene PA0727. Moreover, the Pf4 phage released from the pfiT deletion mutant overcame the immunity provided by the phage repressor Pf4r. Therefore, this study reveals that the TA systems in Pf prophages can regulate phage production and phage immunity, providing new insights into the function of TAs in mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF02604, PF05016
contributors: 
  - Helena Shomar
  - Marie Guillaume
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1111/1751-7915.13570
---

# PfiAT

## Description

PfiAT is a toxin-antitoxin system discovered in 2020 :ref{doi=10.1111/1751-7915.13570}.

The role of PfiAT as an antiphage system is not certain and might be removed from DefenseFinder. This system is known to inhibit the Pf4 prophage from *Pseudomonas aeruginosa*.

The PfiAT system is composed of two small proteins: the antitoxin PfiA and the toxin PfiT.

## Molecular mechanism

The PfiAT system acts as a regulator of the Pf4 prophages :ref{doi=10.1111/1751-7915.13570}. The PfiAT complex will autorepress its own locus.

In the absence of PfiT, PfiA is not able to bind to the promotor. This change leads to the expression of xisF4 and PA0727 leading to the phage replication and excretion.


## Example of genomic structure

The PfiAT is composed of 2 proteins: PfiA and PfiT.
The PfiAT is composed of 2 proteins: PfiA and PfiT.

Here is an example found in the RefSeq database:
Here is an example found in the RefSeq database:

![pfiat](/pfiat/PfiAT.svg){max-width=750px}

The PfiAT system in *Desulfoscipio gibsoniae* (GCF_000233715.2, NC_021184) is composed of 2 proteins PfiA (WP_006521270.1) PfiT (WP_006521271.1) 
The PfiAT system in *Desulfoscipio gibsoniae* (GCF_000233715.2, NC_021184) is composed of 2 proteins PfiA (WP_006521270.1) PfiT (WP_006521271.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

To our knowledge, no experimental validation of this toxin-antitoxin system exists.
It was shown to regulate the replication and expression of the prophage Pf4.

