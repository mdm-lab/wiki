---
title: Viperin
layout: article
tableColumns:
    article:
      doi: 10.1038/s41586-020-2762-2
      abstract: |
        Viperin is an interferon-induced cellular protein that is conserved in animals. It has previously been shown to inhibit the replication of multiple viruses by producing the ribonucleotide 3'-deoxy-3',4'-didehydro (ddh)-cytidine triphosphate (ddhCTP), which acts as a chain terminator for viral RNA polymerase2. Here we show that eukaryotic viperin originated from a clade of bacterial and archaeal proteins that protect against phage infection. Prokaryotic viperins produce a set of modified ribonucleotides that include ddhCTP, ddh-guanosine triphosphate (ddhGTP) and ddh-uridine triphosphate (ddhUTP). We further show that prokaryotic viperins protect against T7 phage infection by inhibiting viral polymerase-dependent transcription, suggesting that it has an antiviral mechanism of action similar to that of animal viperin. Our results reveal a class of potential natural antiviral compounds produced by bacterial immune systems.
    Sensor: Unknown
    Activator: Direct binding
    Effector: Nucleotide modifying
    PFAM: PF04055, PF13353
contributors:
    - Marian Dominguez-Mirazo
    - Heloïse Georjon
relevantAbstracts:
    - doi: 10.1038/s41586-020-2762-2
---

# Viperin
## Description
 
Viperins, for "Virus Inhibitory Protein, Endoplasmic Reticulum-associated, INterferon-inducible", are antiviral enzymes whose expression is stimulated by interferons in eukaryotic cells. They are important components of eukaryotic innate immunity and present antiviral activity against a wide diversity of viruses, including double-stranded DNA viruses, single-strand RNA viruses and retroviruses :ref{doi=10.1146/annurev-virology-011720-095930}.  

Recently,  Viperin-like enzymes were found in prokaryotes (pVips).  Strikingly, like their eukaryotic counter-part with eukaryotic viruses, pVips provide clear protection against phage infection to their host and therefore constitute a new defense system :ref{doi=10.1038/s41586-020-2762-2}. Like eukaryotic Viperins, pVips produce modified nucleotides that block phage transcription, acting as chain terminators. They constitute a form of chemical defense. A recent study reported that pVips can be found in around 0.5% of prokaryotic genomes :ref{doi=10.1038/s41467-022-30269-9}.

## Molecular mechanism

![Figure1](/viperin/human_vip.jpg){max-width=750px}
Fig.1: Catalytic activity of human Viperin generates ddhCTP :ref{doi=10.1002/1873-3468.13778}

Viperins are members of the radical S-adenosylmethionine (rSAM) superfamily. This group of enzymes uses a 4Fe-4S] cluster to cleave S-adenosylmethionine (SAM) reductively, generating a radical that is generally transferred to a substrate. It was demonstrated that through their [4Fe-4S] cluster catalytic activity, eukaryotic viperins convert a ribonucleotide, the cytidine triphosphate (CTP) into a modified ribonucleotide, the 3'-deoxy-3',4'-didehydro-CTP (ddhCTP) :ref{doi=10.1038/s41586-018-0238-4}. 

Prokaryotic Viperins also convert ribonucleotides triphosphate into modified ribonucleotides, but contrary to their eukaryotic counterparts can use a diversity of substrates to produce ddhCTP, or ddh-guanosine triphosphate (ddhGTP), or ddh-uridine triphosphate (ddhUTP), or several of these nucleotides for certain pVips :ref{doi=10.1038/s41586-020-2762-2}.

Compared to the initial ribonucleotide triphosphate, the modified ddh-nucleotide product of Viperins lacks a hydroxyl group at the 3′ carbon of the ribose (Fig.1). The ddh-nucleotides produced by Viperins can be used as substrates by some viral RNA polymerases. Because of their lost hydroxyl group at the 3’carbon of the ribose, once incorporated into the newly forming viral RNA chain, these ddh-nucleotides act as chain terminators. By preventing further polymerization of the viral RNA chain, ddh-nucleotides can inhibit viral replication :ref{doi=10.1038/s41586-020-2762-2,10.1038/s41586-018-0238-4}.

## Example of genomic structure

The Viperin is composed of 1 protein: pVip.

Here is an example found in the RefSeq database:

![viperin](/viperin/Viperin.svg){max-width=750px}

The Viperin system in *Vibrio sp. SCSIO 43009* (GCF_023716525.1, NZ_CP071842) is composed of 1 protein: pVip (WP_182016311.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_0
    Origin_0[ pVip6
Selenomonas ruminatium 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624749465'>2624749465</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_1
    Origin_1[ pVip7
Fibrobacter sp. UWT3 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2739066738'>2739066738</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_2
    Origin_2[ pVip9
Vibrio porteresiae 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2574301464'>2574301464</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_3
    Origin_3[ pVip12
Ruegeria intermedia 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2698137626'>2698137626</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_4
    Origin_4[ pVip15
Coraliomargarita akajimensis 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=646713396'>646713396</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_5
    Origin_5[ pVip21
Lewinella persica 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2515428782'>2515428782</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_6
    Origin_6[ pVip32
Phormidium sp. OSCR GFM 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609132705'>2609132705</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_7
    Origin_7[ pVip34
Cryomorphaceae bacterium 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2619892213'>2619892213</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_8
    Origin_8[ pVip37
Shewanella sp. cp20 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2632937107'>2632937107</a>] --> Expressed_8[Escherichia coli]
    Expressed_8[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_9
    Origin_9[ pVip39
Burkholderiales-76 UID4002 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2634960437'>2634960437</a>] --> Expressed_9[Escherichia coli]
    Expressed_9[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_10
    Origin_10[ pVip44
Chondromyces crocatus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2648875132'>2648875132</a>] --> Expressed_10[Escherichia coli]
    Expressed_10[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_11
    Origin_11[ pVip46
Photobacterium swingsii 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2649993803'>2649993803</a>] --> Expressed_11[Escherichia coli]
    Expressed_11[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_12
    Origin_12[ pVip57
Flavobacterium lacus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2718503187'>2718503187</a>] --> Expressed_12[Escherichia coli]
    Expressed_12[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_13
    Origin_13[ pVip58
Pseudoalteromonas ulvae 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2721736750'>2721736750</a>] --> Expressed_13[Escherichia coli]
    Expressed_13[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_14
    Origin_14[ pVip60
Lacinutrix sp. JCM 13824 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2733913669'>2733913669</a>] --> Expressed_14[Escherichia coli]
    Expressed_14[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_15
    Origin_15[ pVip61
Euryarchaeota archaeon SCGC AG-487_M08 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2741341560'>2741341560</a>] --> Expressed_15[Escherichia coli]
    Expressed_15[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_16
    Origin_16[ pVip62
Fibrobacteria bacterium 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2743907592'>2743907592</a>] --> Expressed_16[Escherichia coli]
    Expressed_16[Escherichia coli] ----> T7
    Bernheim_2020[<a href='https://doi.org/10.1038/s41586-020-2762-2'>Bernheim et al., 2020</a>] --> Origin_17
    Origin_17[ pVip63
Pseudoalteromonas sp. XI10 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2744633848'>2744633848</a>] --> Expressed_17[Escherichia coli]
    Expressed_17[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Bernheim_2020
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
        Origin_8
        Origin_9
        Origin_10
        Origin_11
        Origin_12
        Origin_13
        Origin_14
        Origin_15
        Origin_16
        Origin_17
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
        Expressed_8
        Expressed_9
        Expressed_10
        Expressed_11
        Expressed_12
        Expressed_13
        Expressed_14
        Expressed_15
        Expressed_16
        Expressed_17
end
    subgraph Title4[Protects against]
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


