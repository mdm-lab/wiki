---
title: ISG15-like
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.09.04.556158
      abstract: |
        Multiple immune pathways in humans conjugate ubiquitin-like proteins to virus and host molecules as a means of antiviral defense. Here we studied an anti-phage defense system in bacteria, comprising a ubiquitin-like protein, ubiquitin-conjugating enzymes E1 and E2, and a deubiquitinase. We show that during phage infection, this system specifically conjugates the ubiquitin-like protein to the phage central tail fiber, a protein at the tip of the tail that is essential for tail assembly as well as for recognition of the target host receptor. Following infection, cells encoding this defense system release a mixture of partially assembled, tailless phage particles, and fully assembled phages in which the central tail fiber is obstructed by the covalently attached ubiquitin-like protein. These phages exhibit severely impaired infectivity, explaining how the defense system protects the bacterial population from the spread of phage infection. Our findings demonstrate that conjugation of ubiquitin-like proteins is an antiviral strategy conserved across the tree of life.
    Sensor: Unknown
    Activator: Unknown
    Effector: Protein modifying
contributors: 
  - Alba Herrero del Valle
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.09.017
  - doi: 10.1101/2023.09.04.556158
---


# ISG15-like

## Description

ISG15-like (Interferon-stimulated gene 15 - like) systems (also known as Bil systems for Bacterial ISG15-like systems) are a 4-gene defense system comprising a homolog of ubiquitin-like ISG15 (BilA), ubiquitin-conjugating enzymes E1 (BilD) and E2 (BilB), and a deubiquitinase (BilC) :ref{doi=10.1101/2023.09.04.556158,10.1016/j.chom.2022.09.017}. It has been shown to defend against multiple coliphages :ref{doi=10.1016/j.chom.2022.09.017}. The Bil system is analogous to the ISG15 system in humans, which protects against viruses.

## Molecular mechanism

Hör et al., have shown that the ISG15-like system defends bacteria against phages by impairing the infectivity of newly synthesized phages. It does so by preventing tail assembly that leads to non-infective tailless phages or by producing modified tails with an obstructed tail tip that are not capable of infecting. More specifically, BilA is conjugated to the central tail fiber (CTF) protein of the phage :ref{doi=10.1101/2023.09.04.556158}.

## Example of genomic structure

The ISG15-like is composed of 4 proteins: BilA, BilB, BilC and BilD.

Here is an example found in the RefSeq database:

![isg15-like](/isg15-like/ISG15-like.svg){max-width=750px}

The ISG15-like system in *Caulobacter segnis* (GCF_023935105.1, NZ_CP096040) is composed of 4 proteins BilA (WP_252632187.1) BilB (WP_252632188.1) BilC (WP_252632189.1) BilD (WP_252632190.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Collimonas sp. OK412 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810443'>2609810443</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810442'>2609810442</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810441'>2609810441</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810440'>2609810440</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7 & SECphi17
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_1
    Origin_1[Caulobacter sp. Root343 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236682'>2644236682</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236681'>2644236681</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236680'>2644236680</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236679'>2644236679</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T4 & T6 & T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7 & SECphi17
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[Cupriavidus sp. SHE 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619719'>2654619719</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619720'>2654619720</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619721'>2654619721</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619722'>2654619722</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T4 & T6 & T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_3
    Origin_3[Paraburkholderia caffeinilytica 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959492'>2843959492</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959493'>2843959493</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959494'>2843959494</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959495'>2843959495</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T6 & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_4
    Origin_4[Thiomonas sp. FB-6 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655165'>2523655165</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655166'>2523655166</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655167'>2523655167</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655168'>2523655168</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> SECphi27
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
        Origin_4
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        SECphi17
        T4
        T6
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        SECphi17
        T2
        T4
        T6
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T6
        SECphi27
        SECphi27
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

