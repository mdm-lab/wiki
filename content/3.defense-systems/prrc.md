---
title: PrrC
layout: article
tableColumns:
    article:
      doi: 10.1186/1743-422X-7-360
      abstract: |
        Over 50 years of biological research with bacteriophage T4 includes notable discoveries in post-transcriptional control, including the genetic code, mRNA, and tRNA; the very foundations of molecular biology. In this review we compile the past 10 - 15 year literature on RNA-protein interactions with T4 and some of its related phages, with particular focus on advances in mRNA decay and processing, and on translational repression. Binding of T4 proteins RegB, RegA, gp32 and gp43 to their cognate target RNAs has been characterized. For several of these, further study is needed for an atomic-level perspective, where resolved structures of RNA-protein complexes are awaiting investigation. Other features of post-transcriptional control are also summarized. These include: RNA structure at translation initiation regions that either inhibit or promote translation initiation; programmed translational bypassing, where T4 orchestrates ribosome bypass of a 50 nucleotide mRNA sequence; phage exclusion systems that involve T4-mediated activation of a latent endoribonuclease (PrrC) and cofactor-assisted activation of EF-Tu proteolysis (Gol-Lit); and potentially important findings on ADP-ribosylation (by Alt and Mod enzymes) of ribosome-associated proteins that might broadly impact protein synthesis in the infected cell. Many of these problems can continue to be addressed with T4, whereas the growing database of T4-related phage genome sequences provides new resources and potentially new phage-host systems to extend the work into a broader biological, evolutionary context.
    Sensor: Monitor the integrity of the bacterial cell machinery
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF00270, PF02384, PF04313, PF04851, PF12008, PF12161, PF13166, PF18766
contributors:
    - Ernest Mordret
relevant abstracts:
    - doi: 10.1186/1743-422X-7-360
    - doi: 10.1006/jmbi.1995.0343
---

# PrrC

## Description

The PrrC system protects bacteria against phages via an abortive infection. It is composed of a single effector protein but relies on the presence of a full type Ic restriction-modification system in the vicinity. PrrC proteins are therefore typically found embedded in a larger RM system.

## Molecular mechanism
The effector protein prrC complements a RM system by cutting tRNALys in the anticodon loop, upstream of the wobble nucleotide and causes the arrest of phage protein synthesis and phage growth.
PrrC serves as a guardian of the activity of EcoprrI, which can be inactivated by the Stp peptide of phage T4 at the beginning of infection. Inactivation of EcoprrI by Stp induces a conformation change that in turn activates PrrC, releasing its nuclease activity and stalling host and phage growth :ref{doi=10.1006/jmbi.1995.0343}.
Because it sabotages the host's translation machinery, PrrC is considered to be an abortive infection system. 


## Example of genomic structure

The PrrC is composed of the PrrC protein and a type I restriction-modification system.

Here is an example found in the RefSeq database:

![prrc](/prrc/PrrC.svg){max-width=750px}

The PrrC system in *Parazoarcus communis* (GCF_003111645.1, NZ_CP022187) is composed of 4 proteins Type_I_REases (WP_108949925.1) Type_I_S (WP_108949926.1) PrrC (WP_108949927.1) Type_I_MTases (WP_108949929.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Jabbar_1984[<a href='https://doi.org/10.1128/JVI.51.2.522-529.1984'>Jabbar and  Snyder, 1984</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012954793.1'>WP_012954793.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012954794.1'>WP_012954794.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_012954795.1'>WP_012954795.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012954796.1'>WP_012954796.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T4 & Dec8
    subgraph Title1[Reference]
        Jabbar_1984
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        Lambda
        T4
        Dec8
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

