---
title: AbiJ
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF14355
contributors:
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1111/j.1574-6968.1997.tb10185.x
---

# AbiJ

## Description

AbiJ was discovered in 1997 on a *Lactococcus lactis* biovar. diacetylactis  :ref{doi=10.1111/j.1574-6968.1997.tb10185.x}.

AbiJ is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiJ is classified as abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

AbiJ is composed of one protein AbiJ. AbiJ has some homology with [AbiC](/defense-systems/abic) (PF14355).

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown.

## Example of genomic structure

The AbiJ is composed of 1 protein: AbiJ.

Here is an example found in the RefSeq database:

![abij](/abij/AbiJ.svg){max-width=750px}

The AbiJ system in *Marinobacter sp. LQ44* (GCF_001447155.2, NZ_CP014754) is composed of 1 protein: AbiJ (WP_058091398.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid  
<a href='https://ncbi.nlm.nih.gov/protein/AAA99069.1'>AAA99069.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

