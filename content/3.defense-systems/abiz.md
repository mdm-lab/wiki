---
title: AbiZ
layout: article
tableColumns:
    article:
      doi: 10.1128/JB.00904-06
      abstract: |
        The conjugative plasmid pTR2030 has been used extensively to confer phage resistance in commercial Lactococcus starter cultures. The plasmid harbors a 16-kb region, flanked by insertion sequence (IS) elements, that encodes the restriction/modification system LlaI and carries an abortive infection gene, abiA. The AbiA system inhibits both prolate and small isometric phages by interfering with the early stages of phage DNA replication. However, abiA alone does not account for the full abortive activity reported for pTR2030. In this study, a 7.5-kb region positioned within the IS elements and downstream of abiA was sequenced to reveal seven additional open reading frames (ORFs). A single ORF, designated abiZ, was found to be responsible for a significant reduction in plaque size and an efficiency of plaquing (EOP) of 10?6, without affecting phage adsorption. AbiZ causes phage ?31-infected Lactococcus lactis NCK203 to lyse 15 min early, reducing the burst size of ?31 100-fold. Thirteen of 14 phages of the P335 group were sensitive to AbiZ, through reduction in either plaque size, EOP, or both. The predicted AbiZ protein contains two predicted transmembrane helices but shows no significant DNA homologies. When the phage ?31 lysin and holin genes were cloned into the nisin-inducible shuttle vector pMSP3545, nisin induction of holin and lysin caused partial lysis of NCK203. In the presence of AbiZ, lysis occurred 30 min earlier. In holin-induced cells, membrane permeability as measured using propidium iodide was greater in the presence of AbiZ. These results suggest that AbiZ may interact cooperatively with holin to cause premature lysis.
    Sensor: Unknown
    Activator: Unknown
    Effector: Membrane disrupting
contributors: 
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1128/JB.00904-06
---

# AbiZ

## Description

AbiZ was discovered in 2006 on a *Lactococcus lactis* conjugative plasmid pTR2030 :ref{doi=10.1128/JB.00904-06}.

AbiZ is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiZ is classified as abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

AbiZ is composed of a single protein AbiZ.

## Molecular mechanism
AbiZ acts as an abortive infection system. AbiZ proteins sense holins and lysins from the infecting bacteriophage and use them to induce premature cell lysis :ref{doi=10.1128/JB.00904-06,10.1146/annurev-virology-011620-040628}.  

## Example of genomic structure

The AbiZ is composed of 1 protein: AbiZ.

Here is an example found in the RefSeq database:

![abiz](/abiz/AbiZ.svg){max-width=750px}

The AbiZ system in *Fusobacterium pseudoperiodonticum* (GCF_002763695.1, NZ_CP024700) is composed of 1 protein: AbiZ (WP_099987858.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Durmaz_2007[<a href='https://doi.org/10.1128/JB.00904-06'>Durmaz and  Klaenhammer, 2007</a>] --> Origin_0
    Origin_0[Lactococcus lactis 
<a href='https://ncbi.nlm.nih.gov/protein/ABI93964.1'>ABI93964.1</a>] --> Expressed_0[Lactococcus lactis]
    Expressed_0[Lactococcus lactis] ----> Phi31.2 & ul36 & phi31 & phi48 & phi31.1 & Q30 & Q36 & Q33 & phi50 & phi48
    subgraph Title1[Reference]
        Durmaz_2007
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        Phi31.2
        ul36
        phi31
        phi48
        phi31.1
        Q30
        Q36
        Q33
        phi50
        phi48
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

