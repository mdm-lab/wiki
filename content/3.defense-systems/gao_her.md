---
title: Gao_Her
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01935, PF10412, PF13289
contributors: 
  - To do
relevantAbstracts:
  - doi: 10.1126/science.aba0372
  - doi: 10.1016/j.molcel.2023.11.007
  - doi: 10.1016/j.molcel.2023.11.010
---

# Gao_Her
## Example of genomic structure

A total of 2 subsystems have been described for the Gao_Her system.

Here are some examples found in the RefSeq database:

![gao_her_duf](/gao_her/Gao_Her_DUF.svg){max-width=750px}

The Gao_Her_DUF system in *Escherichia coli* (GCF_023657935.1, NZ_CP098183) is composed of 2 proteins DUF4297 (WP_064484828.1) HerA_DUF (WP_064484829.1) 

![gao_her_sir](/gao_her/Gao_Her_SIR.svg){max-width=750px}

The Gao_Her_SIR system in *Xanthomonas citri* (GCF_018831325.1, NZ_CP029270) is composed of 2 proteins SIR2 (WP_046831681.1) HerA_SIR2 (WP_046831680.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ SIR2 + HerA
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_021577682.1'>WP_021577682.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_021577683.1'>WP_021577683.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T3 & T7 & PhiV-1
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_1
    Origin_1[ DUF4297 + HerA
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_016239655.1'>WP_016239655.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_016239654.1'>WP_016239654.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T4 & P1 & Lambda & T3 & T7
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        Lambda
        T3
        T7
        PhiV-1
        T4
        P1
        Lambda
        T3
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

