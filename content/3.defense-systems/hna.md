---
title: Hna
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2023.01.010
      abstract: |
        There is strong selection for the evolution of systems that protect bacterial populations from viral attack. We report a single phage defense protein, Hna, that provides protection against diverse phages in Sinorhizobium meliloti, a nitrogen-fixing alpha-proteobacterium. Homologs of Hna are distributed widely across bacterial lineages, and a homologous protein from Escherichia coli also confers phage defense. Hna contains superfamily II helicase motifs at its N terminus and a nuclease motif at its C terminus, with mutagenesis of these motifs inactivating viral defense. Hna variably impacts phage DNA replication but consistently triggers an abortive infection response in which infected cells carrying the system die but do not release phage progeny. A similar host cell response is triggered in cells containing Hna upon expression of a phage-encoded single-stranded DNA binding protein (SSB), independent of phage infection. Thus, we conclude that Hna limits phage spread by initiating abortive infection in response to a phage protein.
    PFAM: PF00270, PF04851, PF13307
relevantAbstracts:
    - doi: 10.1016/j.chom.2023.01.010
---

# Hna

## To do 

## Example of genomic structure

The Hna is composed of 1 protein: Hna.

Here is an example found in the RefSeq database:

![hna](/hna/Hna.svg){max-width=750px}

The Hna system in *Aggregatimonas sangjinii* (GCF_005943945.1, NZ_CP040710) is composed of 1 protein: Hna (WP_138852947.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Sinorhizobium meliloti 
<a href='https://ncbi.nlm.nih.gov/protein/AAK65868.1'>AAK65868.1</a>] --> Expressed_0[Sinorhizobium meliloti]
    Expressed_0[Sinorhizobium meliloti] ----> 5A & 3K
    Sather_2023[<a href='https://doi.org/10.1016/j.chom.2023.01.010'>Sather et al., 2023</a>] --> Origin_1
    Origin_1[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RDP84117.1'>RDP84117.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T3 & T4 & T7 & HK97
    subgraph Title1[Reference]
        Doron_2018
        Sather_2023
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        5A
        3K
        T3
        T4
        T7
        HK97
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

