---
title: Septu
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13175, PF13304, PF13476
contributors: 
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1016/j.cell.2020.09.065
    - doi: 10.1093/nar/gkab883
    - doi: 10.1126/science.aar4120
    - doi: 10.1038/s41594-023-01172-8
---
# Septu

## Description
The Septu defense system was discovered in 2018 in *Bacillus* :ref{doi=10.1126/science.aar4120}. The name of the system is related to Septu (or Sopdu) the ancient Egypt god of the sky and eastern border regions.

The Septu defense system is composed of two proteins PtuA and PtuB: PtuA encoding for an ATPase (AAA15/AAA21) and PtuB with an HNH endonuclease domain.

After the discovery of Septu, two studies discovered separately another form of Septu :ref{doi=10.1093/nar/gkab883,10.1016/j.cell.2020.09.065}. This new subtype encodes a third protein with a reverse transcriptase domain. This system is either referred to as Septu Type II :ref{doi=10.1093/nar/gkab883} or as [Retron](/defense-systems/retron) subtype RT-I-A :ref{doi=10.1016/j.cell.2020.09.065,10.1016/j.cell.2020.09.065}.

The Septu defense system is part of a large category of systems that encodes the AAA15/21 ABC ATPase :ref{doi=10.1111/mmi.15074}.

## Molecular mechanism
The structure of the PtuAB complex shows a stoichiometry of 6:2 (A:B) :ref{doi=10.1038/s41594-023-01172-8}.
The 6 PtuA proteins form a horseshoe-like trimer of dimers.
The CTD terminal domains of the external PtuA bind to the PtuB endonuclease.

The PtuB is an HNH endonuclease like  Cas9 :ref{doi=10.1016/j.cell.2014.02.001} that cleaves the phage DNA :ref{doi=10.1038/s41594-023-01172-8}.

Interestingly, the ATPase domain of PtuA has no ATPase activity. However, the experimentally determined structure shows that 4 ATPs bind inside the PtuA hexamer :ref{doi=10.1038/s41594-023-01172-8}. The authors then show that the endonuclease activity of PtuAB is down-regulated by ATP in a concentration-dependent manner.

Authors :ref{doi=10.1038/s41594-023-01172-8} hypothesize that the PtuAB complex is inactive in noninfected cells by the physiological concentration of ATP. During phage infection, the pool of ATP can be depleted leading to the activation of the PtuAB complex and the phage DNA cleavage by PtuB HNH endonuclease.

## Example of genomic structure

The Septu is composed of 2 proteins: PtuA and PtuB.

Here is an example found in the RefSeq database:

![septu](/septu/Septu.svg){max-width=750px}

The Septu system in *Burkholderia pyrrocinia* (GCF_001028665.1, NZ_CP011503) is composed of 2 proteins PtuB (WP_148669058.1) PtuA (WP_082146056.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus thuringiensis 
<a href='https://ncbi.nlm.nih.gov/protein/AMR85048.1'>AMR85048.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AMR85049.1'>AMR85049.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SBSphiJ & SBSphiC
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[Bacillus weihenstephanensis 
<a href='https://ncbi.nlm.nih.gov/protein/ABY44616.1'>ABY44616.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ABY44615.1'>ABY44615.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> SBSphiC & SpBeta
    Yi_2024[<a href='https://doi.org/10.1038/s41594-023-01172-8'>Yi et al., 2024</a>] --> Origin_2
    Origin_2[Escherichia coli
<a href='https://ncbi.nlm.nih.gov/protein/AIL15948.1'>AIL15948.1</a>, <a href:'https://ncbi.nlm.nih.gov/protein/AIL15172.1>AIL15172.1</
a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T5 & T7
    subgraph Title1[Reference]
        Doron_2018
        Yi_2024
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Protects against]
        SBSphiJ
        SBSphiC
        SBSphiC
        SpBeta
        T5
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

