---
title: FS_GIY_YIG
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2022.07.014
      abstract: |
        Bacteria encode sophisticated anti-phage systems that are diverse and versatile and display high genetic mobility. How this variability and mobility occurs remains largely unknown. Here, we demonstrate that a widespread family of pathogenicity islands, the phage-inducible chromosomal islands (PICIs), carry an impressive arsenal of defense mechanisms, which can be disseminated intra- and inter-generically by helper phages. These defense systems provide broad immunity, blocking not only phage reproduction, but also plasmid and non-cognate PICI transfer. Our results demonstrate that phages can mobilize PICI-encoded immunity systems to use them against other mobile genetic elements, which compete with the phages for the same bacterial hosts. Therefore, despite the cost, mobilization of PICIs may be beneficial for phages, PICIs, and bacteria in nature. Our results suggest that PICIs are important players controlling horizontal gene transfer and that PICIs and phages establish mutualistic interactions that drive bacterial ecology and evolution.
contributors:
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1016/j.cell.2022.07.014
---

# FS_GIY_YIG

## Description

FS_GIY_YIG or GIY_YIG system was discovered in 2022 :ref{doi=10.1016/j.cell.2022.07.014} inside the PICI KpCIXH209 in *Klebsiella pneumoniae*.

GIY_YIG is a single gene system that encodes for the GIY_YIG protein. The GIY_YIG protein contains the GIY YIG endonuclease domain.


## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown. The endonuclease domain suggests a nucleic acid degrading mechanism.

## Example of genomic structure

The FS_GIY_YIG is composed of 1 protein: GIY_YIG.

Here is an example found in the RefSeq database:

![fs_giy_yig](/fs_giy_yig/FS_GIY_YIG.svg){max-width=750px}

The FS_GIY_YIG system in *Klebsiella pneumoniae* (GCF_021389995.1, NZ_CP089989) is composed of 1 protein: GIY_YIG (WP_053067640.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_071843248.1'>WP_071843248.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> HK97 & HK544
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_071843248.1'>WP_071843248.1</a>] --> Expressed_1[Salmonella enterica]
    Expressed_1[Salmonella enterica] ----> P22 & BTP1
    subgraph Title1[Reference]
        Fillol-Salom_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        HK97
        HK544
        P22
        BTP1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

