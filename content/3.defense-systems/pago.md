---
title: pAgo
layout: article
tableColumns:
    article:
      doi: 10.1186/1745-6150-4-29
      abstract: |
        BACKGROUND: In eukaryotes, RNA interference (RNAi) is a major mechanism of defense against viruses and transposable elements as well of regulating translation of endogenous mRNAs. 
        The RNAi systems recognize the target RNA molecules via small guide RNAs that are completely or partially complementary to a region of the target. 
        Key components of the RNAi systems are proteins of the Argonaute-PIWI family some of which function as slicers, 
        the nucleases that cleave the target RNA that is base-paired to a guide RNA. 
        Numerous prokaryotes possess the CRISPR-associated system (CASS) of defense against phages and plasmids that is, in part, 
        mechanistically analogous but not homologous to eukaryotic RNAi systems. 
        Many prokaryotes also encode homologs of Argonaute-PIWI proteins but their functions remain unknown. 
        RESULTS: We present a detailed analysis of Argonaute-PIWI protein sequences and the genomic neighborhoods of the respective genes in prokaryotes. 
        Whereas eukaryotic Ago/PIWI proteins always contain PAZ (oligonucleotide binding) and PIWI (active or inactivated nuclease) domains, the prokaryotic Argonaute homologs (pAgos) fall into two major groups in which the PAZ domain is either present or absent. The monophyly of each group is supported by a phylogenetic analysis of the conserved PIWI-domains. Almost all pAgos that lack a PAZ domain appear to be inactivated, and the respective genes are associated with a variety of predicted nucleases in putative operons. An additional, uncharacterized domain that is fused to various nucleases appears to be a unique signature of operons encoding the short (lacking PAZ) pAgo form. By contrast, almost all PAZ-domain containing pAgos are predicted to be active nucleases. Some proteins of this group (e.g., that from Aquifex aeolicus) have been experimentally shown to possess nuclease activity, and are not typically associated with genes for other (putative) nucleases. Given these observations, the apparent extensive horizontal transfer of pAgo genes, and their common, statistically significant over-representation in genomic neighborhoods enriched in genes encoding proteins involved in the defense against phages and/or plasmids, we hypothesize that pAgos are key components of a novel class of defense systems. The PAZ-domain containing pAgos are predicted to directly destroy virus or plasmid nucleic acids via their nuclease activity, whereas the apparently inactivated, PAZ-lacking pAgos could be structural subunits of protein complexes that contain, as active moieties, the putative nucleases that we predict to be co-expressed with these pAgos. All these nucleases are predicted to be DNA endonucleases, so it seems most probable that the putative novel phage/plasmid-defense system targets phage DNA rather than mRNAs. Given that in eukaryotic RNAi systems, the PAZ domain binds a guide RNA and positions it on the complementary region of the target, we further speculate that pAgos function on a similar principle (the guide being either DNA or RNA), and that the uncharacterized domain found in putative operons with the short forms of pAgos is a functional substitute for the PAZ domain. CONCLUSION: The hypothesis that pAgos are key components of a novel prokaryotic immune system that employs guide RNA or DNA molecules to degrade nucleic acids of invading mobile elements implies a functional analogy with the prokaryotic CASS and a direct evolutionary connection with eukaryotic RNAi. The predictions of the hypothesis including both the activities of pAgos and those of the associated endonucleases are readily amenable to experimental tests.
    Sensor: Detecting invading nucleic acid
    Activator: Direct
    Effector: Diverse (Nucleotide modifyingn, Membrane disrupting)
    PFAM: PF02171, PF13289, PF13676, PF14280, PF18742
contributors:
  - Daan Swarts
relevantAbstracts:
  - doi: 10.1016/j.cell.2022.03.012
  - doi: 10.1016/j.chom.2022.04.015
  - doi: 10.1038/s41564-022-01207-8
  - doi: 10.1038/s41586-020-2605-1
  - doi: 10.1186/1745-6150-4-29
  - doi: 10.1038/s41564-022-01239-0
---

# pAgo

## Description
Argonaute proteins comprise a diverse protein family and can be found in both prokaryotes and eukaryotes :ref{doi=10.1016/j.mib.2023.102313}. Despite low sequence conservation, eAgos and long pAgos generally have a conserved domain architecture and share a common mechanism of action; they use a 5'-phosphorylated single-stranded nucleic acid guide (generally 15-22 nt in length) to target complementary nucleic acid sequences :ref{doi=10.1038/nsmb.2879} eAgos strictly mediate RNA-guided RNA silencing, while pAgos show higher mechanistic diversification, and can make use of guide RNAs and/or single-stranded guide DNAs to target RNA and/or DNA targets :ref{doi=10.1016/j.mib.2023.102313}. Depending on the presence of catalytic residues and the degree of complementarity between the guide and target sequences, eAgo and pAgos either cleave the target or recruit and/or activate accessory proteins. This can result in degradation of the target nucleic acid, but might also trigger alternative downstream effects, ranging from poly(A) tail shortening and RNA decapping :ref{doi=10.1016/J.CELL.2018.03.006} or chromatin formation in eukaryotes :ref{doi=10.1038/s41580-022-00528-0}, to abortive infection in prokaryotes :ref{doi=10.1016/j.tcb.2022.10.005}.

## Molecular mechanism

Based on their phylogeny, Agos have been subdivided into various (sub)clades. eAgos are generally subdivided into the AGO and PIWI clades, but these will not be discussed further here. pAgos can be further subdivided into long-A pAgos, long-B pAgos, short pAgos, SiAgo-like pAgos, and PIWI-RE proteins :ref{doi=10.1128/mBio.01935-18,10.1016/j.mib.2023.102313,10.1016/j.tcb.2022.10.005,10.1186/1745-6150-8-13,10.1186/1745-6150-4-29}. Below, we briefly outline the general mechanism of pAgos that have a demonstrated role in host defense.

### Long-A pAgos
Akin to eAgos, most long A-pAgos characterized to date have a N-L1-PAZ-L2-MID-PIWI domain architecture :ref{doi=10.1038/nsmb.2879}. In contrast to eAgos, however, certain long-A pAgos use a single-stranded guide DNA to bind and cleave complementary target DNA sequences :ref{doi=10.1093/nar/gkz306,10.1093/nar/gkz379,10.1038/s41586-020-2605-1,10.1093/nar/gkv415,10.1038/nature12971,10.1038/nmicrobiol.2017.35}. Long-A pAgos are preferentially programmed with guide DNAs targeting invading DNA through a poorly understood mechanism, which might involve DNA repair proteins :ref{doi=10.1038/s41586-020-2605-1} or the pAgo itself :ref{doi=10.1016/j.molcel.2017.01.033,10.1038/nmicrobiol.2017.34}. Most long-A pAgos have an intact catalytic site in the PIWI domain which allows to cleave their targets :ref{doi=10.1073/pnas.1321032111}. As such, they act as an innate immune system that clears plasmid and phage DNA from the cell :ref{doi=10.1093/nar/gkz379,10.1038/s41586-020-2605-1,10.1093/nar/gkv415,10.1038/nature12971,10.1093/nar/gkad290}. 

Within the long-A pAgo clade, various subclades of other pAgos exist that rely on distinct function mechanisms. For example, various long-A pAgo can (additionally) use guide RNAs and/or cleave RNA targets. Furthermore, CRISPR-associated pAgos use 5'-OH guide RNAs to target DNA :ref{doi=10.1073/pnas.1524385113}, and PliAgo-like pAgos use small DNA guides to target RNA :ref{doi=10.1038/s41467-022-32079-5}. Certain long-A pAgos genetically co-localize with other putative enzymes including (but not limited to) putative nucleases, helicases, DNA-binding proteins, or PLD-like proteins :ref{doi=10.1038/nsmb.2879,10.1128/mBio.01935-18}. The relevance of these associations is currently unknown. 

### Long-B pAgos
Akin to long-A pAgogs, long B-pAgos have a N-L1-PAZ-L2-MID-PIWI domain composition, but most have a shorter PAZ* domain, and in contrast to long-A pAgos all long-B pAgos are catalytically inactive :ref{doi=10.1128/mBio.01935-18}. Long-B pAgos characterized to date use guide RNAs to bind invading DNA :ref{doi=10.1038/s41598-023-32600-w,10.1016/j.molcel.2013.08.014,10.1038/s41467-023-42793-3}. In the absence of co-encoded proteins, long-B pAgos repress invader activity :ref{doi=10.1016/j.molcel.2013.08.014}. In addition, most long-B pAgos are co-encoded with effector proteins including (but not limited to) SIR2, nucleases, membrane proteins, and restriction endonucleases :ref{doi=10.1038/nsmb.2879,10.1128/mBio.01935-18,10.1186/1745-6150-4-29,10.1038/s41467-023-42793-3}. These effector proteins are activated upon pAgo-mediated invader detection, and generally catalyze reactions that result in cell death :ref{doi=10.1038/s41467-023-42793-3}. As such, long-B pAgo together with their associated proteins mediate abortive infection. 

### Short pAgos
Short pAgos are truncated: they only contain the MID and PIWI domains essential for guide-mediate target binding :ref{doi=10.1016/j.tcb.2022.10.005}. They are catalytically inactive and are co-encoded with an APAZ domain that is fused to one of the various effector domains. In short pAgo systems characterized to date, the short pAgo and the APAZ domain-containing protein form a heterodimeric complex :ref{doi=10.1016/j.cell.2022.03.012,10.1038/s41564-022-01239-0}. Within this complex, the short pAgo uses a guide RNA to bind complementary target DNAs. This triggers catalytic activation of the effector domain fused to the APAZ domain, generally resulting in cell death :ref{doi=10.1016/j.cell.2022.03.012,10.1038/s41564-022-01239-0}. As such, short pAgo systems mediate abortive infection. 

Based on their phylogeny, short pAgos are subdivided in S1A, S1B, S2A, and S2B clades :ref{doi=10.1128/mBio.01935-18,10.1016/j.tcb.2022.10.005}. In clade S1A and S1B (SPARSA) systems, APAZ is fused to a SIR2 domain. In clade S2A (SPARTA) systems, APAZ is fused to a TIR domain. Both SPARSA and SPARTA systems trigger cell death by depletion of NAD(P)+  :ref{doi=10.1016/j.cell.2022.03.012,10.1038/s41564-022-01239-0}. In S2B clade systems, APAZ is fused to one or more effector domains, including Mrr-like, DUF4365, RecG/DHS-like and other domains. In all clade S1A SPARSA systems, but also for certain other systems within other clades, the effector-APAZ is fused to the short pAgo. 

### Pseudo-short pAgos
Akin to short pAgos, pseudo-short pAgos are comprised of the MID and PIWI domains only :ref{doi=10.1016/j.tcb.2022.10.005}. However, they do not phylogenetically cluster with canonical short pAgos and do not colocalize with effector-APAZ proteins. Instead, certain pseudo-short are found across the long-A and long-B pAgo clades (e.g. *Archaeoglobus fulgidus* pAgo, a truncated long-B pAgo :ref{doi=10.1038/s41598-023-32600-w,10.1038/s41598-021-83889-4}), while others form a distinct branch in the phylogenetic pAgo tree (see SiAgo-like pAgos below).  

### SiAgo-like pAgos
SiAgo-like pAgos are pseudo-short pAgos that form a separate branch in the phylogenetic tree of pAgos. They are named after the type system from *Sulfolobus islandicus* :ref{doi=10.1016/j.chom.2022.04.015}. SiAgo is comprised of MID and PIWI domains and is co-encoded with Ago-associated proteins Aga1 and Aga2. SiAgo and Aga1 form a cytoplasmic heterodimeric complex. While it is currently unknown what guide/target types activate the SiAgo/Aga1 complex, it is directed toward membrane-localized Aga2 upon viral infection. This triggers Aga2-mediated membrane depolarization and causes cell death :ref{doi=10.1016/j.chom.2022.04.015}. 


## Example of genomic structure

A total of 6 subsystems have been described for the pAgo system.
Here are some examples found in the RefSeq database:

![pago_longa](/pago/pAgo_LongA.svg){max-width=750px}

The pAgo_LongA system in *Halosimplex pelagicum* (GCF_013415905.1, NZ_CP058909) is composed of 1 protein: pAgo_LongA (WP_179918860.1) 

![pago_longb](/pago/pAgo_LongB.svg){max-width=750px}

The pAgo_LongB system in *Serratia fonticola* (GCF_019252525.1, NZ_CP072742) is composed of 2 proteins pAgo_LongB (WP_218520044.1) EcAgaN (WP_235784821.1) 

![pago_s1a](/pago/pAgo_S1A.svg){max-width=750px}

The pAgo_S1A system in *Parabacteroides merdae* (GCF_020735605.1, NZ_CP085927) is composed of 2 proteins pAgo_S1A (WP_227945673.1) pAgo_S1A (WP_227945674.1) 

![pago_s1b](/pago/pAgo_S1B.svg){max-width=750px}

The pAgo_S1B system in *Comamonas flocculans* (GCF_007954405.1, NZ_CP042344) is composed of 2 proteins SIR2APAZ (WP_146914209.1) pAgo_S1B (WP_146913473.1) 

![pago_s2b](/pago/pAgo_S2B.svg){max-width=750px}

The pAgo_S2B system in *Granulicella tundricola* (GCF_000178975.2, NC_015064) is composed of 2 proteins XAPAZ (WP_013581437.1) pAgo_S2B (WP_013581438.1) 

![pago_sparta](/pago/pAgo_SPARTA.svg){max-width=750px}

The pAgo_SPARTA system in *Roseivivax sp. THAF30* (GCF_009363575.1, NZ_CP045389) is composed of 2 proteins TIRAPAZ (WP_152461295.1) pAgo_SPARTA (WP_152461296.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Kuzmenko_2020[<a href='https://doi.org/10.1038/s41586-020-2605-1'>Kuzmenko et al., 2020</a>] --> Origin_0
    Origin_0[ Ago
Clostridium butyricum 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045143632.1'>WP_045143632.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> M13 & P1vir
    Xing_2022[<a href='https://doi.org/10.1128/mbio.03656-21'>Xing et al., 2022</a>] --> Origin_1
    Origin_1[Natronobacterium gregoryi 
<a href='https://ncbi.nlm.nih.gov/protein/WP_005580376.1'>WP_005580376.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_2
    Origin_2[ GsSir2/Ago
Geobacter sulfurreducens 
<a href='https://ncbi.nlm.nih.gov/protein/WP_010942012.1'>WP_010942012.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_010942011.1'>WP_010942011.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> LambdaVir & SECphi27
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_2
    Origin_2[ GsSir2/Ago
Geobacter sulfurreducens 
<a href='https://ncbi.nlm.nih.gov/protein/WP_010942012.1'>WP_010942012.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_010942011.1'>WP_010942011.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> LambdaVir & SECphi27
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_3
    Origin_3[ CcSir2/Ago
Caballeronia cordobensis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_053571900.1'>WP_053571900.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_053571899.1'>WP_053571899.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> LambdaVir
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_4
    Origin_4[ PgSir2/Ago
Paraburkholderia graminis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_006053074.1'>WP_006053074.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> LambdaVir & SECphi27
    Lisitskaya_2022[<a href='https://doi.org/10.1093/nar/gkad290'>Lisitskaya et al., 2023</a>] --> Origin_5
    Origin_5[ Ago
Exiguobacterium marinum] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> P1vir
    Garb_2022[<a href='https://doi.org/10.1038/s41564-022-01207-8'>Garb et al., 2022</a>] --> Origin_6
    Origin_6[ Sir2/Ago
Geobacter sulfurreducens 
<a href='https://ncbi.nlm.nih.gov/protein/NP_952413'>NP_952413</a>, <a href='https://ncbi.nlm.nih.gov/protein/NP_952414'>NP_952414</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> LambdaVir
    Zeng_2021[<a href='https://doi.org/10.1016/j.chom.2022.04.015'>Zeng et al., 2022</a>] --> Origin_7
    Origin_7[ SiAgo/Aga1/Aga2
Sulfolobus islandicus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012735993.1'>WP_012735993.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012718851.1'>WP_012718851.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_012735992.1'>WP_012735992.1</a>] --> Expressed_7[Sulfolobus islandicus]
    Expressed_7[Sulfolobus islandicus] ----> SMV1
    subgraph Title1[Reference]
        Kuzmenko_2020
        Xing_2022
        Zaremba_2022
        Lisitskaya_2022
        Garb_2022
        Zeng_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_2
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
end
    subgraph Title4[Protects against]
        M13
        P1vir
        T7
        LambdaVir
        SECphi27
        LambdaVir
        SECphi27
        LambdaVir
        LambdaVir
        SECphi27
        P1vir
        LambdaVir
        SMV1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


