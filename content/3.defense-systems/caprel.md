---
title: CapRel
layout: article
tableColumns:
    article:
      doi: 10.1038/s41586-022-05444-z
      abstract: |
        Bacteria have evolved diverse immunity mechanisms to protect themselves against the constant onslaught of bacteriophages. Similar to how eukaryotic innate immune systems sense foreign invaders through pathogen-associated molecular patterns4 (PAMPs), many bacterial immune systems that respond to bacteriophage infection require phage-specific triggers to be activated. However, the identities of such triggers and the sensing mechanisms remain largely unknown. Here we identify and investigate the anti-phage function of CapRelSJ46, a fused toxini-antitoxin system that protects Escherichia coli against diverse phages. Using genetic, biochemical and structural analyses, we demonstrate that the C-terminal domain of CapRelSJ46 regulates the toxic N-terminal region, serving as both antitoxin and phage infection sensor. Following infection by certain phages, newly synthesized major capsid protein binds directly to the C-terminal domain of CapRelSJ46 to relieve autoinhibition, enabling the toxin domain to pyrophosphorylate tRNAs, which blocks translation to restrict viral infection. Collectively, our results reveal the molecular mechanism by which a bacterial immune system directly senses a conserved, essential component of phages, suggesting a PAMP-like sensing model for toxin-antitoxin-mediated innate immunity in bacteria. We provide evidence that CapRels and their phage-encoded triggers are engaged in a "Red Queen conflict", revealing a new front in the intense coevolutionary battle between phages and bacteria. Given that capsid proteins of some eukaryotic viruses are known to stimulate innate immune signalling in mammalian hosts, our results reveal a deeply conserved facet of immunity.
    Sensor: Sensing of phage protein
    Activator: Direct
    Effector: Nucleic acid degrading (pyrophosphorylates tRNAs)
    PFAM: PF04607
contributors: 
  - Héloïse Georjon
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1038/s41586-022-05444-z
---

# CapRel
## Description

CapRel is a fused toxin-antitoxin system that is active against diverse phages when expressed in *Escherichia coli* :ref{doi=10.1038/s41586-022-05444-z}. CapRel belongs to the family of toxSAS toxin-antitoxin systems. CapRel is an Abortive infection system that is found in Cyanobacteria, Actinobacteria, Proteobacteria, Spirochetes, Bacteroidetes, and Firmicutes, as well as in some temperate phages.

## Molecular mechanism

The CapRel system of Salmonella temperate phage SJ46 is normally found in a closed conformation, which is thought to maintain CapRel in an auto-inhibited state. However, during phage SECPhi27 infection, binding of the major phage capsid protein (Gp57) to CapRel releases it from is inhibited state, allowing pyrophosphorylation of tRNAs by the toxin domain and resulting in translation inhibition :ref{doi=10.1038/s41586-022-05444-z}. Other phage capsid proteins can be recognized by CapRel, as observed during infection by phage Bas8.


Different CapRel homologs confer defense against different phages, suggesting variable phage specificity of CapRel system which seems to be mediated by the C-terminal region of CapRel. 


## Example of genomic structure

The CapRel is composed of 1 protein: CapRel.

Here is an example found in the RefSeq database:

![caprel](/caprel/CapRel.svg){max-width=750px}

The CapRel system in *Campylobacter sp. RM12175* (GCF_002139875.1, NZ_CP018793) is composed of 1 protein: CapRel (WP_086315994.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Zhang_2022[<a href='https://doi.org/10.1038/s41586-022-05444-z'>Zhang et al., 2022</a>] --> Origin_0
    Origin_0[Salmonella phage SJ46 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001749390.1'>WP_001749390.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & RB69 & SECphi27
    Zhang_2022[<a href='https://doi.org/10.1038/s41586-022-05444-z'>Zhang et al., 2022</a>] --> Origin_1
    Origin_1[Enterobacter chengduensis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001749390.1'>WP_001749390.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7
    Zhang_2022[<a href='https://doi.org/10.1038/s41586-022-05444-z'>Zhang et al., 2022</a>] --> Origin_2
    Origin_2[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_0235711397.1'>WP_0235711397.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> SECphi18
    subgraph Title1[Reference]
        Zhang_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
        RB69
        SECphi27
        T7
        SECphi18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
