---
title: SspBCDE
layout: article
tableColumns:
    article:
      doi: 10.1128/mBio.00613-21
      abstract: |
        Unlike nucleobase modifications in canonical restriction-modification systems, DNA phosphorothioate (PT) epigenetic modification occurs in the DNA sugar-phosphate backbone when the nonbridging oxygen is replaced by sulfur in a double-stranded (ds) or single-stranded (ss) manner governed by DndABCDE or SspABCD, respectively. SspABCD coupled with SspE constitutes a defense barrier in which SspE depends on sequence-specific PT modifications to exert its antiphage activity. Here, we identified a new type of ssDNA PT-based SspABCD-SspFGH defense system capable of providing protection against phages through a mode of action different from that of SspABCD-SspE. We provide further evidence that SspFGH damages non-PT-modified DNA and exerts antiphage activity by suppressing phage DNA replication. Despite their different defense mechanisms, SspFGH and SspE are compatible and pair simultaneously with one SspABCD module, greatly enhancing the protection against phages. Together with the observation that the sspBCD-sspFGH cassette is widely distributed in bacterial genomes, this study highlights the diversity of PT-based defense barriers and expands our knowledge of the arsenal of phage defense mechanisms.IMPORTANCE We recently found that SspABCD, catalyzing single-stranded (ss) DNA phosphorothioate (PT) modification, coupled with SspE provides protection against phage infection. SspE performs both PT-simulated NTPase and DNA-nicking nuclease activities to damage phage DNA, rendering SspA-E a PT-sensing defense system. To our surprise, ssDNA PT modification can also pair with a newly identified 3-gene sspFGH cassette to fend off phage infection with a different mode of action from that of SspE. Interestingly, both SspFGH and SspE can pair with the same SspABCD module for antiphage defense, and their combination provides Escherichia coli JM109 with additive phage resistance up to 105-fold compared to that for either barrier alone. This agrees with our observation that SspFGH and SspE coexist in 36 bacterial genomes, highlighting the diversity of the gene contents and molecular mechanisms of PT-based defense systems.
    Sensor: Detecting invading nucleic acid
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF01507, PF01580, PF03235, PF07510, PF13182
relevantAbstracts:
    - doi: 10.1128/mBio.00613-21
    - doi: 10.1128/mBio.00613-21
---

# SspBCDE
## Example of genomic structure

The SspBCDE is composed of 7 proteins: SspB, SspC, SspD, SspE, SspF, SspG and SspH.

Here is an example found in the RefSeq database:

![sspbcde](/sspbcde/SspBCDE.svg){max-width=750px}

The SspBCDE system in *Vibrio fluvialis* (GCF_018140575.1, NZ_CP073273) is composed of 6 proteins SspB (WP_050492436.1) SspC (WP_212571168.1) SspD (WP_032481030.1) SspF (WP_212571169.1) SspG (WP_212571170.1) SspH (WP_212571171.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Xiong_2020[<a href='https://doi.org/10.1038/s41564-020-0700-6'>Xiong et al., 2020</a>] --> Origin_0
    Origin_0[ SspABCD+SspE
Vibrio cyclitrophicus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_022570853.1'>WP_022570853.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_016789109.1'>WP_016789109.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_016789110.1'>WP_016789110.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_016789111.1'>WP_016789111.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_016789113.1'>WP_016789113.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_016789114.1'>WP_016789114.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4 & T1 & JMPW1 & JMPW2 & EEP & T7
    Xiong_2020[<a href='https://doi.org/10.1038/s41564-020-0700-6'>Xiong et al., 2020</a>] --> Origin_1
    Origin_1[ SspBCD+SspE
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000429341.1'>WP_000429341.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000928407.1'>WP_000928407.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_000840786.1'>WP_000840786.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001424009.1'>WP_001424009.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T4 & T1 & JMPW1 & JMPW2 & EEP & T7 & PhiX174
    Xiong_2020[<a href='https://doi.org/10.1038/s41564-020-0700-6'>Xiong et al., 2020</a>] --> Origin_2
    Origin_2[ SspBCD+SspE
Streptomyces yokosukanensis
<a href='https://ncbi.nlm.nih.gov/protein/WP_067135675.1'>WP_067135675.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_037642090.1'>WP_037642090.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_208614377.1'>WP_208614377.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_067135521.1'>WP_067135521.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_067135523.1'>WP_067135523.1</a>] --> Expressed_2[Streptomyces lividans]
    Expressed_2[Streptomyces lividans] ----> JXY1
    Wang_2021[<a href='https://doi.org/10.1128/mBio.00613-21'>Wang et al., 2021</a>] --> Origin_3
    Origin_3[ SspBCD+SspFGH
Vibrio anguillarum 
<a href='https://ncbi.nlm.nih.gov/protein/WP_022570853.1'>WP_022570853.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_016789109.1'>WP_016789109.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_016789110.1'>WP_016789110.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_016789111.1'>WP_016789111.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_017048669.1'>WP_017048669.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/'></a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_017048667.1'>WP_017048667.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_017048666.1'>WP_017048666.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_017048665.1'>WP_017048665.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_017048664.1'>WP_017048664.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T1 & JMPW2 & T4 & EEP
    subgraph Title1[Reference]
        Xiong_2020
        Wang_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Protects against]
        T4
        T1
        JMPW1
        JMPW2
        EEP
        T7
        T4
        T1
        JMPW1
        JMPW2
        EEP
        T7
        PhiX174
        JXY1
        T1
        JMPW2
        T4
        EEP
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

