---
title: CARD_NLR
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.05.28.542683
      abstract: |
        Caspase recruitment domains (CARDs) and pyrin domains are important facilitators of inflammasome activity and pyroptosis. Upon pathogen recognition by NLR proteins, CARDs recruit and activate caspases, which, in turn, activate gasdermin pore forming proteins to and induce pyroptotic cell death. Here we show that CARD-like domains are present in defense systems that protect bacteria against phage. The bacterial CARD is essential for protease-mediated activation of certain bacterial gasdermins, which promote cell death once phage infection is recognized. We further show that multiple anti-phage defense systems utilize CARD-like domains to activate a variety of cell death effectors. We find that these systems are triggered by a conserved immune evasion protein that phages use to overcome the bacterial defense system RexAB, demonstrating that phage proteins inhibiting one defense system can activate another. We also detect a phage protein with a predicted CARD-like structure that can inhibit the CARD-containing bacterial gasdermin system. Our results suggest that CARD domains represent an ancient component of innate immune systems conserved from bacteria to humans, and that CARD-dependent activation of gasdermins is conserved in organisms across the tree of life.
    Sensor: Unknown
    Activator: Unknown
    Effector: Membrane disrupting or other
    PFAM: PF00082, PF00089, PF00614, PF01223, PF13091, PF13191, PF13365
contributors:
    - Marian Dominguez-Mirazo
relevantAbstract:
    - doi: 10.1101/2023.05.28.542683
---

# CARD_NLR
## Description
Pore-forming proteins called gasdermins control cell-death response to infection in animals. Gasdermins are also present in bacteria where they have been shown to act as an abortive infection system that permeabilizes the cell membrane before phage release :ref{doi=10.1126/science.abj8432,10.1101/2023.05.28.542683}. In *Lysobacter*, the gasdermin operon includes two genes encoding trypsin-like protease domains, and a gene encoding an ATPase domain :ref{doi=10.1101/2023.05.28.542683}. Intact active sites for the second protease and the ATPase, but not the first protease, are required for successful phage defense :ref{doi=10.1126/science.abj8432}. The domain architecture of the ATPase suggests it belongs to a protein family that is considered the ancestor of the eukaryotic  nucleotide oligomerization domain (NOD)-like receptor (NLR) protein family :ref{doi=10.1101/2023.05.28.542683}. In animals, NLR initiates the formation of the inflammasome complex :ref{doi=10.1126/science.abe3069}. The second protease contains a region with a similar structure to the human CARD domain :ref{doi=10.1101/2023.05.28.542683}. The CARD domain takes part in the assembly of immune protein complexes :ref{doi=10.1038/sj.cdd.4401890}. The CARD-like domain in the *Lysobacter* system is required for successful phage defense :ref{doi=10.1101/2023.05.28.542683}. Homology searches recovered multiple bacterial operons that include two proteases, one of them containing a CARD-like domain, and an NLR-like protein. In most cases, the effector gasdermin gene was replaced by another gene:ref{doi=10.1101/2023.05.28.542683}. The operon found in *Pedobacter rhizosphaerae* exhibits phage defense capabilities and contains a protein with phospholipase and endonuclease domains replacing the gasdermin gene. This system confers protection against the same phages as the _Lysobacter_ gasdermin-containing system, suggesting that the proteases and ATPase participate in phage specificity and recognition. 

## Molecular mechanisms
For the *Lysobacter* system, the effector has been described as a pore-formin protein that disrupts the cell membrane :ref{doi=10.1101/2023.05.28.542683}. To our knowledge, other parts of the molecular mechanisms have yet to be elucidated. 

## Example of genomic structure

A total of 5 subsystems have been described for the CARD_NLR system.

Here is some examples found in the RefSeq database:

![card_nlr_endonuclease](/card_nlr/CARD_NLR_Endonuclease.svg){max-width=750px}

The CARD_NLR_Endonuclease system in *Spirosoma montaniterrae* (GCF_001988955.1, NZ_CP014263) is composed of 4 proteins Trypsin (WP_077131797.1) CARD_Protease_new (WP_077131798.1) NLR_new (WP_157579217.1) Endonuclease (WP_083732853.1) 

![card_nlr_gasdermin](/card_nlr/CARD_NLR_GasderMIN.svg){max-width=750px}

The CARD_NLR_GasderMIN system in *Lysobacter enzymogenes* (GCF_005954665.1, NZ_CP040656) is composed of 4 proteins bGSDM (WP_057949280.1) Trypsin_new (WP_158229849.1) CARD_Protease (WP_175429318.1) NLR_new (WP_057949283.1) 

![card_nlr_phospho](/card_nlr/CARD_NLR_Phospho.svg){max-width=750px}

The CARD_NLR_Phospho system in *Achromobacter deleyi* (GCF_013116765.2, NZ_CP074375) is composed of 4 proteins Trypsin_Phospho (WP_171663676.1) Trypsin_new (WP_171663675.1) CARD_Protease_supposed (WP_171663674.1) NLR_new (WP_171663673.1) 

![card_nlr_subtilase](/card_nlr/CARD_NLR_Subtilase.svg){max-width=750px}

The CARD_NLR_Subtilase system in *Mesorhizobium sp. L-8-3* (GCF_016756615.1, NZ_AP023262) is composed of 4 proteins Subtilase (WP_202329294.1) Trypsin_new (WP_202305630.1) CARD_Protease_supposed (WP_202305632.1) NLR_IMG (WP_202329295.1) 

![card_nlr_like](/card_nlr/CARD_NLR_like.svg){max-width=750px}

The CARD_NLR_like system in *Sphingomonas sanguinis* (GCF_019297835.1, NZ_CP079203) is composed of 4 proteins Subtilase (WP_219018701.1) CARD_Protease_new (WP_219018702.1) NLR_new (WP_219018703.1) Endonuclease (WP_219021412.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

::info
This section is empty
::

