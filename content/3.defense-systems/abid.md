---
title: AbiD
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF07751
contributors:
  - Marian Dominguez-Mirazo
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1128/aem.61.5.2023-2026.1995
---

# AbiD

## Description

AbiD is a single gene system discovered in May 1995 in the plasmid pBF61 of *Lactococcus lactis* :ref{doi=10.1128/aem.61.5.2023-2026.1995}. An homolog of AbiD, named AbiD1 was discovered in July 1995 :ref{doi=10.1128/jb.177.13.3818-3823.1995}.

AbiD is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiR is classified as a possible abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

This antiphage defense system is very close to AbiD1 and AbiF :ref{doi=10.1128/aem.61.12.4321-4328.1995} and is categorized as the same system. Furthermore, AbiD is also detected as the [Abi2](/defense-systems/abi2) system.

### Molecular mechanism

For AbiD1 :ref{doi=10.1128/jb.177.13.3824-3829.1995} ([AAA79209](https://www.ncbi.nlm.nih.gov/protein/AAA79209)), the expression of the protein is toxic to the bacteria and is therefore highly regulated. Expression of the protein is triggered by the orf1 of phage bIL66 which results in inhibition of the phage RuvC-like endonuclease activity and stops phage multiplication.

## Example of genomic structure

The AbiD is composed of 1 protein: AbiD.

Here is an example found in the RefSeq database:

![abid](/abid/AbiD.svg){max-width=750px}

The AbiD system in *Idiomarina sp. OT37-5b* (GCF_002968395.1, NZ_CP027188) is composed of 1 protein: AbiD (WP_105307479.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid  
<a href='https://ncbi.nlm.nih.gov/protein/AAA63619.1'>AAA63619.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
