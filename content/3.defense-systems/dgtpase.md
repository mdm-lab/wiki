---
title: dGTPase
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01158-0
      abstract: |
        DNA viruses and retroviruses consume large quantities of deoxynucleotides (dNTPs) when replicating. The human antiviral factor SAMHD1 takes advantage of this vulnerability in the viral lifecycle, and inhibits viral replication by degrading dNTPs into their constituent deoxynucleosides and inorganic phosphate. Here, we report that bacteria use a similar strategy to defend against bacteriophage infection. We identify a family of defensive bacterial deoxycytidine triphosphate (dCTP) deaminase proteins that convert dCTP into deoxyuracil nucleotides in response to phage infection. We also identify a family of phage resistance genes that encode deoxyguanosine triphosphatase (dGTPase) enzymes, which degrade dGTP into phosphate-free deoxyguanosine and are distant homologues of human SAMHD1. Our results suggest that bacterial defensive proteins deplete specific deoxynucleotides (either dCTP or dGTP) from the nucleotide pool during phage infection, thus starving the phage of an essential DNA building block and halting its replication. Our study shows that manipulation of the dNTP pool is a potent antiviral strategy shared by both prokaryotes and eukaryotes.
    Sensor: Monitoring of the host cell machinery integrity
    Activator: Direct
    Effector: Nucleotide modifying
    PFAM: PF01966, PF13286
contributors: 
  - Aude Bernheim
relevantAbstracts:
  - doi:  10.1038/s41564-022-01158-0 
---

# dGTPase

## Description
dGTPase is a family of proteins discovered in :ref{doi=10.1038/s41564-022-01158-0}. It degrades dGTP into phosphate-free deoxyguanosine. It was suggested that these *"bacterial _defensive proteins deplete_ _deoxynucleotides__ from the_ nucleotide pool during phage infection, thus starving the phage of an essential DNA building block and halting its replication"*. The mechanism is remindful of the mechanism of SAMHD1 in humans.

## Molecular mechanism 
dGTPase degrades dGTP into phosphate-free deoxyguanosine. Phage mutants that overcome this defense carry mutations in phage-RNAP-modifying proteins suggesting, that *"phage-mediated inhibition of host transcription may be involved in triggering the activation of bacterial dNTP-depletion"*. 

## Example of genomic structure

The dGTPase is composed of 1 protein: Sp_dGTPase.

Here is an example found in the RefSeq database:

![dgtpase](/dgtpase/dGTPase.svg){max-width=750px}

The dGTPase system in *Citrobacter sp. RHBSTW-00986* (GCF_013783065.1, NZ_CP056202) is composed of 1 protein: Sp_dGTPase (WP_048216953.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2704680458'>2704680458</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_1
    Origin_1[Mesorhizobium ssp. 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2522901616'>2522901616</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_2
    Origin_2[Pseudoalteromonas luteoviolacea 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2731093246'>2731093246</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T2
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_3
    Origin_3[Shewanella putrefaciens 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2524134135'>2524134135</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T2 & T6 & T7 & SECphi17
    subgraph Title1[Reference]
        Tal_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Protects against]
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T2
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T2
        T6
        T7
        SECphi17
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

