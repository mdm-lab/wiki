---
title: Dnd
layout: article
tableColumns:
    article:
      doi: 10.1038/nchembio.2007.39
      abstract: |
        Modifications of the canonical structures of DNA and RNA play critical roles in cell physiology, DNA replication, transcription and translation in all organisms. We now report that bacterial dnd gene clusters incorporate sulfur into the DNA backbone as a sequence-selective, stereospecific phosphorothioate modification. To our knowledge, unlike any other DNA or RNA modification systems, DNA phosphorothioation by dnd gene clusters is the first physiological modification described on the DNA backbone.
    Sensor: Detecting invading nucleic acid
    Activator: Unknown
    Effector: Nucleic acid degrading
    PFAM: PF00266, PF01507, PF01935, PF08870, PF13476, PF14072
relevantAbstracts:
    - doi: 10.1038/nchembio.2007.39
    - doi: 10.1038/s41467-019-09390-9
---

# Dnd
## Example of genomic structure

A total of 2 subsystems have been described for the Dnd system.

Here is some examples found in the RefSeq database:

![dnd_abcde](/dnd/Dnd_ABCDE.svg){max-width=750px}

The Dnd_ABCDE system in *Streptomyces lividans* (GCF_000739105.1, NZ_CP009124) is composed of 5 proteins DndA (WP_016327563.1) DndB (WP_003972933.1) DndC (WP_003972934.1) DndD (WP_003972935.1) DndE (WP_003972936.1) 

![dnd_abcdefgh](/dnd/Dnd_ABCDEFGH.svg){max-width=750px}

The Dnd_ABCDEFGH system in *Shewanella acanthi* (GCF_019457475.1, NZ_CP080413) is composed of 9 proteins DndA (WP_220049638.1) DndB (WP_220049639.1) DndC (WP_220049640.1) DndD (WP_220049642.1) DndE (WP_220049643.1) DndB (WP_220049645.1) DptH (WP_220049650.1) DptG (WP_220052703.1) DptF (WP_220049651.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Xiong_2019[<a href='https://doi.org/10.1038/s41467-019-09390-9'>Xiong et al., 2019</a>] --> Origin_0
    Origin_0[ DndCDEA-PbeABCD
Halalkalicoccus jeotgali] --> Expressed_0[Natrinema sp. CJ7-F]
    Expressed_0[Natrinema sp. CJ7-F] ----> SNJ1
    subgraph Title1[Reference]
        Xiong_2019
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        SNJ1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

