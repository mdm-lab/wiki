---
title: PD-Lambda-5
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF02086
contributors:
    - Nathalie Bechon
relevantAbstracts:
    - doi: 10.1038/s41564-022-01219-4
---

# PD-Lambda-5
## Description
PD-lambda-5 is a two-gene system identified in a P2-like prophage. It encodes a protein with a nuclease and a HEPN domain, and a predicted methyltransferase. It confers broad anti-phage defense and does not seem to mediate abortive infection.

##Molecular mechanism
PD-lambda-5 molecular mechanism has not been described to date. It confers protection against a broad range of phages and does not seem to be an abortive infection system.

## Example of genomic structure

The PD-Lambda-5 is composed of 2 proteins: PD-Lambda-5_A and PD-Lambda-5_B.

Here is an example found in the RefSeq database:

![pd-lambda-5](/pd-lambda-5/PD-Lambda-5.svg){max-width=750px}

The PD-Lambda-5 system in *Plantactinospora sp. BB1* (GCF_003030385.1, NZ_CP028159) is composed of 2 proteins PD-Lambda-5_A (WP_159104731.1) PD-Lambda-5_B (WP_199853739.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCQ13837.1'>RCQ13837.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/RCQ13838.1'>RCQ13838.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & LambdaVir & SECphi17 & SECphi18 & SECphi27 & T3 & T7
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
        LambdaVir
        SECphi17
        SECphi18
        SECphi27
        T3
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

