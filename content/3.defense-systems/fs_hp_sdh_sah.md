---
title: FS_HP_SDH_sah
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2022.07.014
      abstract: |
        Bacteria encode sophisticated anti-phage systems that are diverse and versatile and display high genetic mobility. How this variability and mobility occurs remains largely unknown. Here, we demonstrate that a widespread family of pathogenicity islands, the phage-inducible chromosomal islands (PICIs), carry an impressive arsenal of defense mechanisms, which can be disseminated intra- and inter-generically by helper phages. These defense systems provide broad immunity, blocking not only phage reproduction, but also plasmid and non-cognate PICI transfer. Our results demonstrate that phages can mobilize PICI-encoded immunity systems to use them against other mobile genetic elements, which compete with the phages for the same bacterial hosts. Therefore, despite the cost, mobilization of PICIs may be beneficial for phages, PICIs, and bacteria in nature. Our results suggest that PICIs are important players controlling horizontal gene transfer and that PICIs and phages establish mutualistic interactions that drive bacterial ecology and evolution.
    PFAM: PF01972
relevantAbstracts:
    - doi: 10.1016/j.cell.2022.07.014
---

# FS_HP_SDH_sah

## To do 

## Example of genomic structure

The FS_HP_SDH_sah is composed of 2 proteins: HP and SDH_sah.

Here is an example found in the RefSeq database:

![fs_hp_sdh_sah](/fs_hp_sdh_sah/FS_HP_SDH_sah.svg){max-width=750px}

The FS_HP_SDH_sah system in *Escherichia coli* (GCF_904831805.1, NZ_LR882990) is composed of 2 proteins HP (WP_133302715.1) SDH_sah (WP_032201440.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/BBW89085.1'>BBW89085.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/BBW89084.1'>BBW89084.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4
    subgraph Title1[Reference]
        Fillol-Salom_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

