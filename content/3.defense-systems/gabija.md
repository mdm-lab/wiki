---
title: Gabija
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Direct
    Effector: Degrading nucleic acids
    PFAM: PF00580, PF11398, PF13175, PF13245, PF13304, PF13361, PF13476
contributors:
    - Nathalie Bechon
relevantAbstracts:
    - doi: 10.1093/nar/gkab277
    - doi: 10.1126/science.aar4120
    - doi: 10.1016/j.chom.2023.06.014
    - doi: 10.1038/s41586-023-06855-2
    - doi: 10.1038/s41586-023-06869-w
---

# Gabija

## Description

Gabija is named after the Lithuanian spirit of fire, protector of home and family. It is a two-gene defense system found in 8.5% of the 4360 bacterial and archeal genomes that were initially analyzed in :ref{doi=10.1126/science.aar4120}. Both proteins are necessary for defense and are forming a heteromeric octamer complex: GajA forms a central tetramer surrounded by two GajB dimers :ref{doi=10.1038/s41586-023-06855-2,10.1093/nar/gkad951}. A phage protein inhibiting Gabija function was described, Gabidja anti-defense 1 (Gad1) :ref{doi=10.1038/s41586-023-06855-2,10.1038/s41586-023-06869-w}.

## Molecular mechanism

The precise mechanism of the Gabija system remains to be fully described, yet studies suggest that it could act through a dual phage inhibition mechanism.
GajA was shown to be a sequence-specific DNA-nicking endonuclease, whose activity is inhibited by nucleotide concentration. This nucleotide sensing is mediated by GajA ATPase-like domain. Accordingly, GajA would be fully inhibited at cellular nucleotide concentrations. It was hypothesized that upon nucleotide depletion during phage infection, GajA would become activated :ref{doi=10.1093/nar/gkab277}. 
Moreover, a later study suggests that the _gajB_ gene encodes an NTPase, which would form a complex with GajA to achieve anti-phage defense. GajB is activated by DNA termini produced by GajA activity and then hydrolyzes (d)A/(d)GTP, depleting essential nucleotides and increasing GajA activity :ref{doi=10.1016/j.chom.2023.06.014}.
Therefore, both proteins would cooperate to achieve both nucleotide depletion and DNA cleavage, causing abortive infection.

## Example of genomic structure

The Gabija is composed of 2 proteins: GajA and GajB.

Here is an example found in the RefSeq database:

![gabija](/gabija/Gabija.svg){max-width=750px}

The Gabija system in *Roseomonas fluvialis* (GCF_022846615.1, NZ_AP025637) is composed of 2 proteins GajA (WP_244458879.1) GajB_2 (WP_244458880.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus cereus strain VD045 
<a href='https://ncbi.nlm.nih.gov/protein/EJR29742.1'>EJR29742.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR29743.1'>EJR29743.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SBSphiC & SpBeta & phi105 & rho14 & phi29
    Cheng_2021[<a href='https://doi.org/10.1093/nar/gkab277'>Cheng et al., 2021</a>] --> Origin_0
    Origin_0[Bacillus cereus strain VD045 
<a href='https://ncbi.nlm.nih.gov/protein/EJR29742.1'>EJR29742.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR29743.1'>EJR29743.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T7
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[Bacillus cereus strain HuB5-5 
<a href='https://ncbi.nlm.nih.gov/protein/EJQ91274.1'>EJQ91274.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJQ91275.1'>EJQ91275.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> SpBeta & phi105
    subgraph Title1[Reference]
        Doron_2018
        Cheng_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_2
        Expressed_1
end
    subgraph Title4[Protects against]
        SBSphiC
        SpBeta
        phi105
        rho14
        phi29
        T7
        SpBeta
        phi105
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

