---
title: Kiwa
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF16162
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1126/science.aar4120
  - doi: 10.1101/2023.02.26.530102
---

# Kiwa

## Description

The Kiwa antiviral defense system was first described in :ref{doi=10.1126/science.aar4120} and further described in :ref{doi=10.1101/2023.02.26.530102}. It is named after one of the divine guardians of the ocean in the Māori traditions. Kiwa is composed of two proteins: KwaA and KwaB. 
## Molecular mechanisms

KwaA detects phage infection by detecting the inhibition of the host RNA polymerase by phages. This triggers the response by KwaB, which decreases phage DNA replication through a RecBCD-dependent pathway :ref{doi=10.1101/2023.02.26.530102}.

## Example of genomic structure

The Kiwa is composed of 2 proteins: KwaA and KwaB.

Here is an example found in the RefSeq database:

![kiwa](/kiwa/Kiwa.svg){max-width=750px}

The Kiwa system in *Dickeya zeae* (GCF_019464635.1, NZ_CP040817) is composed of 2 proteins KwaA (WP_038908257.1) KwaB_2 (WP_192991081.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/AEZ43441.1'>AEZ43441.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AEZ43440.1'>AEZ43440.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi18
    subgraph Title1[Reference]
        Doron_2018
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        LambdaVir
        SECphi18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


