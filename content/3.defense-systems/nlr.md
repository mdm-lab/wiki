---
title: NLR
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2023.04.015
      abstract: |
        Bacteria use a wide range of immune pathways to counter phage infection. A subset of these genes shares homology with components of eukaryotic immune systems, suggesting that eukaryotes horizontally acquired certain innate immune genes from bacteria. Here, we show that proteins containing a NACHT module, the central feature of the animal nucleotide-binding domain and leucine-rich repeat containing gene family (NLRs), are found in bacteria and defend against phages. NACHT proteins are widespread in bacteria, provide immunity against both DNA and RNA phages, and display the characteristic C-terminal sensor, central NACHT, and N-terminal effector modules. Some bacterial NACHT proteins have domain architectures similar to the human NLRs that are critical components of inflammasomes. Human disease-associated NLR mutations that cause stimulus-independent activation of the inflammasome also activate bacterial NACHT proteins, supporting a shared signaling mechanism. This work establishes that NACHT module-containing proteins are ancient mediators of innate immunity across the tree of life.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF05729
contributors:
    - Héloïse Georjon
relevantAbstract:
    - doi: 10.1016/j.cell.2023.04.015
---

# NLR / bNACHT
## Description
This NLR like system is also known as the bNACHT systems.

Several proteins containing a NACHT module were found to have anti-phage activity. NACHT modules are also found in metazoans, where they play a role in innate immunity. 

bNacht systems have some similarities with other defense systems:
[CARD_NLR](/defense-systems/card_nlr) , [Rst_TIR-NLR](/defense-systems/rst_tir-nlr), [Avs](/defense-systems/avs).

## Molecular mechanism

Bacterial NACHT module-containing proteins (bNACHT) encompass a C-terminal sensor, a central NACHT module, and N-terminal effector modules. The N-terminal effectors associated with bNACHT proteins are diverse, and include domains that have been previously described to be involved in other defense systems {doi=10.1016/j.cell.2023.04.015}
As far as we are aware, the precise molecular mechanism of bNACHT is unknown.

## Example of genomic structure

A total of 2 subsystems have been described for the NLR system.

Here is some examples found in the RefSeq database:

![nlr_like_bnacht01](/nlr/NLR_like_bNACHT01.svg){max-width=750px}

The NLR_like_bNACHT01 system in *Colwellia sp. Arc7-D* (GCF_003061515.1, NZ_CP028924) is composed of 1 protein: NLR_like_bNACHT01 (WP_162533747.1) 

![nlr_like_bnacht09](/nlr/NLR_like_bNACHT09.svg){max-width=750px}

The NLR_like_bNACHT09 system in *Parabacteroides johnsonii* (GCF_020735865.1, NZ_CP085975) is composed of 1 protein: NLR_like_bNACHT09 (WP_009859882.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_0
    Origin_0[ bNACHT01
Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_015632533.1'>WP_015632533.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4 & T5 & T6
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_1
    Origin_1[ bNACHT02
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_021557529.1'>WP_021557529.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7 & MS2
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_2
    Origin_2[ bNACHT11
Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_114260439.1'>WP_114260439.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T4 & T6
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_3
    Origin_3[ bNACHT12
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_021519735.1'>WP_021519735.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T4 & T6 & MS2
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_4
    Origin_4[ bNACHT23
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000433597.1'>WP_000433597.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> T2 & T6 & T5
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_5
    Origin_5[ bNACHT25
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001702659.1'>WP_001702659.1</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> T2 & T4 & T6 & LambdaVir & MS2
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_6
    Origin_6[ bNACHT32
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_057688292'>WP_057688292</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> T2 & T4 & T6 & LambdaVir & MS2
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_7
    Origin_7[ bNACHT67
Klebsiella michiganensis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_064381242'>WP_064381242</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T2 & T4
    Kibby_2022[<a href='https://doi.org/10.1101/2022.07.19.500537'>Kibby et al., 2022</a>] --> Origin_8
    Origin_8[ bNACHT09
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_087866631'>WP_087866631</a>] --> Expressed_8[Escherichia coli]
    Expressed_8[Escherichia coli] ----> T2 & T4 & T5 & LambdaVir & T3 & T7
    subgraph Title1[Reference]
        Kibby_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
        Origin_8
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
        Expressed_8
end
    subgraph Title4[Protects against]
        T4
        T5
        T6
        T7
        MS2
        T2
        T4
        T6
        T4
        T6
        MS2
        T2
        T6
        T5
        T2
        T4
        T6
        LambdaVir
        MS2
        T2
        T4
        T6
        LambdaVir
        MS2
        T2
        T4
        T2
        T4
        T5
        LambdaVir
        T3
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

::

