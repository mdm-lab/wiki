---
title: SanaTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.molcel.2013.02.002
      abstract: |
        Toxin-antitoxin (TA) modules, composed of a toxic protein and a counteracting antitoxin, play important roles in bacterial physiology. We examined the experimental insertion of 1.5 million genes from 388 microbial genomes into an Escherichia coli host using more than 8.5 million random clones. This revealed hundreds of genes (toxins) that could only be cloned when the neighboring gene (antitoxin) was present on the same clone. Clustering of these genes revealed TA families widespread in bacterial genomes, some of which deviate from the classical characteristics previously described for such modules. Introduction of these genes into E. coli validated that the toxin toxicity is mitigated by the antitoxin. Infection experiments with T7 phage showed that two of the new modules can provide resistance against phage. Moreover, our experiments revealed an "antidefense" protein in phage T7 that neutralizes phage resistance. Our results expose active fronts in the arms race between bacteria and phage.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF08843
contributors:
    - Alba Herrero del Valle

relevantAbstracts:
    - doi: 10.1016/j.molcel.2013.02.002
---

# SanaTA

## Description

The SanaTA system is composed of 2 proteins: SanaT and, SanaA, where SanaT encodes a toxin and SanaA encodes an antitoxin to form a toxin-antitoxin (TA) system. The toxin protein, SanaT, contains a nucleotidyltransferase domain (PF08843). This system provides resistance against mutated T7 phages that lack the nonessential 4.5 gene in *Shewanella sp.* ANA-3. Sberro et al. showed that the defensiveness of sanaTA depends on the cleavage of SanaA by the Lon system, a protease that degrades the antitoxin to allow the activity of the toxin :ref{doi=10.1016/j.molcel.2013.02.002}.

## Molecular mechanism

It has been shown to function as a toxin-antitoxin (TA) system.

## Example of genomic structure

The SanaTA is composed of 2 proteins: SanaA and SanaT.

Here is an example found in the RefSeq database:

![sanata](/sanata/SanaTA.svg){max-width=750px}

The SanaTA system in *Euzebyella marina* (GCF_003687485.1, NZ_CP032050) is composed of 2 proteins SanaA (WP_121848345.1) SanaT (WP_121848346.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Sberro_2013[<a href='https://doi.org/10.1016/j.molcel.2013.02.002'>Sberro et al., 2013</a>] --> Origin_0
    Origin_0[Shewanella sp. ANA-3 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=639720518'>639720518</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=639720519'>639720519</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Sberro_2013
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



