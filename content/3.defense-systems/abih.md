---
title: AbiH
layout: article
tableColumns:
    article:
      doi: 10.1111/j.1574-6968.1996.tb08446.x
      abstract: |
        A gene which encodes resistance by abortive infection (Abi+) to bacteriophage was cloned from Lactococcus lactis ssp. lactis biovar. diacetylactis S94. This gene was found to confer a reduction in efficiency of plating and plaque size for prolate-headed bacteriophage phi 53 (group I of homology) and total resistance to the small isometric-headed bacteriophage phi 59 (group III of homology). The cloned gene is predicted to encode a polypeptide of 346 amino acid residues with a deduced molecular mass of 41 455 Da. No homology with any previously described genes was found. A probe was used to determine the presence of this gene in two strains on 31 tested.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF14253
contributors:
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1111/j.1574-6968.1996.tb08446.x
---

# AbiH

## Description
AbiH a system discovered in 1996 :ref{doi=10.1111/j.1574-6968.1996.tb08446.x}.

AbiH is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. Even though, their name corresponds to an abortive infection, their mechanism of action does not necessarily correspond to an abortive infection. For the abortive infection, phenotype AbiH is classified as unknown in :ref{doi=10.1016/j.mib.2023.102312}.

This system is a single gene system with no known domain (except its own PFAM).

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The AbiH is composed of 1 protein: AbiH.

Here is an example found in the RefSeq database:

![abih](/abih/AbiH.svg){max-width=750px}

The AbiH system in *Bacteroides faecium* (GCF_012113595.1, NZ_CP050831) is composed of 1 protein: AbiH (WP_167962074.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Prevost_1996[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococci 
<a href='https://ncbi.nlm.nih.gov/protein/CAA66252.1'>CAA66252.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2
    subgraph Title1[Reference]
        Prevost_1996
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>




