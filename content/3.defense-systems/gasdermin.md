---
title: GasderMIN
layout: article
tableColumns:
    article:
      doi: 10.1126/science.abj8432
      abstract: |
        Gasdermin proteins form large membrane pores in human cells that release immune cytokines and induce lytic cell death. Gasdermin pore formation is triggered by caspase-mediated cleavage during inflammasome signaling and is critical for defense against pathogens and cancer. We discovered gasdermin homologs encoded in bacteria that defended against phages and executed cell death. Structures of bacterial gasdermins revealed a conserved pore-forming domain that was stabilized in the inactive state with a buried lipid modification. Bacterial gasdermins were activated by dedicated caspase-like proteases that catalyzed site-specific cleavage and the removal of an inhibitory C-terminal peptide. Release of autoinhibition induced the assembly of large and heterogeneous pores that disrupted membrane integrity. Thus, pyroptosis is an ancient form of regulated cell death shared between bacteria and animals.
    Sensor: Phage protein
    Activator: Unknown
    Effector: Membrane disrupting
contributors: 
  - Aude Bernheim
relevantAbstracts:
    - doi: 10.1126/science.abj8432
    - doi: 10.1101/2023.05.28.542683
---

# GasderMIN


## Description

Gasdermin proteins were initially discovered in humans. Recently they were shown to be present in multiple bacteria, where they are almost always encoded in an operon together with a protease. The experimental validation of the antiphage activity of bacterial gasdermins was demonstrated through the heterologous expression of a 4-genes operon from Lysobacter in *E. coli* :ref{doi=10.1126/science.abj8432,10.1101/2023.05.28.542683}. 

## Molecular Mechanism

Akin to their human counterparts, bacterial gasdermins encode a C-terminal inhibitory domain. Following phage infection, proteases associated with bacterial gasdermin cleave this domain triggering oligomerization of gasdermins into large, membrane-breaching pores :ref{doi=10.1126/science.abj8432} leading to cell death:ref{doi=10.1101/2023.05.28.542683}. As such gasdermins containing systems are abortive infection systems. rIIB, a protein allowing T6 to overcome the RexAB system was shown to activate gasdermin. It was further shown that CARD domains are also essential in bacterial gasdermins defense :ref{doi=10.1101/2023.05.28.542683}.

## Example of genomic structure

The GasderMIN is composed of 1 protein: bGSDM.

Here is an example found in the RefSeq database:

![gasdermin](/gasdermin/GasderMIN.svg){max-width=750px}

The GasderMIN system in *Flavobacterium johnsoniae* (GCF_000016645.1, NC_009441) is composed of 1 protein: bGSDM (WP_012023900.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Johnson_2022[<a href='https://doi.org/10.1371/journal.pgen.1010065'>Johnson et al., 2022</a>] --> Origin_0
    Origin_0[Lysobacter enzymogenes 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794907'>2841794907</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794908'>2841794908</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794909'>2841794909</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794910'>2841794910</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & T4 & T6
    subgraph Title1[Reference]
        Johnson_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T5
        T4
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


