---
title: Old_exonuclease
layout: article
tableColumns:
    article:
      doi: 10.1128/jb.177.3.497-501.1995
      abstract: |
        The Old protein of bacteriophage P2 is responsible for interference with the growth of phage lambda and for killing of recBC mutant Escherichia coli. We have purified Old fused to the maltose-binding protein to 95% purity and characterized its enzymatic properties. The Old protein fused to maltose-binding protein has exonuclease activity on double-stranded DNA as well as nuclease activity on single-stranded DNA and RNA. The direction of digestion of double-stranded DNA is from 5' to 3', and digestion initiates at either the 5'-phosphoryl or 5'-hydroxyl terminus. The nuclease is active on nicked circular DNA, degrades DNA in a processive manner, and releases 5'-phosphoryl mononucleotides.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleic acid degrading
    PFAM: PF13175, PF13304
contributors:
    - Marian Dominguez-Mirazo
relevantAbstracts:
    - doi: 10.1128/jb.177.3.497-501.1995
    - doi: 10.1128/jb.177.3.497-501.1995
---

# Old_exonuclease

## Description
The OLD proteins are a family of nucleases present in bacteria, archaea, and viruses :ref{doi=10.1093/nar/gkz703}. The OLD protein found in the P2 *E.coli* prophage is the best characterized one. The protein is an exonuclease that digests dsDNA in the 5' to 3' direction :ref{doi=10.1128/jb.177.3.497-501.1995}. It also has nuclease activity against single-stranded DNA and RNA :ref{doi=10.1128/jb.177.3.497-501.1995}. It's been shown to protect against phage lambda :ref{doi=10.1128/jb.177.3.497-501.1995}, and when cloned with the P2 Tin accessory gene, it was shown to protect against other *E. coli* phages :ref{doi=10.1016/j.chom.2022.02.018}. The protein also contains an ATPase domain that affects nuclease activity :ref{doi=10.1128/jb.177.3.497-501.1995}. Inhibition of the RecBCD complex activates the OLD nuclease :ref{doi=10.1016/j.mib.2023.102325}. OLD proteins are divided into two classes based on amino acid sequence conservation and gene neighborhood :ref{doi=10.1093/nar/gkz703}. The P2-associated protein belongs to class 2 :ref{doi=10.1093/nar/gkz703}. 

## Molecular Mechanisms
The old_exonuclease is dsDNA exonuclease that digests in the 5' to 3' direction :ref{doi=10.1128/jb.177.3.497-501.1995}. To our knowledge, other aspects of the molecular mechanisms remain unknown. 

## Example of genomic structure

The Old_exonuclease is composed of 1 protein: Old_exonuclease.

Here is an example found in the RefSeq database:

![old_exonuclease](/old_exonuclease/Old_exonuclease.svg){max-width=750px}

The Old_exonuclease system in *Shewanella xiamenensis* (GCF_022453805.1, NZ_CP092630) is composed of 1 protein: Old_exonuclease (WP_240293412.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Enterobacteria phage P2 
<a href='https://ncbi.nlm.nih.gov/protein/NP_046798.1'>NP_046798.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T4 & LF82_P8 & Al505_P2
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        Lambda
        T4
        LF82_P8
        Al505_P2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

