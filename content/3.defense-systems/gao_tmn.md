---
title: Gao_Tmn
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1126/science.aba0372
---

# Gao_Tmn

## Description

The Gao_Tmn system is named after the first author of the first paper describing it :ref{doi=10.1126/science.aba0372} and contains the description of the gene identified: a transmembrane NTPase.

## Molecular mechanisms

As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The Gao_Tmn is composed of 1 protein: TmnA.

Here is an example found in the RefSeq database:

![gao_tmn](/gao_tmn/Gao_Tmn.svg){max-width=750px}

The Gao_Tmn system in *Pectobacterium carotovorum* (GCF_009931215.1, NZ_CP034237) is composed of 1 protein: TmnA (WP_161509209.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001683567.1'>WP_001683567.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & P1 & PhiV-1 & PhiX
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        P1
        PhiV-1
        PhiX
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

