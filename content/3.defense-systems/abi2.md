---
title: Abi2
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Direct Binding
    Effector: Unknown
    PFAM: PF07751
contributors:
  - Marian Dominguez-Mirazo
relevantAbstracts:
  - doi: 10.1016/j.mib.2005.06.006
---

# Abi2

## Description

The Abi2 system corresponds to a group of abortive infection systems: [AbiD](/defense-systems/abid), AbiD1 and AbiF.

The Abi2/AbiD1 system of lactococci is composed of a single protein Abi_2 :ref{doi=10.1016/j.mib.2005.06.006}. Expression of the protein is toxic to the bacteria and is therefore highly regulated. Expression of the protein is triggered by the orf1 of phage bIL66 which results in inhibition of the phage RuvC-like endonuclease activity and stops phage multiplication. 

## Molecular mechanism

For AbiD1 :ref{doi=10.1128/jb.177.13.3824-3829.1995} (AAA79209), the expression of the protein is toxic to the bacteria and is therefore highly regulated. Expression of the protein is triggered by the orf1 of phage bIL66 which results in inhibition of the phage RuvC-like endonuclease activity and stops phage multiplication.


## Example of genomic structure

The Abi2 is composed of 1 protein: Abi_2.

Here is an example found in the RefSeq database:

![abi2](/abi2/Abi2.svg){max-width=750px}

The Abi2 system in *Veillonella parvula* (GCF_002005185.1, NZ_CP019721) is composed of 1 protein: Abi_2 (WP_077707938.1) 

## Molecular mechanisms
As far as we are aware, the molecular mechanisms remain unclear.

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation 

As Abi2 is a group of different Abortive infection systems, here is the experimental validation of AbiD:

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid  
<a href='https://ncbi.nlm.nih.gov/protein/AAA63619.1'>AAA63619.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
