---
title: NixI
layout: article
tableColumns:
    article:
      doi: 10.1093/nar/gkac002
      abstract: |
        PLEs (phage-inducible chromosomal island-like elements) are phage parasites integrated into the chromosome of epidemic Vibrio cholerae. In response to infection by its viral host ICP1, PLE excises, replicates and hijacks ICP1 structural components for transduction. Through an unknown mechanism, PLE prevents ICP1 from transitioning to rolling circle replication (RCR), a prerequisite for efficient packaging of the viral genome. Here, we characterize a PLE-encoded nuclease, NixI, that blocks phage development likely by nicking ICP1’s genome as it transitions to RCR. NixI-dependent cleavage sites appear in ICP1’s genome during infection of PLE(+) V. cholerae. Purified NixI demonstrates in vitro nuclease activity specifically for sites in ICP1’s genome and we identify a motif that is necessary for NixI-mediated cleavage. Importantly, NixI is sufficient to limit ICP1 genome replication and eliminate progeny production, representing the most inhibitory PLE-encoded mechanism revealed to date. We identify distant NixI homologs in an expanded family of putative phage parasites in vibrios that lack nucleotide homology to PLEs but nonetheless share genomic synteny with PLEs. More generally, our results reveal a previously unknown mechanism deployed by phage parasites to limit packaging of their viral hosts’ genome and highlight the prominent role of nuclease effectors as weapons in the arms race between antagonizing genomes.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleic acid degrading
contributors:
    - Marian Dominguez-Mirazo 
relevantAbstracts:
    - doi: 10.1093/nar/gkac002
---

# NixI
## Description
Phage-inducible chromosomal island-like elements (PLEs) are chromosomally-integrated phage parasites described in *Vibrio cholerae* :ref{doi=10.7554/eLife.53200}. PLEs excise in response to infection by phage ICP1 and halt its progeny production. PLE halts ICP1 infection by redirecting virion packaging and interfering with ICP1 genome replication :ref{doi=10.1093/nar/gkz1005}. NixI is a PLE-encoded nuclease that nicks the ICP1 genome at specific sites preventing transition to the rolling circle replication (RCR) :ref{doi=10.1093/nar/gkac002}. 

## Molecular mechanisms
The NixI endonuclease cleaves the ICP1 genome at the GNAANCTT motif :ref{doi=10.1093/nar/gkac002}. It creates nicks and does not cause double-stranded breaks :ref{doi=10.1093/nar/gkac002}. 

## Example of genomic structure

The NixI is composed of 2 proteins: NixI and Stix.

Here is an example found in the RefSeq database:

![nixi](/nixi/NixI.svg){max-width=750px}

The NixI system in *Apilactobacillus kunkeei* (GCF_001314945.1, NZ_CP012920) is composed of 1 protein: NixI (WP_158520138.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Legault_2022[<a href='https://doi.org/10.1093/nar/gkac002'>LeGault et al., 2022</a>] --> Origin_0
    Origin_0[Vibrio cholerae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045177897.1'>WP_045177897.1</a>] --> Expressed_0[Vibrio cholerae]
    Expressed_0[Vibrio cholerae] ----> ICP1
    subgraph Title1[Reference]
        Legault_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        ICP1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

