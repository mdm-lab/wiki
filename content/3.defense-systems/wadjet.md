---
title: Wadjet
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Detecting invading nucleic acid
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF09660, PF09661, PF09664, PF09983, PF11795, PF11796, PF11855, PF13555, PF13558, PF13835
contributors: 
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1126/science.aar4120
    - doi: 10.1016/j.molcel.2022.09.008
---

# Wadjet

## Description
Wadjet is a defense system discovered in 2018 :ref{doi=10.1126/science.aar4120}. This system is named after the goddess [Wadjet](https://en.wikipedia.org/wiki/Wadjet) matron and protector of the lower Egypt.

This system was discovered using the [defense island](/general-concepts/defense-islands) concept :ref{doi=10.1126/science.aar4120}. However, this system do not show any activity against phages, this system provides a resistance against plasmid transformation :ref{doi=10.1126/science.aar4120,10.1016/j.molcel.2022.09.008}.

This system is composed of 4 genes: JetA, JetB, JetC and JetD. JetA, JetB and JetC were found to be similar to respectively MukF, MukE and MukB which are housekeeping genes :ref{doi=10.1126/science.aar4120,10.1098/rstb.2004.1606}. Notably, JetC (similar to MukB) is a SMC domain-containing protein. 

### Molecular mechanism
Wadjet was shown to possess an anti-plasmid activity via close, circular DNA cleavage :ref{doi=10.1098/rstb.2004.1606}. JetA, B and C were shown to form a complex which acts as a sensor detecting the foreign DNA using topology sensing. The JetD protein forms a homodimeric endonuclease that cleaves double-stranded DNA. JetD needs to be in contact with the JetABC complex to be able to cleave DNA.

The precise mechanism suggested by the authors :ref{doi=10.1098/rstb.2004.1606} is that the JetABC complex binds to the DNA and extrudes DNA loops similarly to Muk and other SMC proteins :ref{doi=10.1126/science.abm4012,10.1126/science.abb0981}. These loops are then cleaved by JetD.

## Example of genomic structure

A total of 3 subsystems have been described for the Wadjet system.

Here is some examples found in the RefSeq database:

![wadjet_i](/wadjet/Wadjet_I.svg){max-width=750px}

The Wadjet_I system in *Corynebacterium humireducens* (GCF_000819445.1, NZ_CP005286) is composed of 4 proteins JetA_I (WP_040084540.1) JetB_I (WP_052437638.1) JetC_I (WP_040084541.1) JetD_I (WP_040084542.1) 

![wadjet_ii](/wadjet/Wadjet_II.svg){max-width=750px}

The Wadjet_II system in *Streptomyces paludis* (GCF_003344965.1, NZ_CP031194) is composed of 4 proteins JetA_II (WP_114665121.1) JetB_II (WP_114660010.1) JetC_II (WP_114660011.1) JetD_II (WP_228447139.1) 

![wadjet_iii](/wadjet/Wadjet_III.svg){max-width=750px}

The Wadjet_III system in *Cytobacillus spongiae* (GCF_021390075.1, NZ_CP089997) is composed of 4 proteins JetD_III (WP_233811322.1) JetA_III (WP_233811323.1) JetB_III (WP_233811324.1) JetC_III (WP_233811325.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

::info
This section is empty
::
