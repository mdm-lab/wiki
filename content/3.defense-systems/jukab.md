---
title: JukAB
layout: article
tableColumns:
    article:
      doi: 10.1101/2022.09.17.508391
      abstract: |
        Jumbo bacteriophages of the PhiKZ-like family are characterized by large genomes (>200 kb) and the remarkable ability to assemble a proteinaceous nucleus-like structure. The nucleus protects the phage genome from canonical DNA-targeting immune systems, such as CRISPR-Cas and restriction-modification. We hypothesized that the failure of common bacterial defenses creates selective pressure for immune systems that target the unique jumbo phage biology. Here, we identify the "jumbo phage killer"(Juk) immune system that is deployed by a clinical isolate of Pseudomonas aeruginosa to resist PhiKZ. Juk immunity rescues the cell by preventing early phage transcription, DNA replication, and nucleus assembly. Phage infection is first sensed by JukA (formerly YaaW), which localizes rapidly to the site of phage infection at the cell pole, triggered by ejected phage factors. The effector protein JukB is recruited by JukA, which is required to enable immunity and the subsequent degradation of the phage DNA. JukA homologs are found in several bacterial phyla and are associated with numerous other putative effectors, many of which provided specific antiPhiKZ activity when expressed in P. aeruginosa. Together, these data reveal a novel strategy for immunity whereby immune factors are recruited to the site of phage protein and DNA ejection to prevent phage progression and save the cell.
    PFAM: PF13099
contributors:
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1101/2022.09.17.508391
---

# JukAB

## Description
JukAB was shown to be active against nucleus-like forming Jumbo phages :ref{doi=10.1038/s41586-022-05013-4} in *Pseudomonas aeruginosa* :ref{doi=10.1101/2022.09.17.508391}. This system was named JukAB for **Ju**mbo phage **K**iller.

JukAB is composed of two proteins JukA and JukB. JukA encodes for the DUF3944 domain present in other potential defense systems according to :ref{doi=10.1101/2022.09.17.508391}.

## Molecular mechanism

JukAB is acting at the early stage of PhiKZ infection before the phage forms its nucleus :ref{doi=10.1101/2022.09.17.508391}.

Using fluorescence microscopy, the authors showed that JukA is acting as a sensor of the phage infection using PhiKZ ejection factors :ref{doi=10.1101/2022.09.17.508391}. After the detection of the infection by JukA, JukB is recruited and the phage is not able to form its nucleus-like particle and other nuclease from the host will degrade the phage DNA.

The authors :ref{doi=10.1101/2022.09.17.508391} hypothesized that JukA detects the "rod-like" inner body. JukA will recruit JukB which destabilizes the early protective structure of PhiKZ. The accessible DNA will then be degraded by other nucleases.

## Example of genomic structure

The JukAB is composed of 2 proteins: JukA and JukB.

Here is an example found in the RefSeq database:

![jukab](/jukab/JukAB.svg){max-width=750px}

The JukAB system in *Kosakonia oryzae* (GCF_020544365.1, NZ_CP065358) is composed of 2 proteins JukA (WP_064562651.1) JukB (WP_064562654.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Li_2022[<a href='https://doi.org/10.1101/2022.09.17.508391'>Li et al., 2022</a>] --> Origin_0
    Origin_0[Pseudomonas aeruginosa 
<a href='https://ncbi.nlm.nih.gov/protein/WP_003137195.1'>WP_003137195.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_003137196.1'>WP_003137196.1</a>] --> Expressed_0[Pseudomonas aeruginosa]
    Expressed_0[Pseudomonas aeruginosa] ----> ΦΚZ
    subgraph Title1[Reference]
        Li_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        ΦΚZ
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

