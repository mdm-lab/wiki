---
title: RM
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.003
      abstract: |
        The phenomena of prokaryotic restriction and modification, as well as anti-restriction, were first discovered five decades ago but have yielded only gradually to rigorous analysis. Work presented at the 5th New England Biolabs Meeting on Restriction-Modification (available on REBASE) and several recently published genetic, biochemical and biophysical analyses indicate that these fields continue to contribute significantly to basic science. Recently, there have been several studies that have shed light on the still developing field of restriction-modification and on the newly re-emerging field of anti-restriction.
    Sensor: Detecting invading nucleic acid
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF00270, PF02384, PF04313, PF04851, PF12008, PF12161, PF18766
contributors: 
  - Aude Bernheim
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1016/j.mib.2005.06.003
  - doi: 10.1093/nar/gku734
---

# RM

## Description
Restriction modification systems are the most abundant antiphage systems. They already have their own [Wikipedia page](https://en.wikipedia.org/wiki/Restriction_modification_system)

## Molecular Mechanisms
Several reviews detail the molecular mechanisms of restriction-modification systems. For example in :ref{doi=10.1016/j.mib.2005.06.003}:

"Bacterial restriction-modification (R-M) systems function as prokaryotic immune systems that attack foreign DNA entering the cell :ref{doi=10.1128/jb.65.2.113-121.1953}. Typically, R-M systems have enzymes responsible for two opposing activities: a restriction endonuclease (REase) that recognizes a specific DNA sequence for cleavage and a cognate methyltransferase (MTase) that confers protection from cleavage by methylation of adenine or cytosine bases within the same recognition sequence. REases recognize ‘non-self’ DNA (Figure 1), such as that of phage and plasmids, by its lack of characteristic modification within specific recognition sites :ref{doi=10.1093/nar/29.18.3705}. Foreign DNA is then inactivated by endonucleolytic cleavage. Generally, methylation of a specific cytosine or adenine within the recognition sequence confers protection from restriction. Host DNA is normally methylated by the MTase following replication, whereas invading non-self DNA is not."

![Figure_1](/rm/Figure_1_Tock_Dryden_2005.png){max-width=750px}

Figure 1. The function of R-M systems, as illustrated by Type I R-M enzymes. From :ref{doi=10.1016/j.mib.2005.06.003}.


## Example of genomic structure

A total of 5 subsystems have been described for the RM system.

Here is some examples found in the RefSeq database:

![rm_type_i](/rm/RM_Type_I.svg){max-width=750px}

The RM_Type_I system in *Tessaracoccus flavescens* (GCF_001998865.1, NZ_CP019607) is composed of 3 proteins Type_I_MTases (WP_237268350.1) Type_I_S (WP_077346826.1) Type_I_REases (WP_237268249.1) 

![rm_type_ii](/rm/RM_Type_II.svg){max-width=750px}

The RM_Type_II system in *Aeromonas caviae* (GCF_000783775.2, NZ_CP026055) is composed of 2 proteins Type_II_MTases (WP_042865101.1) Type_II_REases (WP_042865102.1) 

![rm_type_iig](/rm/RM_Type_IIG.svg){max-width=750px}

The RM_Type_IIG system in *Microbacterium wangchenii* (GCF_004564355.1, NZ_CP038266) is composed of 1 protein: Type_IIG (WP_135067980.1) 

![rm_type_iii](/rm/RM_Type_III.svg){max-width=750px}

The RM_Type_III system in *Sphingobacterium spiritivorum* (GCF_016725325.1, NZ_CP068083) is composed of 2 proteins Type_III_REases (WP_139420208.1) Type_III_MTases (WP_139420206.1) 

![rm_type_iv](/rm/RM_Type_IV.svg){max-width=750px}

The RM_Type_IV system in *Halobaculum rubrum* (GCF_019880225.1, NZ_CP082284) is composed of 1 protein: Type_IV_REases (WP_222915961.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation
Many RM systems defend against many phages, as such we cannot map this correctly.

