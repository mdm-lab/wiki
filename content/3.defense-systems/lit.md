---
title: Lit
layout: article
tableColumns:
    article:
      doi: 10.1073/pnas.91.2.802
      abstract: |
        Bacteriophage T4 multiples poorly in Escherichia coli strains carrying the defective prophage, e14; the e14 prophage contains the lit gene for late inhibitor of T4 in E. coli. The exclusion is caused by the interaction of the e14-encoded protein, Lit, with a short RNA or polypeptide sequence encoded by gol from within the major head protein gene of T4. The interaction between Lit and the gol product causes a severe inhibition of all translation and prevents the transcription of genes downstream of the gol site in the same transcription unit. However, it does not inhibit most transcription, nor does it inhibit replication or affect intracellular levels of ATP. Here we show that the interaction of gol with Lit causes the cleavage of translation elongation factor Tu (EF-Tu) in a region highly conserved from bacteria to humans. The depletion of EF-Tu is at least partly responsible for the inhibition of translation and the phage exclusion. The only other phage-exclusion system to be understood in any detail also attacks a highly conserved cellular component, suggesting that phage-exclusion systems may yield important reagents for studying cellular processes.
    Sensor: Monitoring host integrity
    Activator: Direct
    Effector: Other (Cleaves an elongation factor, inhibiting cellular translation
    PFAM: PF10463
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1128/jb.169.3.1232-1238.1987
  - doi: 10.1128/jb.170.5.2056-2062.1988
  - doi: 10.1073/pnas.91.2.802
  - doi: 10.1074/jbc.M002546200
  - doi: 10.1186/1743-422X-7-360
---

# Lit

## Description

Lit was first identified in 1989 :ref{doi=10.1128/jb.169.3.1232-1238.1987}, stands for late inhibitors of T4 and was found to inhibit phage T4 in Escherichia coli (K12). The Lit gene is found in the e14 cryptic prophage :ref{doi=10.1128/jb.170.5.2056-2062.1988}. Lit is also partially active against other T-even phages :ref{doi=10.1073/pnas.91.2.802}.

## Molecular mechanisms

The Lit system detects cleaves EF-Tu translation factor :ref{doi=10.1073/pnas.91.2.802} at a late stage of phage maturation, when the major capsid protein binds to EF-Tu and triggers its cleavage by Lit :ref{doi=10.1074/jbc.M002546200}. As a result, the translation is inhibited, which ultimately leads to cell death. Lit is part of the abortive infection category of defense systems.

## Example of genomic structure

The Lit is composed of 1 protein: Lit.

Here is an example found in the RefSeq database:

![lit](/lit/Lit.svg){max-width=750px}

The Lit system in *Klebsiella pneumoniae* (GCF_002180175.1, NZ_CP021686) is composed of 1 protein: Lit (WP_223861284.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Yu_1994[<a href='https://doi.org/10.1073/pnas.91.2.802'>Yu and  Snyder, 1994</a>] --> Origin_0
    Origin_0[Escherichia coli defective prophage e14 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001257372.1'>WP_001257372.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4
    subgraph Title1[Reference]
        Yu_1994
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

