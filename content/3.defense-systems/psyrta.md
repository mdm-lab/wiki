---
title: PsyrTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00270, PF00271, PF02481, PF04851, PF18306
contributors:
    - Ernest Mordret
relevantAbstracts:
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1016/j.molcel.2013.02.002
    - doi: 10.1371/journal.ppat.1005317
---

# PsyrTA

## Description

Originally found in a high throughput shotgun cloning of bacterial fragments in E. coli looking for Toxin-Antitoxin pairs. PsyrTA is composed of two proteins, PsyrT, the toxin, is a RecQ family DNA helicase, and PsyrA, the antitoxin, was shown to be a Nucleotide-binding protein. Note that that system is sometimes called RqlHI :ref{doi=10.1371/journal.ppat.1005317}, where RqlH refers to PsyrT and RqlI to PsyrA.

## Molecular mechanisms

from :ref{doi=10.1016/j.molcel.2013.02.002} :

> The psyrT shares homology with domains of the RecQ helicase,
> a family of proteins implicated in DNA repair (Bernstein et al.,
> 2010); and the antitoxin of the same system, psyrA, has a nucle-
> otide binding domain (COG0758) that was previously described
> in proteins involved in DNA uptake


from :ref{doi=10.1016/j.chom.2022.09.017} :

> Both systems encode an antitoxin
> with homology to DprA, a single-stranded DNA (ssDNA)-binding
> protein known to be involved in DNA transformation (Mortier-
> Barrière et al., 2007). The toxin contains a phosphoribosyl trans-
> ferase (PRTase) domain, which was previously found in effectors
> of retron abortive infection systems "


## Example of genomic structure

The PsyrTA is composed of 2 proteins: PsyrA and PsyrT.

Here is an example found in the RefSeq database:

![psyrta](/psyrta/PsyrTA.svg){max-width=750px}

The PsyrTA system in *Providencia rettgeri* (GCF_013702265.1, NZ_CP059348) is composed of 2 proteins PsyrT (WP_049246657.1) PsyrA (WP_001103168.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus sp. FJAT-29814 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2746452868'>2746452868</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2746452867'>2746452867</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
::


