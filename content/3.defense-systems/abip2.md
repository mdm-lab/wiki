---
title: AbiP2
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00078
contributors:
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1093/nar/gkac467
---

# AbiP2

## Description

AbiP2 system was discovered in 2006 inside a variable region of the P2-like coliphages :ref{doi=10.1128/JB.188.4.1643-1647.2006}. This paper shows a relation with the known "Abi" system [AbiK](/defense-systems/abik).

It was then proven to have an antiphage activity against T5 in E. coli :ref{doi=10.1126/science.aba0372}.

AbiP2 is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiP2 is classified as possible abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

This system is called AbiP2 for the P2-like phage and has no relation with AbiP (a homolog of [AbiC](/defense-systems/abic)](/defense-systems/abic)).

However, with the discovery of dozens of new systems, it was categorized as one of the UG/Abi defense systems :ref{doi=10.1093/nar/gkac467} along with [DRT](/defense-systems/drt) different subsystems, [AbiA](/defense-systems/abia), [AbiK](/defense-systems/abik) and [Rst_RT_Nitralase_TM](/defense-systems/rst_rt-nitrilase-tm).

Those systems are characterized by the presence of a reverse transcriptase domain of the "Unknown Group RT".

## Molecular mechanism

To our knowledge, the molecular mechanism is unknown. Similarly, for the other systems of this family, the molecular mechanism remains unknown.

## Example of genomic structure

The AbiP2 is composed of 1 protein: AbiP2.

Here is an example found in the RefSeq database:

![abip2](/abip2/AbiP2.svg){max-width=750px}

The AbiP2 system in *Halomonas alkaliphila* (GCF_003612795.1, NZ_CP024811) is composed of 1 protein: AbiP2 (WP_120386816.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ RT-Abi-P2
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_047657908.1'>WP_047657908.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_1
    Origin_1[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045971693.1'>WP_045971693.1</a>] --> Expressed_1[lactococci]
    Expressed_1[lactococci] ----> 936
    subgraph Title1[Reference]
        Gao_2020
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        T5
        936
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

