---
title: BREX
layout: article
tableColumns:
    article:
      doi: 10.15252/embj.201489455
      abstract: |
        The perpetual arms race between bacteria and phage has resulted in the evolution of efficient resistance systems that protect bacteria from phage infection. Such systems, which include the CRISPR-Cas and restriction-modification systems, have proven to be invaluable in the biotechnology and dairy industries. Here, we report on a six-gene cassette in Bacillus cereus which, when integrated into the Bacillus subtilis genome, confers resistance to a broad range of phages, including both virulent and temperate ones. This cassette includes a putative Lon-like protease, an alkaline phosphatase domain protein, a putative RNA-binding protein, a DNA methylase, an ATPase-domain protein, and a protein of unknown function. We denote this novel defense system BREX (Bacteriophage Exclusion) and show that it allows phage adsorption but blocks phage DNA replication. Furthermore, our results suggest that methylation on non-palindromic TAGGAG motifs in the bacterial genome guides self/non-self discrimination and is essential for the defensive function of the BREX system. However, unlike restriction-modification systems, phage DNA does not appear to be cleaved or degraded by BREX, suggesting a novel mechanism of defense. Pan genomic analysis revealed that BREX and BREX-like systems, including the distantly related Pgl system described in Streptomyces coelicolor, are widely distributed in ~10% of all sequenced microbial genomes and can be divided into six coherent subtypes in which the gene composition and order is conserved. Finally, we detected a phage family that evades the BREX defense, implying that anti-BREX mechanisms may have evolved in some phages as part of their arms race with bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00069, PF00176, PF00270, PF00271, PF01507, PF01555, PF02384, PF04851, PF07669, PF07714, PF08378, PF08665, PF08747, PF08849, PF10923, PF13337, PF16565
contributors:
    - Marian Dominguez-Mirazo
relevantAbstracts:
    - doi: 10.1093/nar/gkaa290
    - doi: 10.1093/nar/gky1125
    - doi: 10.15252/embj.201489455
---

# BREX
## Description


BREX (for Bacteriophage Exclusion) is a family of anti-phage defense systems. BREX systems are active against both lytic and lysogenic phages. They allow phage adsorption but block phage DNA replication, and are considered to be [RM](/defense-systems/rm)-like systems :ref{doi=10.15252/embj.201489455,10.1093/nar/gkaa290}. BREX systems are found in around 10% of sequenced microbial genomes :ref{doi=10.15252/embj.201489455}.

BREX systems can be divided into six subtypes, and are encoded by 4 to 8 genes, some of these genes being mandatory while others are subtype-specific :ref{doi=10.15252/embj.201489455}.

## Molecular mechanism

*B. cereus* BREX Type 1 system was reported to methylate target motifs in the bacterial genome :ref{doi=10.15252/embj.201489455}. The methylation activity of this system has been hypothesized to allow for self from non-self discrimination, as it is the case for Restriction-Modification ([RM)](/defense-systems/rm) systems. 

However, the mechanism through which BREX Type 1 systems defend against phages is distinct from RM systems, and does not seem to degrade phage nucleic acids :ref{doi=10.15252/embj.201489455}. 

To date, BREX molecular mechanism remains to be described.


## Example of genomic structure

BREX systems necessarily include the pglZ gene (encoding for a putative alkaline phosphatase), which is accompanied by either brxC or pglY. These two genes share only a distant homology but have been hypothesized to fulfill the same function among the different BREX subtypes :ref{doi=10.15252/embj.201489455}.

Goldfarb and colleagues reported a 6-gene cassette from *Bacillus cereus* as being the model for BREX Type 1. BREX Type 1 are the most widespread BREX systems, and present two core genes (pglZ and brxC).  Four other genes  are associated with BREX Type 1 : *pglX (*encoding for a putative methyltransferase),  *brxA (*encoding an RNA-binding anti-termination protein)*, brxB (*unknown functio*n), brxC (*encoding for a protein with ATP-binding domain) and *brxL* (encoding for a putative protease) :ref{doi=10.15252/embj.201489455,10.1093/nar/gkaa290}.

Type 2 BREX systems include the system formerly known as Pgl, which is comprised of four genes (pglW, X, Y, and Z) :ref{doi=10.1093/nar/gky1125}, to which :ref{doi=10.15252/embj.201489455} found often associated two additional genes (brxD, and brxHI).

Although 4 additional BREX subtypes have been proposed, BREX Type 1 and Type 2 remain the only ones to be experimentally validated. A detailed description of the other subtypes can be found in :ref{doi=10.15252/embj.201489455}.

A total of 6 subsystems have been described for the BREX system.

Here is some examples found in the RefSeq database:

![brex_i](/brex/BREX_I.svg){max-width=750px}

The BREX_I system in *Mycobacterium kubicae* (GCF_015689175.1, NZ_CP065047) is composed of 6 proteins brxA_DUF1819 (WP_241007777.1) brxB_DUF1788 (WP_174814228.1) brxC (WP_085074024.1) pglX1 (WP_085074003.1) pglZA (WP_085074004.1) brxL (WP_085074005.1) 

![brex_ii](/brex/BREX_II.svg){max-width=750px}

The BREX_II system in *Nocardia terpenica* (GCF_002568625.1, NZ_CP023778) is composed of 6 proteins pglW (WP_232535326.1) pglX2 (WP_098693854.1) pglY (WP_098693855.1) pglZ2 (WP_098693856.1) brxD (WP_098693857.1) brxHI (WP_098693858.1) 

![brex_iii](/brex/BREX_III.svg){max-width=750px}

The BREX_III system in *Rubinisphaera brasiliensis* (GCF_000165715.2, NC_015174) is composed of 5 proteins pglZ3 (WP_013627487.1) brxHII (WP_013627488.1) pglXI (WP_013627489.1) brxC (WP_041397812.1) brxF (WP_218916504.1) 

![brex_iv](/brex/BREX_IV.svg){max-width=750px}

The BREX_IV system in *Olsenella sp. oral taxon 807* (GCF_001189515.2, NZ_CP012069) is composed of 4 proteins brxL (WP_050344523.1) pglZ4 (WP_172674480.1) brxC4 (WP_050340978.1) brxP (WP_050340980.1) 

![brex_v](/brex/BREX_V.svg){max-width=750px}

The BREX_V system in *Heyndrickxia vini* (GCF_016772275.1, NZ_CP065425) is composed of 7 proteins brxHII (WP_202780866.1) pglZA (WP_202778092.1) pglX1 (WP_202778093.1) pglX1 (WP_202778094.1) brxC (WP_202778095.1) brxB_DUF1788 (WP_246483846.1) brxA_DUF1819 (WP_202778096.1) 

![brex_vi](/brex/BREX_VI.svg){max-width=750px}

The BREX_VI system in *Virgibacillus halodenitrificans* (GCF_001878675.1, NZ_CP017962) is composed of 8 proteins brxE (WP_071648637.1) brxA_DUF1819 (WP_071648638.1) brxB_DUF1788 (WP_071648639.1) brxC (WP_197035844.1) pglX1 (WP_071648641.1) pglZA (WP_071648642.1) brxD (WP_071648643.1) brxHI (WP_071648644.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation


<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Gordeeva_2017[<a href='https://doi.org/10.1093/nar/gky1125'>Gordeeva et al., 2019</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_085962535.1'>WP_085962535.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000566901.1'>WP_000566901.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001019648.1'>WP_001019648.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_021524842.1'>WP_021524842.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001180895.1'>WP_001180895.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001193074.1'>WP_001193074.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda
    Goldfarb_2015[<a href='https://doi.org/10.15252/embj.201489455'>Goldfarb et al., 2015</a>] --> Origin_1
    Origin_1[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/ZP_02596040.1'>ZP_02596040.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ZP_02596039.1'>ZP_02596039.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ZP_02596038.1'>ZP_02596038.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ZP_02596037.1'>ZP_02596037.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ZP_02596036.1'>ZP_02596036.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ZP_02596035.1'>ZP_02596035.1</a>] --> Expressed_1[Bacillus subtilis ]
    Expressed_1[Bacillus subtilis ] ----> SPbeta & SP16 & Zeta & phi3T & SPO2 & SPO1 & SP82G
    subgraph Title1[Reference]
        Gao_2020
        Gordeeva_2017
        Goldfarb_2015
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        Lambda
        Lambda
        SPbeta
        SP16
        Zeta
        phi3T
        SPO2
        SPO1
        SP82G
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

