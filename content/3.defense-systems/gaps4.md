---
title: GAPS4
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.03.28.534373
      abstract: |
        Bacteria are found in ongoing conflicts with rivals and predators, which lead to an evolutionary arms race and the development of innate and adaptive immune systems. Although diverse bacterial immunity mechanisms have been recently identified, many remain unknown, and their dissemination within bacterial populations is poorly understood. Here, we describe a widespread genetic element, defined by the Gamma-Mobile-Trio (GMT) proteins, that serves as a mobile bacterial weapons armory. We show that GMT islands have cargo comprising various combinations of secreted antibacterial toxins, anti-phage defense systems, and secreted anti-eukaryotic toxins. This finding led us to identify four new anti-phage defense systems encoded within GMT islands and reveal their active domains and mechanisms of action. We also find the phage protein that triggers the activation of one of these systems. Thus, we can identify novel toxins and defense systems by investigating proteins of unknown function encoded within GMT islands. Our findings imply that the concept of "defense islands" may be broadened to include other types of bacterial innate immunity mechanisms, such as antibacterial and anti-eukaryotic toxins that appear to stockpile with anti-phage defense systems within GMT weapon islands.
contributors:
    - Hugo Vaysset
relevantAbstracts:
    - doi: 10.1101/2023.03.28.534373
---

# GAPS4

## Description

GAPS4 is a two genes system (GAPS4a and GAPS4b). GAPS stands for GMT-encoded Anti-Phage System. 

GAPS4 is present in both Gram-positive and Gram-negative GAPS4 activity was assessed in *E. coli* and was shown to be active against T4, P1-vir and lambda-vir and to reduce lysis plaque size of T7 :ref{doi=10.1101/2023.03.28.534373}. 

## Molecular mechanism
GAPS4a is a nuclease containing a domain from the PDDEXK clan (CL0236) suggesting that the GAPS4 defense system is acting via DNA degradation. Both genes are required for phage defense and were predicted to form a heterodimer. It was shown that the system causes host DNA degradation upon infection by lambda-vir only, which would suggest that GAPS4 is an abortive infection system :ref{doi=10.1101/2023.03.28.534373}.


## Example of genomic structure

The GAPS4 is composed of 2 proteins: GAPS4a and GAPS4b.

Here is an example found in the RefSeq database:

![gaps4](/gaps4/GAPS4.svg){max-width=750px}

The GAPS4 system in *Hafnia alvei* (GCF_902387815.1, NZ_LR699008) is composed of 2 proteins GAPS4a (WP_111329122.1) GAPS4b (WP_111329123.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Mahata_2023[<a href='https://doi.org/10.1101/2023.03.28.534373'>Mahata et al., 2023</a>] --> Origin_0
    Origin_0[Vibrio parahaemolyticus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_055466293.1'>WP_055466293.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_055466294.1'>WP_055466294.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7 & T4 & P1-vir & Lambda-vir
    subgraph Title1[Reference]
        Mahata_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T7
        T4
        P1-vir
        Lambda-vir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

