---
title: PD-T4-6
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse *E. coli* strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in *E. coli*, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in *E. coli* and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00069, PF03793, PF07714
contributors: 
    - Marian Dominguez-Mirazo
relevantAbstracts:
    - doi: 10.1038/s41564-022-01219-4

---

# PD-T4-6
## Description
The PD-T4-6 system is composed of a single protein. It was discovered via a selection screening of 71 *E. coli* strains challenged with diverse phage. The name stands from Phage Defense (PD) and the phage with which the strain was challenged (T4) :ref{doi=10.1038/s41564-022-01219-4}. The system has been identified as an Abortive Infection (Abi) system :ref{doi=10.1038/s41564-022-01219-4,10.1016/j.mib.2023.102312}. The protein was found within a P2-like prophage and contains a predicted Der/Thr kinase domain. Site-specific mutation in the domain reduces phage protection :ref{doi=10.1038/s41564-022-01219-4}. 

## Molecular mechanisms
As far as we are aware, the molecular mechanism is unknown.

## Example of genomic structure

The PD-T4-6 is composed of 1 protein: PD-T4-6.

Here is an example found in the RefSeq database:

![pd-t4-6](/pd-t4-6/PD-T4-6.svg){max-width=750px}

The PD-T4-6 system in *Streptococcus ruminantium* (GCF_021654555.1, NZ_AP025333) is composed of 1 protein: PD-T4-6 (WP_024532776.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RRM76169.1'>RRM76169.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

