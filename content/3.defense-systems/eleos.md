---
title: Eleos
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00350, PF01926, PF18709
contributors: 
    - Alba Herrero del Valle
relevantAbstracts:
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1101/2022.12.12.520048
---

# Eleos

## Description

\The Eleos (for the Greek goddess of mercy) system was previously described as the Dynamins-like system in :ref{doi=10.1016/j.chom.2022.09.017}. It is formed by the LeoA and LeoBC proteins. LeoBC has been found to be analogous to GIMAPs (GTPases immunity-associated proteins), that are interferon-inducible :ref{doi=10.1101/2022.12.12.520048}. LeoA in *E. coli* ETEC H10407 localizes to the periplasm and has been suggested to potentiate bacterial virulence. Its crystal structure has been solved :ref{doi=10.1371/journal.pone.0107211}. Eleos from *Bacillus vietnamensis* NBRC 101237 has been found to protect against jumbo-phages in *Bacillus subtilis* :ref{doi=10.1016/j.chom.2022.09.017}.

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The Eleos system is composed of 2 proteins: LeoA and, LeoBC. Sometimes, the system is in three genes: LeoA, LeoB and LeoC.

Here is an example found in the RefSeq database:

![eleos](/eleos/Eleos.svg){max-width=750px}

The Eleos system in *Synechococcus sp. CBW1002* (GCF_015840915.1, NZ_CP060398) is composed of 2 proteins LeoBC (WP_197170386.1) LeoA (WP_197170387.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus vietnamensis 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2732656981'>2732656981</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2732656980'>2732656980</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> AR9 & PBS1
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        AR9
        PBS1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
