---
title: Butters_gp30_gp31
layout: article
tableColumns:
    article:
      doi: 10.1128/mSystems.00534-20
      abstract: |
        Many sequenced bacterial genomes, including those of pathogenic bacteria, contain prophages. Some prophages encode defense systems that protect their bacterial host against heterotypic viral attack. Understanding the mechanisms undergirding these defense systems is crucial to appreciate the scope of bacterial immunity against viral infections and will be critical for better implementation of phage therapy that would require evasion of these defenses. Furthermore, such knowledge of prophage-encoded defense mechanisms may be useful for developing novel genetic tools for engineering phage-resistant bacteria of industrial importance., A diverse set of prophage-mediated mechanisms protecting bacterial hosts from infection has been recently uncovered within cluster N mycobacteriophages isolated on the host, Mycobacterium smegmatis mc2155. In that context, we unveil a novel defense mechanism in cluster N prophage Butters. By using bioinformatics analyses, phage plating efficiency experiments, microscopy, and immunoprecipitation assays, we show that Butters genes located in the central region of the genome play a key role in the defense against heterotypic viral attack. Our study suggests that a two-component system, articulated by interactions between protein products of genes 30 and 31, confers defense against heterotypic phage infection by PurpleHaze (cluster A/subcluster A3) or Alma (cluster A/subcluster A9) but is insufficient to confer defense against attack by the heterotypic phage Island3 (cluster I/subcluster I1). Therefore, based on heterotypic phage plating efficiencies on the Butters lysogen, additional prophage genes required for defense are implicated and further show specificity of prophage-encoded defense systems., IMPORTANCE Many sequenced bacterial genomes, including those of pathogenic bacteria, contain prophages. Some prophages encode defense systems that protect their bacterial host against heterotypic viral attack. Understanding the mechanisms undergirding these defense systems is crucial to appreciate the scope of bacterial immunity against viral infections and will be critical for better implementation of phage therapy that would require evasion of these defenses. Furthermore, such knowledge of prophage-encoded defense mechanisms may be useful for developing novel genetic tools for engineering phage-resistant bacteria of industrial importance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: ''
contributors: 
  - Helena Shomar
  - Marie Guillaume
relevantAbstracts:
  - doi: 10.1128/mSystems.00534-20

---

# Butters_gp30_gp31

## Description
The anti-phage defense system Butters_gp30_gp31 is encoded in the genomes of Actinobacteria, including in prophages.
It was experimentally validated in the host *Mycobacterium smegmatis* and displayed resistance against phages PurpleHaze and Alma.  

## Molecular mechanism
To our knowledge, the mechanism of action remains unknown.
However, the proteins of this system are predicted as a cytoplasmic protein (gp30) and a 4-pass transmembrane protein (gp31). The proposed mechanism of action is hypothesized to resemble the RexA/B system of coliphage Lambda, where activation of gp30 by phage infection stimulates the ion channel gp31 which causes membrane depolarization and loss of intracellular ATP, which in turn, causes abortive infection.
 
## Example of genomic structure

The Butters_gp30_gp31 is composed of 2 proteins: Butters_gp30 and Butters_gp31.

Here is an example found in the RefSeq database:

![butters_gp30_gp31](/butters_gp30_gp31/Butters_gp30_gp31.svg){max-width=750px}

The Butters_gp30_gp31 system in *Snodgrassella alvi* (GCF_022870965.1, NZ_CP091515) is composed of 2 proteins Butters_gp31 (WP_025330184.1) CarolAnn_gp44 (WP_144353282.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Mageeney_2020[<a href='https://doi.org/10.1128/mSystems.00534-20'>Mageeney et al., 2020</a>] --> Origin_0
    Origin_0[Mycobacterium phage Butters 
<a href='https://ncbi.nlm.nih.gov/protein/YP_007869841.1'>YP_007869841.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/YP_007869842.1'>YP_007869842.1</a>] --> Expressed_0[Mycobacterium smegmatis]
    Expressed_0[Mycobacterium smegmatis] ----> PurpleHaze
    Expressed_0[Mycobacterium smegmatis] ----> Alma
    subgraph Title1[Reference]
        Mageeney_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        PurpleHaze
        Alma
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
