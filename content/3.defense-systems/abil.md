---
title: AbiL
layout: article
tableColumns:
    article:
      doi: 10.1111/j.1574-6968.1997.tb10185.x
      abstract: |
        A 16-kb plasmid (pND859) was identified from Lactococcus lactis biovar. diacetylactis UK12922 which encodes phage resistance to the small isometric phage 712 when tested in L. lactis LM0230. The gene encoding phage abortive infection, designated abi-859, was localized on a 1.2-kb region which consists of an open reading frame (ORF) of 846 bp preceded by a potential ribosome-binding site and a putative promoter region. A helix-turn-helix region typical of DNA-binding motifs was identified near the N-terminal of the abi-859 product, suggesting a possible interaction with the phage DNA.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13175, PF13304, PF13707
contributors: 
  - Ernest Mordret
relevantAbstracts:
  - doi: 10.1111/j.1574-6968.1997.tb10185.x
  - doi: 10.1016/j.mib.2005.06.006

---

# AbiL

## Description

AbiL was discovered in Lactococcus lactis. A plasmid containing the defense system was found to prevent infection by phage 712 through an abortive infection system

## Molecular mechanism

Abortive infection. the PF13707 domain includes the RloB protein that is found within a bacterial restriction-modification operon. This family includes the AbiLii protein that is found as part of a plasmid-encoded phage abortive infection mechanism. Deletion within abiLii abolished the phage resistance. The family includes some proteins annotated as CRISPR Csm2 proteins.

## Example of genomic structure

The AbiL is composed of 2 proteins: AbiLi and AbiLii.

Here is an example found in the RefSeq database:

![abil](/abil/AbiL.svg){max-width=750px}

The AbiL system in *Sulfurimonas sp. SWIR-19* (GCF_020410885.1, NZ_CP084578) is composed of 2 proteins AbiLi2 (WP_226066409.1) AbiLii2 (WP_226066410.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAB53710.1'>AAB53710.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AAB53711.1'>AAB53711.1</a>] --> Expressed_0[lactococci ]
    Expressed_0[lactococci ] ----> 936 & c2
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>




