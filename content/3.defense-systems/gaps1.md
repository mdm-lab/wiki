---
title: GAPS1
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.03.28.534373
      abstract: |
        Bacteria are found in ongoing conflicts with rivals and predators, which lead to an evolutionary arms race and the development of innate and adaptive immune systems. Although diverse bacterial immunity mechanisms have been recently identified, many remain unknown, and their dissemination within bacterial populations is poorly understood. Here, we describe a widespread genetic element, defined by the Gamma-Mobile-Trio (GMT) proteins, that serves as a mobile bacterial weapons armory. We show that GMT islands have cargo comprising various combinations of secreted antibacterial toxins, anti-phage defense systems, and secreted anti-eukaryotic toxins. This finding led us to identify four new anti-phage defense systems encoded within GMT islands and reveal their active domains and mechanisms of action. We also find the phage protein that triggers the activation of one of these systems. Thus, we can identify novel toxins and defense systems by investigating proteins of unknown function encoded within GMT islands. Our findings imply that the concept of "defense islands" may be broadened to include other types of bacterial innate immunity mechanisms, such as antibacterial and anti-eukaryotic toxins that appear to stockpile with anti-phage defense systems within GMT weapon islands.
    Sensor: Phage-protein sensing
    Activator: Unknown
    Effector: Unknown
contributors:
    - Marian Dominguez-Mirazo
relevantAbstracts:
    - doi: 10.1101/2023.03.28.534373
---

# GAPS1

## Description

The GAPS1 system is composed of a single protein. It was found in Gamma-Mobile-Trio (GMT) protein containing genomic island in *Vibrio*, and cloned into *E. coli* K-12 :ref{doi=10.1101/2023.03.28.534373}. The name GAPS derives from the "GMT-encoded Anti-Phage System" acronym. GAPS1 contains a predicted nuclease domain whose mutation prevents defense activity, however DNA degradation was not detected in targeted phage :ref{doi=10.1101/2023.03.28.534373}. Mutations in the folded capsid protein (Gp10) of phage T7 result in an escape phenotype, with GAPS1 shown to be activated upon Gp10 expression, suggesting activation of the system at late stages of the infection cycle :ref{doi=10.1101/2023.03.28.534373}. 

## Molecular mechanisms
The molecular mechanism remains to be fully elucidated. 


## Example of genomic structure

The GAPS1 is composed of 1 protein: GAPS1.

Here is an example found in the RefSeq database:

![gaps1](/gaps1/GAPS1.svg){max-width=750px}

The GAPS1 system in *Hydrogenophaga sp. BPS33* (GCF_009859475.1, NZ_CP044549) is composed of 1 protein: GAPS1 (WP_159591124.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Mahata_2023[<a href='https://doi.org/10.1101/2023.03.28.534373'>Mahata et al., 2023</a>] --> Origin_0
    Origin_0[Vibrio parahaemolyticus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_005477165.1'>WP_005477165.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7 & P1-vir
    subgraph Title1[Reference]
        Mahata_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T7
        P1-vir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


