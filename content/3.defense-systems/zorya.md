---
title: Zorya
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00176, PF00271, PF00691, PF04851, PF15611
contributors:
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1093/nar/gkab883
    - doi: 10.1126/science.aar4120
    - doi: 10.1101/2023.12.18.572097
---

# Zorya

## Description

The Zorya defense system was discovered in 2018 :ref{doi=10.1126/science.aar4120} among other systems.

All subtypes of Zorya are composed of at least two proteins named ZorA and ZorB. In certain subtypes, additional proteins can be involved in the system (ZorC and ZorD in type I Zorya, ZorE in type II, or ZorF and ZorG type III).

## Molecular mechanism

The molecular mechanism of type I Zoya was recently described :ref{doi=10.1101/2023.12.18.572097}. ZorA and ZorB were shown to form a transmembrane heteromer with 5:2 (ZorA:ZorB) stoechiometry. The protein complex acts as a peptidoglycan-binding rotary motor. The structure shows that the 5 ZorA proteins form a long tail inside the cytoplasm. Part of this tail has amino acid homology with the core signaling unit of the bacterial chemosensory array suggesting that the ZorA tail is responsible for the activation of ZorC and ZorD :ref{doi=10.1016/j.sbi.2023.102565}.

On the other hand, ZorC and ZorD interact with DNA :ref{doi=10.1101/2023.12.18.572097} and ZorD acts as an ATP-dependent nuclease.

With all those results, the mechanism suggested by the authors is :ref{doi=10.1101/2023.12.18.572097}:
- Detection of the phage infection by the ZorAB complex (sensing either cell wall depression or inner membrane curvature)
- Signal transduction from ZorA tail to ZorC and ZorD
- DNA binding by ZorC/D and DNA cleavage by ZorD close to the injection site.


## Example of genomic structure

A total of 2 subsystems have been described for the Zorya system.

Here is some examples found in the RefSeq database:

![zorya_typei](/zorya/Zorya_TypeI.svg){max-width=750px}

The Zorya_TypeI system in *Erwinia pyrifoliae* (GCF_000027265.1, NC_012214) is composed of 4 proteins ZorA (WP_012666948.1) ZorB (WP_012666949.1) ZorC (WP_012666950.1) ZorD (WP_012666951.1) 

![zorya_typeii](/zorya/Zorya_TypeII.svg){max-width=750px}

The Zorya_TypeII system in *Pectobacterium aroidearum* (GCF_015689195.1, NZ_CP065044) is composed of 3 proteins ZorA2 (WP_205947278.1) ZorB (WP_205947279.1) ZorE (WP_205947280.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[ Type I
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/ABV17222.1'>ABV17222.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ABV17786.1'>ABV17786.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ABV16709.1'>ABV16709.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ABV17208.1'>ABV17208.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi27 & T7
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[ Type II
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/ACA79490.1'>ACA79490.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ACA79491.1'>ACA79491.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ACA79492.1'>ACA79492.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7 & SECphi17
    Payne_2021[<a href='https://doi.org/10.1093/nar/gkab883'>Payne et al., 2021</a>] --> Origin_2
    Origin_2[ Type III
Stenotrophomonas nitritireducens 
<a href='https://ncbi.nlm.nih.gov/protein/WP_055768786.1'>WP_055768786.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_055768783.1'>WP_055768783.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_055768781.1'>WP_055768781.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_055768778.1'>WP_055768778.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T1 & T4 & T7 & LambdaVir & PVP-SE1
    subgraph Title1[Reference]
        Doron_2018
        Payne_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Protects against]
        LambdaVir
        SECphi27
        T7
        T7
        SECphi17
        T1
        T4
        T7
        LambdaVir
        PVP-SE1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


