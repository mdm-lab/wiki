---
title: AbiC
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF16872
contributors: 
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1128/jb.174.22.7463-7469.1992
---

# AbiC

## Description

AbiC was discovered in *Lactococcus lactis* plasmid in 1992 :ref{doi=10.1128/jb.174.22.7463-7469.1992}

AbiC is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiC is classified as abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

AbiC is a single gene system composed of the protein AbiC.  

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown.


## Example of genomic structure

The AbiC is composed of 1 protein: AbiC.

Here is an example found in the RefSeq database:

![abic](/abic/AbiC.svg){max-width=750px}

The AbiC system in *Cupriavidus pauculus* (GCF_008693385.1, NZ_CP044066) is composed of 1 protein: AbiC (WP_150373977.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae's  PICI KpCIFDAARGOS_1313 
<a href='https://ncbi.nlm.nih.gov/protein/QRS09118.1'>QRS09118.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & Lambda & HK97 & HK544 & HK578 & T7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae's  PICI KpCIFDAARGOS_1313 
<a href='https://ncbi.nlm.nih.gov/protein/QRS09118.1'>QRS09118.1</a>] --> Expressed_1[Salmonella enterica]
    Expressed_1[Salmonella enterica] ----> P22 & BTP1 & ES18
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae's  PICI KpCIFDAARGOS_1313 
<a href='https://ncbi.nlm.nih.gov/protein/QRS09118.1'>QRS09118.1</a>] --> Expressed_2[Klebsiella pneumoniae]
    Expressed_2[Klebsiella pneumoniae] ----> Pokey & Raw & Eggy
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_1
    Origin_1[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAA53569.1'>AAA53569.1</a>] --> Expressed_3[lactococci]
    Expressed_3[lactococci] ----> 936 & P335
    subgraph Title1[Reference]
        Fillol-Salom_2022
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Protects against]
        T5
        Lambda
        HK97
        HK544
        HK578
        T7
        P22
        BTP1
        ES18
        Pokey
        Raw
        Eggy
        936
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

