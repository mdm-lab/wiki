---
title: Borvo
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF12770

contributors:
    - Héloïse Georjon

relevantAbstracts:
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1016/j.cell.2023.02.029    
---

# Borvo

## Description
Borvo is a single-gene anti-phage system that was identified through bioinformatic prediction and experimental validation :ref{doi=10.1016/j.chom.2022.09.017}.

## Molecular mechanisms
Mutations in the phage DNA polymerase can allow phages to escape Borvo defense, indicating that it could be the trigger of the system :ref{doi=10.1016/j.cell.2023.02.029}. Borvo is a suspected abortive infection :ref{doi=10.1016/j.cell.2023.02.029}. However, as far as we are aware, the precise molecular mechanism of Borvo is unknown.

## Example of genomic structure

The Borvo is composed of 1 protein: BovA.

Here is an example found in the RefSeq database:

![borvo](/borvo/Borvo.svg){max-width=750px}

The Borvo system in *Enterobacter cloacae complex sp. ECL78* (GCF_019056615.1, NZ_CP077660) is composed of 1 protein: BovA (WP_126517459.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2646127138'>2646127138</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T5
        SECphi4
        SECphi6
        SECphi18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


