---
title: DdmDE
layout: article
tableColumns:
    article:
      doi: 10.1038/s41586-022-04546-y
      abstract: |
        Horizontal gene transfer can trigger rapid shifts in bacterial evolution. Driven by a variety of mobile genetic elements—in particular bacteriophages and plasmids—the ability to share genes within and across species underpins the exceptional adaptability of bacteria. Nevertheless, invasive mobile genetic elements can also present grave risks to the host; bacteria have therefore evolved a vast array of defences against these elements1. Here we identify two plasmid defence systems conserved in the Vibrio cholerae El Tor strains responsible for the ongoing seventh cholera pandemic2-4. These systems, termed DdmABC and DdmDE, are encoded on two major pathogenicity islands that are a hallmark of current pandemic strains. We show that the modules cooperate to rapidly eliminate small multicopy plasmids by degradation. Moreover, the DdmABC system is widespread and can defend against bacteriophage infection by triggering cell suicide (abortive infection, or Abi). Notably, we go on to show that, through an Abi-like mechanism, DdmABC increases the burden of large low-copy-number conjugative plasmids, including a broad-host IncC multidrug resistance plasmid, which creates a fitness disadvantage that counterselects against plasmid-carrying cells. Our results answer the long-standing question of why plasmids, although abundant in environmental strains, are rare in pandemic strains; have implications for understanding the dissemination of antibiotic resistance plasmids; and provide insights into how the interplay between two defence systems has shaped the evolution of the most successful lineage of pandemic V. cholerae.
relevantAbstracts:
    - doi: 10.1038/s41586-022-04546-y
---

# DdmDE
## Example of genomic structure

The DdmDE is composed of 2 proteins: DdmE and DdmD.

Here is an example found in the RefSeq database:

![ddmde](/ddmde/DdmDE.svg){max-width=750px}

The DdmDE system in *Vibrio coralliilyticus* (GCF_000772065.1, NZ_CP009617) is composed of 2 proteins DdmD (WP_043007104.1) DdmE (WP_043007106.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

::info
This section is empty
::