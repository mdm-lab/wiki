---
title: AbiU
layout: article
tableColumns:
    article:
      doi: 10.1128/AEM.67.11.5225-5232.2001
      abstract: |
        This study reports on the identification and characterization of a novel abortive infection system, AbiU, from Lactococcus lactis. AbiU confers resistance to phages from the three main industrially relevant lactococcal phage species: c2, 936, and P335. The presence of AbiU reduced the efficiency of plaquing against specific phage from each species as follows: 3.7 × 10−1, 1.0 × 10−2, and 1.0 × 10−1, respectively. abiU involves two open reading frames,abiU1 (1,772 bp) and abiU2 (1,019 bp). Evidence indicates that AbiU1 is responsible for phage resistance and that AbiU2 may downregulate phage resistance against 936 and P335 type phages but not c2 type phage. AbiU appeared to delay transcription of both phage 712 and c2, with the effect being more marked on phage c2.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF10592
contributors: 
    - Nathalie Bechon
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1128/AEM.67.11.5225-5232.2001
---

# AbiU

## Description
AbiU is a single-protein abortive infection defense system described in *Lactococcus*. 

## Molecular mechanism
The molecular mechanism of AbiU is not well understood. It was shown that cells expressing AbiU showed delayed transcription of phage DNA, although how it is achieved, or how it protects the bacterial culture is not understood. AbiU was shown to be encoded near another gene that seems to be an inhibitor of defense :ref{doi=10.1128/AEM.67.11.5225-5232.2001}.

## Example of genomic structure

The AbiU is composed of 1 protein: AbiU.

Here is an example found in the RefSeq database:

![abiu](/abiu/AbiU.svg){max-width=750px}

The AbiU system in *Bacillus subtilis* (GCF_001889385.1, NZ_CP018295) is composed of 1 protein: AbiU (WP_072589761.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAG17058.'>AAG17058.</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

