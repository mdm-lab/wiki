---
title: Mokosh
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00069, PF07714, PF08378, PF13086, PF13087, PF13091, PF13245, PF13604
contributors:
    - Marian Dominguez-Mirazo
relevantAbstract:
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1101/2022.12.12.520048
---

# Mokosh

## Description
The Mokosh system was discovered in *E. coli* by examining clusters of genes enriched in defense islands :ref{doi=10.1016/j.chom.2022.09.017}. It contains genes with an RNA helicase domain and a predicted phospholipase D domain (PLD) nuclease domain. Mutations in the ATP-binding domain of the helicase, and the active site of the PLD nuclease disrupt phage defense. The system is divided into two types. Mokosh type I has two genes, one gene containing the RNA helicase domain and an additional serine-threonine kinase domain (STK), and one gene containing the PLD nuclease. Type II Mokosh is formed of a single gene containing both the helicase and nuclease domains. Recent efforts have shown homology between the Mokosh system and human proteins involved in the piRNA pathway, a defense mechanism of animal germlines that prevents expression of transposable elements :ref{doi=10.1101/2022.12.12.520048}. The system gets its name from the goddess protector of women's destiny in Slavic mythology :ref{doi=10.1016/j.chom.2022.09.017}.

## Molecular mechanisms
As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

A total of 7 subsystems have been described for the Mokosh system.

Here is some examples found in the RefSeq database:

![mokosh_typeii](/mokosh/Mokosh_TypeII.svg){max-width=750px}

The Mokosh_TypeII system in *Massilia sp. NP310* (GCF_019443485.1, NZ_CP080379) is composed of 1 protein: MkoC (WP_184237977.1) 

![mokosh_type_i_a](/mokosh/Mokosh_Type_I_A.svg){max-width=750px}

The Mokosh_Type_I_A system in *Cupriavidus pauculus* (GCF_019931045.1, NZ_CP082863) is composed of 2 proteins MkoA_A (WP_170292610.1) MkoB_A (WP_150981546.1) 

![mokosh_type_i_b](/mokosh/Mokosh_Type_I_B.svg){max-width=750px}

The Mokosh_Type_I_B system in *Collimonas pratensis* (GCF_001584185.1, NZ_CP013234) is composed of 2 proteins MkoB_B (WP_061945389.1) MkoA_B (WP_061945390.1) 

![mokosh_type_i_c](/mokosh/Mokosh_Type_I_C.svg){max-width=750px}

The Mokosh_Type_I_C system in *Klebsiella sp. WP7-S18-CRE-02* (GCF_014169515.1, NZ_AP022189) is composed of 2 proteins MkoB_C (WP_227674162.1) MkoA_C (WP_227674163.1) 

![mokosh_type_i_d](/mokosh/Mokosh_Type_I_D.svg){max-width=750px}

The Mokosh_Type_I_D system in *Geomonas paludis* (GCF_023221575.1, NZ_CP096574) is composed of 2 proteins MkoA_D (WP_248647094.1) MkoB_D (WP_248647095.1) 

![mokosh_type_i_e](/mokosh/Mokosh_Type_I_E.svg){max-width=750px}

The Mokosh_Type_I_E system in *Planococcus faecalis* (GCF_002009235.1, NZ_CP019401) is composed of 2 proteins MkoA_E (WP_078080177.1) MkoB_E (WP_071154541.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[ Type I
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2597791081'>2597791081</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2597791080'>2597791080</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & LambdaVir & T5 & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_1
    Origin_1[ Type I
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2720391838'>2720391838</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2720391839'>2720391839</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> LambdaVir
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[ Type II
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2586378093'>2586378093</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> SECphi17
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
        LambdaVir
        T5
        SECphi27
        LambdaVir
        SECphi17
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



