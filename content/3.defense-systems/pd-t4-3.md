---
title: PD-T4-3
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors:
    - Ernest Mordret
relevant abstracts:
    - doi: 10.1038/s41564-022-01219-4
---

# PD-T4-3
 
## Description

PD-T4-3 is a single gene system isolated during an experimental screen of fosmids derived from 71 strains of diverse *E.coli*, and cloned back in a derivative of K12. Fragments containing the PD-T4-3 gene were shown to provide resistance to phage T4. The protein is characterized by a GIY-YIG nuclease domain, and mutations inactivating this domain abolished the resistance phenotype. It was originally found on a µ-like prophage

## Molecular mechanism

the molecular mechanism of this system is unknown

## Example of genomic structure

The PD-T4-3 is composed of 1 protein: PD-T4-3.

Here is an example found in the RefSeq database:

![pd-t4-3](/pd-t4-3/PD-T4-3.svg){max-width=750px}

The PD-T4-3 system in *Morganella morganii* (GCF_018802485.1, NZ_CP064826) is composed of 1 protein: PD-T4-3 (WP_214179544.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCO27183.1'>RCO27183.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
---
::

