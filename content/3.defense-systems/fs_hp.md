---
title: FS_HP
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2022.07.014
      abstract: |
        Bacteria encode sophisticated anti-phage systems that are diverse and versatile and display high genetic mobility. How this variability and mobility occurs remains largely unknown. Here, we demonstrate that a widespread family of pathogenicity islands, the phage-inducible chromosomal islands (PICIs), carry an impressive arsenal of defense mechanisms, which can be disseminated intra- and inter-generically by helper phages. These defense systems provide broad immunity, blocking not only phage reproduction, but also plasmid and non-cognate PICI transfer. Our results demonstrate that phages can mobilize PICI-encoded immunity systems to use them against other mobile genetic elements, which compete with the phages for the same bacterial hosts. Therefore, despite the cost, mobilization of PICIs may be beneficial for phages, PICIs, and bacteria in nature. Our results suggest that PICIs are important players controlling horizontal gene transfer and that PICIs and phages establish mutualistic interactions that drive bacterial ecology and evolution.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors:
    - Marian Dominguez-Mirazo
relevantAbstracts:
    - doi: 10.1016/j.cell.2022.07.014
---

# FS_HP

## Description

PICIs (Phage-inducible chromosomal islands) are highly mobile genetic elements that reside in the bacterial chromosome in the absence of a helper phage. Following infection by the helper phage, PICIs excise and replicate by hijacking the helper phage machinery. The FS_HP system was discovered in *E. _fergusonii_ through the manual search for immune systems in flanking regions of gram-negative PICIs :ref{doi=10.1016/j.cell.2022.07.014}. It is composed of a single protein with a hypothetical domain, from which it derives the HP part of its name. The system showcases a broad defense spectrum. It was tested against 15 lytic phages in 3 gram-negative bacteria and protected the bacterial host against 3 unrelated phages in 2 different bacteria species. FS_HP also blocked the formation of phage particles upon induction of the P22 *S. enterica* prophage. Therefore, the system can block phage in both lytic and lysogenic life cycles. It was also shown to reduce the production of transducing particles. 

## Molecular mechanisms

As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The FS_HP is composed of 1 protein: HP.

Here is an example found in the RefSeq database:

![fs_hp](/fs_hp/FS_HP.svg){max-width=750px}

The FS_HP system in *Escherichia coli* (GCF_016944735.1, NZ_CP063511) is composed of 1 protein: HP (WP_227456581.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia fergusonii 
<a href='https://ncbi.nlm.nih.gov/protein/QML19489.1'>QML19489.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> HK578
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia fergusonii 
<a href='https://ncbi.nlm.nih.gov/protein/QML19489.1'>QML19489.1</a>] --> Expressed_1[Salmonella enterica]
    Expressed_1[Salmonella enterica] ----> P22 & ES18
    subgraph Title1[Reference]
        Fillol-Salom_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        HK578
        P22
        ES18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

