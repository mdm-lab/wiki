---
title: Rst_TIR-NLR
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13676
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.02.018
---

# Rst_TIR-NLR

## Description

The Rst_TIR-NLR system is named after the first author of the paper describing it (Rousset et al. 2022) and the domains of the only protein it contains (TIR, toll-interleukin-1 receptor, and NLR, nucleotide-binding leucine-rich repeat receptor) :ref{doi=10.1016/j.chom.2022.02.018}.

This system is very similar to other bacterial defense systems: bNACHT (https://defense-finder.pasteur.cloud/wiki/defense-systems/nlr), CARD_NLR (https://defense-finder.pasteur.cloud/wiki/defense-systems/card_nlr), Avs (AVAST) (https://defense-finder.pasteur.cloud/wiki/defense-systems/avs). 

## Molecular mechanisms

As far as we are aware, the molecular mechanism is unknown. However, the molecular mechanism of closely related systems was previously studied. See bNACHT (https://defense-finder.pasteur.cloud/wiki/defense-systems/nlr), CARD_NLR (https://defense-finder.pasteur.cloud/wiki/defense-systems/card_nlr) and Avs (AVAST) (https://defense-finder.pasteur.cloud/wiki/defense-systems/avs)

## Example of genomic structure

The Rst_TIR-NLR is composed of 1 protein: TIR.

Here is an example found in the RefSeq database:

![rst_tir-nlr](/rst_tir-nlr/Rst_TIR-NLR.svg){max-width=750px}

The Rst_TIR-NLR system in *Methylosinus sp. C49* (GCF_009936375.1, NZ_AP022332) is composed of 1 protein: TIR (WP_161914415.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_044784989.1'>WP_044784989.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4 & P1 & CLB_P2 & LF82_P8 & AL505_P2 & T7
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T4
        P1
        CLB_P2
        LF82_P8
        AL505_P2
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

