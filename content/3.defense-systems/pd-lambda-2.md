---
title: PD-Lambda-2
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF06114, PF09907, PF14350
relevantAbstracts:
    - doi: 10.1038/s41564-022-01219-4
---

# PD-Lambda-2
## Example of genomic structure

The PD-Lambda-2 is composed of 3 proteins: PD-Lambda-2_A, PD-Lambda-2_B and PD-Lambda-2_C.

Here is an example found in the RefSeq database:

![pd-lambda-2](/pd-lambda-2/PD-Lambda-2.svg){max-width=750px}

The PD-Lambda-2 system in *Vibrio parahaemolyticus* (GCF_013393845.1, NZ_CP040100) is composed of 3 proteins PD-Lambda-2_C (WP_116869984.1) PD-Lambda-2_B (WP_029809306.1) PD-Lambda-2_A (WP_029809305.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCO93357.1'>RCO93357.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/RCO93356.1'>RCO93356.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/RCO93355.1'>RCO93355.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi17 & SECphi18 & SECphi27 & T3
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        LambdaVir
        SECphi17
        SECphi18
        SECphi27
        T3
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

