---
title: Panchino_gp28
layout: article
tableColumns:
    article:
      doi: 10.1038/nmicrobiol.2016.251
      abstract: |
        Temperate phages are common, and prophages are abundant residents of sequenced bacterial genomes. Mycobacteriophages are viruses that infect mycobacterial hosts including Mycobacterium tuberculosis and Mycobacterium smegmatis, encompass substantial genetic diversity and are commonly temperate. Characterization of ten Cluster N temperate mycobacteriophages revealed at least five distinct prophage-expressed viral defence systems that interfere with the infection of lytic and temperate phages that are either closely related (homotypic defence) or unrelated (heterotypic defence) to the prophage. Target specificity is unpredictable, ranging from a single target phage to one-third of those tested. The defence systems include a single-subunit restriction system, a heterotypic exclusion system and a predicted (p)ppGpp synthetase, which blocks lytic phage growth, promotes bacterial survival and enables efficient lysogeny. The predicted (p)ppGpp synthetase coded by the Phrann prophage defends against phage Tweety infection, but Tweety codes for a tetrapeptide repeat protein, gp54, which acts as a highly effective counter-defence system. Prophage-mediated viral defence offers an efficient mechanism for bacterial success in host-virus dynamics, and counter-defence promotes phage co-evolution.
    PFAM: PF01170, PF02384, PF13588
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1038/nmicrobiol.2016.251
---

# Panchino_gp28

## Description

The Panchino gp28 defense system was described in :ref{doi=10.1038/nmicrobiol.2016.251} and is named after the Panchino prophage (on which it is located) and the corresponding gene. It is a single-gene system.

## Molecular mechanisms

Panchino gp28 acts as a single gene type I restriction system :ref{doi=10.1038/nmicrobiol.2016.251,10.1016/j.mib.2023.102321}.

## Example of genomic structure

The Panchino_gp28 is composed of 1 protein: gp28.

Here is an example found in the RefSeq database:

![panchino_gp28](/panchino_gp28/Panchino_gp28.svg){max-width=750px}

The Panchino_gp28 system in *Burkholderia pseudomallei* (GCF_018228585.1, NZ_CP073731) is composed of 1 protein: gp28 (WP_249212015.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Dedrick_2017[<a href='https://doi.org/10.1038/nmicrobiol.2016.251'>Dedrick et al., 2017</a>] --> Origin_0
    Origin_0[Mycobacterium Panchino phage 
<a href='https://ncbi.nlm.nih.gov/protein/YP_009304936.1'>YP_009304936.1</a>] --> Expressed_0[Mycobacterium smegmatis mc2 155]
    Expressed_0[Mycobacterium smegmatis mc2 155] ----> Tweety & TM4 & Bruita & U2 & 244
    subgraph Title1[Reference]
        Dedrick_2017
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        Tweety
        TM4
        Bruita
        U2
        244
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
