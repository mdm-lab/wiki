---
title: Paris
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Sensing of phage protein
    Activator: Unknown
    Effector: Unknown
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.02.018
---

# Paris

## Description

PARIS (for Phage Anti-Restriction-Induced System) is a novel anti-phage system. It comprises an ATPase associated with a DUF4435 protein, which can be found either as a two-gene cassette or a single-gene fusion :ref{doi=10.1016/j.chom.2022.02.018}.

## Molecular mechanisms

This system relies on an unknown [Abortive infection](/general-concepts/abortive-infection) mechanism to trigger growth arrest upon sensing a phage-encoded protein (Ocr). Interestingly, the Ocr protein has been found to inhibit R-M systems and BREX systems, making PARIS a suitable defense mechanism against RM resistant and/or BREX resistant phages :ref{doi=10.1016/j.chom.2022.02.018,10.1093/nar/gkaa510,10.1007/BF01036001}. 

## Example of genomic structure

A total of 2 subsystems have been described for the Rst_PARIS system.
The classical Paris is composed of two proteins AriA and AriB. The other type of Paris is the fused version with one AriAB protein.

Here is some examples found in the RefSeq database:

![paris_i](/paris/PARIS_I.svg){max-width=750px}

The PARIS_I system in *Streptomyces sampsonii* (GCF_001704195.1, NZ_CP016824) is composed of 2 proteins AriA_AAA15 (WP_079165307.1) AriB_DUF4435_I (WP_143067273.1) 

![paris_i_merge](/paris/PARIS_I_merge.svg){max-width=750px}

The PARIS_I_merge system in *Rhizobium sp. SL42* (GCF_021729845.1, NZ_CP063397) is composed of 1 protein: AAA_15_DUF4435 (WP_237370054.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[ Paris 1
Escherichia coli  P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000334847.1'>WP_000334847.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000342409.1'>WP_000342409.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T4 & CLB_P2 & LF82_P8 & Al505_P2 & T7
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_1
    Origin_1[ Paris 2
Escherichia coli P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001007866.1'>WP_001007866.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000093097.1'>WP_000093097.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> Lambda & T4 & CLB_P2 & LF82_P8 & T7
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        Lambda
        T4
        CLB_P2
        LF82_P8
        Al505_P2
        T7
        Lambda
        T4
        CLB_P2
        LF82_P8
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

