---
title: Gao_Qat
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01026, PF07693
relevantAbstracts:
    - doi: 10.1126/science.aba0372
---

# Gao_Qat
## Example of genomic structure

The Gao_Qat is composed of 4 proteins: QatA, QatB, QatC and QatD.

Here is an example found in the RefSeq database:

![gao_qat](/gao_qat/Gao_Qat.svg){max-width=750px}

The Gao_Qat system in *Ralstonia wenshanensis* (GCF_021173085.1, NZ_CP076413) is composed of 4 proteins QatA (WP_232041545.1) QatB (WP_232041546.1) QatC (WP_232041547.1) QatD (WP_232043074.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/STG85056.1'>STG85056.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/STG85057.1'>STG85057.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/STG85058.1'>STG85058.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/STG85059.1'>STG85059.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & Lambda
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        P1
        Lambda
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

