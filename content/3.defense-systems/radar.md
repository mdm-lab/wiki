---
title: RADAR
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleic acid degrading
contributors: 
  - Hugo Vaysset
  - Aude Bernheim
relevantAbstracts:
  - doi: 10.1016/j.cell.2023.01.026
  - doi: 10.1016/j.cell.2023.01.012
  - doi: 10.1126/science.aba0372
---

# RADAR
## Description

RADAR (Restriction by an Adenosine Deaminase Acting on RNA) is comprised of two genes, encoding respectively for an adenosine triphosphatase (RdrA) and a divergent adenosine deaminase (RdrB), which are in some cases associated with a small membrane protein (RdrC or D). They were first uncovered in a study exploring the content of defense islands :ref{doi=10.1126/science.aba0372}. 

## Molecular mechanism
In the initial study describing RADAR, RADAR was found to perform RNA editing of adenosine to inosine during phage infection :ref{doi=10.1126/science.aba0372}. Editing sites were broadly distributed on the host transcriptome, which could prove deleterious leading to observed growth arrest of RADAR upon phage infection.  
Further structural studies revealed potentially different mechanisms of actions :ref{doi=10.1016/j.cell.2023.01.026,10.1016/j.cell.2023.01.012}. RdrA and RdrB assemble to form a giant 10 MDa complex. The RADAR defense system limits phage replication by catalyzing ATP deamination. Within this system, RdrB functions as an adenosine deaminase, leading to the buildup of ITP (Inosine Tri-Phosphate) and dITP. RdrA induces RdrB activity and potentially regulates the detection of phage infections.


## Example of genomic structure

A total of 2 subsystems have been described for the RADAR system.

Here is some examples found in the RefSeq database:

![radar_i](/radar/radar_I.svg){max-width=750px}

The radar_I system in *Vibrio splendidus* (GCF_022812155.1, NZ_CP089205) is composed of 2 proteins rdrA_I (WP_243585344.1) rdrB_I (WP_243585345.1) 

![radar_ii](/radar/radar_II.svg){max-width=750px}

The radar_II system in *Enterobacter sp. JH25* (GCF_021725435.1, NZ_CP091319) is composed of 3 proteins rdrD_II (WP_022649082.1) rdrB_II (WP_227746616.1) rdrA_II (WP_022649084.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Duncan-Lowey_2022[<a href='https://doi.org/10.1016/j.cell.2023.01.012'>Duncan-Lowey et al., 2023</a>] --> Origin_0
    Origin_0[Citrobacter rodentium 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012906049.1'>WP_012906049.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012906048.1'>WP_012906048.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & T3 & T6
    Duncan-Lowey_2022[<a href='https://doi.org/10.1016/j.cell.2023.01.012'>Duncan-Lowey et al., 2023</a>] --> Origin_1
    Origin_1[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2548796856'>2548796856</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2548796855'>2548796855</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T2 & T4 & T6
    Duncan-Lowey_2022[<a href='https://doi.org/10.1016/j.cell.2023.01.012'>Duncan-Lowey et al., 2023</a>] --> Origin_2
    Origin_2[Streptococcus suis 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2706833061'>2706833061</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2706833062'>2706833062</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T4 & T5 & T6
    subgraph Title1[Reference]
        Gao_2020
        Duncan-Lowey_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Protects against]
        T2
        T4
        T5
        T3
        T6
        T2
        T4
        T5
        T3
        T6
        T2
        T4
        T6
        T2
        T4
        T5
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

