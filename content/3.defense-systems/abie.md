---
title: AbiE
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF08843, PF09407, PF09952, PF11459, PF13338, PF17194
contributors:
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1093/nar/gkt1419
    - doi: 10.1128/aem.61.12.4321-4328.1995
---

# AbiE

## Description
AbiE was discovered in 1995 on a Lactococcal plasmid :ref{doi=10.1128/aem.61.12.4321-4328.1995} together with AbiF a homolog of [AbiD](/defense-systems/abid)](/defense-systems/abid).

AbiE is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiE is classified as abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

AbiE is composed of two proteins: AbiEi and AbiEii. AbiEii is annotated as a nucleotidyltransferase.

## Molecular mechanism

AbiE is a family of anti-phage defense systems. They act through a Toxin-Antitoxin mechanism, and are comprised of a pair of genes, with one gene being toxic while the other confers immunity to this toxicity :ref{doi=10.1093/nar/gkt1419}.

AbiEii is a GTP-binding nucleotidyltransferase (NTase) whose expression induces a reversible growth arrest :ref{doi=10.1093/nar/gkt1419}. On the other hand, AbiEi is a transcriptional autorepressor that binds to the promoter of the abiE operon.
Based on these mechanisms, AbiE systems are classified as Type IV Toxin-Antitoxin systems, where the antitoxin and toxin are both proteins that do not directly interact with each other :ref{doi=10.1093/nar/gkt1419}.


## Example of genomic structure

The AbiE is composed of 2 proteins: AbiEii and AbiEi.

Here is an example found in the RefSeq database:

![abie](/abie/AbiE.svg){max-width=750px}

The AbiE system in *Halomonas piezotolerans* (GCF_012427705.1, NZ_CP048602) is composed of 2 proteins AbiEi_3 (WP_231125510.1) AbiEii (WP_231125511.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAB52382.1'>AAB52382.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AAB52383.1'>AAB52383.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

