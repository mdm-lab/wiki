---
title: Nhi
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.03.001
      abstract: |
        The perpetual arms race between bacteria and their viruses (phages) has given rise to diverse immune systems, including restriction-modification and CRISPR-Cas, which sense and degrade phage-derived nucleic acids. These complex systems rely upon production and maintenance of multiple components to achieve antiphage defense. However, the prevalence and effectiveness of minimal, single-component systems that cleave DNA remain unknown. Here, we describe a unique mode of nucleic acid immunity mediated by a single enzyme with nuclease and helicase activities, herein referred to as Nhi (nuclease-helicase immunity). This enzyme provides robust protection against diverse staphylococcal phages and prevents phage DNA accumulation in cells stripped of all other known defenses. Our observations support a model in which Nhi targets and degrades phage-specific replication intermediates. Importantly, Nhi homologs are distributed in diverse bacteria and exhibit functional conservation, highlighting the versatility of such compact weapons as major players in antiphage defense.
    Sensor: Phage protein sensing
    Activator: Direct binding
    Effector: Nucleic acid degrading
    PFAM: PF01443, PF09848, PF13604
contributors:
  - Alba Herrero del Valle
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.03.001
  - doi: 10.1016/j.chom.2022.09.017
---

# Nhi

## Description

The Nhi (nuclease-helicase immunity) system targets and degrades specific phage DNA replication intermediates :ref{doi=10.1016/j.chom.2022.03.001}.  Nayeemul Bari et al. showed that Nhi from *Staphylococcus epidermidis* protects against a diverse panel of staphylococcal phages and Millman et al. showed that a protein Nhi-like (that shares the domain organization with Nhi but not the sequence) from *Bacillus cereus* protects against some Bacillus phages :ref{doi=10.1016/j.chom.2022.03.001,10.1016/j.chom.2022.09.017}. 

## Molecular mechanisms

Nhi contains two domains, a nuclease and a helicase domain that are both needed for the anti-phage activity. The nuclease domain has 3′–5′ exonuclease and plasmid nicking activities while the helicase unwinds dsDNA biderctionally. Nhi specifically recognizes phage single-stranded DNA binding proteins (SSB) that cover the phage genome to target this DNA for degradation thanks to its helicase and nuclease domains :ref{doi=10.1016/j.chom.2022.03.001}.

## Example of genomic structure

The Nhi is composed of 1 protein: Nhi.

Here is an example found in the RefSeq database:

![nhi](/nhi/Nhi.svg){max-width=750px}

The Nhi system in *Staphylococcus sp. MZ9* (GCF_018622975.1, NZ_CP076029) is composed of 1 protein: Nhi (WP_045177897.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[ Nhi-like
Bacillus cereus] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> phi3T & SpBeta & SPR
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_1
    Origin_1[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/AAW53361.1'>AAW53361.1</a>] --> Expressed_1[Staphylococcus epidermidis]
    Expressed_1[Staphylococcus epidermidis] ----> JBug18 & Pike & CNPx
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_1
    Origin_1[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/AAW53361.1'>AAW53361.1</a>] --> Expressed_2[Staphylococcus aureus]
    Expressed_2[Staphylococcus aureus] ----> Lorac
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_2
    Origin_2[Staphylococcus aureus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000632676.1'>WP_000632676.1</a>] --> Expressed_3[Staphylococcus aureus]
    Expressed_3[Staphylococcus aureus] ----> Lorac
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_3
    Origin_3[Staphylococcus aureus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045177897.1'>WP_045177897.1</a>] --> Expressed_4[Staphylococcus aureus]
    Expressed_4[Staphylococcus aureus] ----> Lorac
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_4
    Origin_4[Vibrio vulnificus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_101958732.1'>WP_101958732.1</a>] --> Expressed_5[Staphylococcus aureus]
    Expressed_5[Staphylococcus aureus] ----> Lorac
    subgraph Title1[Reference]
        Millman_2022
        Bari_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_1
        Origin_2
        Origin_3
        Origin_4
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
end
    subgraph Title4[Protects against]
        phi3T
        SpBeta
        SPR
        JBug18
        Pike
        CNPx
        Lorac
        Lorac
        Lorac
        Lorac
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

