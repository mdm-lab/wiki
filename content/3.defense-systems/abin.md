---
title: AbiN
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors: 
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1023/A:1002027321171
  - doi: 10.1016/j.mib.2005.06.006
  - doi: 10.1016/S0378-1097(98)00002-0
---

# AbiN

## Description
AbiN was discovered in *Lactococcus lactis* subsp. cremoris S114 in 1998 :ref{doi=10.1016/S0378-1097(98)00002-0}.

AbiN is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiN is classified as unknown for abortive infection phenotype in :ref{doi=10.1016/j.mib.2023.102312}.

AbiN is a single gene system composed of the protein AbiN.

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown.

## Example of genomic structure

The AbiN is composed of 1 protein: AbiN.

Here is an example found in the RefSeq database:

![abin](/abin/AbiN.svg){max-width=750px}

The AbiN system in *Listeria innocua* (GCF_023078395.1, NZ_CP095723) is composed of 1 protein: AbiN (WP_238929053.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal prophage  
<a href='https://ncbi.nlm.nih.gov/protein/CAA72648.1'>CAA72648.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
