---
title: FS_HsdR_like
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2022.07.014
      abstract: |
        Bacteria encode sophisticated anti-phage systems that are diverse and versatile and display high genetic mobility. How this variability and mobility occurs remains largely unknown. Here, we demonstrate that a widespread family of pathogenicity islands, the phage-inducible chromosomal islands (PICIs), carry an impressive arsenal of defense mechanisms, which can be disseminated intra- and inter-generically by helper phages. These defense systems provide broad immunity, blocking not only phage reproduction, but also plasmid and non-cognate PICI transfer. Our results demonstrate that phages can mobilize PICI-encoded immunity systems to use them against other mobile genetic elements, which compete with the phages for the same bacterial hosts. Therefore, despite the cost, mobilization of PICIs may be beneficial for phages, PICIs, and bacteria in nature. Our results suggest that PICIs are important players controlling horizontal gene transfer and that PICIs and phages establish mutualistic interactions that drive bacterial ecology and evolution.
contributors:
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1016/j.cell.2022.07.014
---

# FS_HsdR_like

## Description

FS_HsdR_like or HsdR_like was discovered in 2022 :ref{doi=10.1016/j.cell.2022.07.014} inside the PICI KpCIC51 in *Klebsiella pneumoniae*.

The HsdR_like system is also effective for blocking conjugation.

The HsdR_like system is composed of tree genes. The first protein encodes a DUF6731 domain and is named after it. A hypothetical protein and the HsdR protein with an N terminal HEPN domain and C terminal Zn finger domain.

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown.

## Example of genomic structure

The FS_HsdR_like is composed of 3 proteins: HdrR, DUF6731 and HP.

Here is an example found in the RefSeq database:

![fs_hsdr_like](/fs_hsdr_like/FS_HsdR_like.svg){max-width=750px}

The FS_HsdR_like system in *Klebsiella pneumoniae* (GCF_009661415.2, NZ_CP062997) is composed of 3 proteins DUF6731 (WP_048981062.1) HP (WP_048981064.1) HdrR (WP_153928365.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/QFH39804.1'>QFH39804.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/QFH39805.1'>QFH39805.1, <a href='https://ncbi.nlm.nih.gov/protein/QFH39806.1'>QFH39806.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & Lambda & HK97 & HK544 & HK544 & HK578 & T7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/QFH39805.1'>QFH39805.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/RXS01985.1'>RXS01985.1</a>] --> Expressed_1[Salmonella enterica]
    Expressed_1[Salmonella enterica] ----> P22 & BTP1 & ES18
    subgraph Title1[Reference]
        Fillol-Salom_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        T5
        Lambda
        HK97
        HK544
        HK544
        HK578
        T7
        P22
        BTP1
        ES18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

