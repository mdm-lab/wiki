---
title: Pif
layout: article
tableColumns:
    article:
      doi: 10.1007/BF00327934
      abstract: |
        We report the molecular cloning of the pif region of the F plasmid and its physical dissection by subcloning and deletion analysis. Examination of the polypeptide products synthesized in maxicells by plasmids carrying defined pif sequences has shown that the region specifies at least two proteins of molecular weights 80,000 and 40,000, the genes for which appear to lie in the same transcriptional unit. In addition, analysis of pif-lacZ fusion plasmids has detected a pif promoter and determined the direction of transcription across the pif region.
    Sensor: Sensing of phage protein
    Activator: Unknown
    Effector: Membrane disrupting (?)
    PFAM: PF07693
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1007/BF00327934
  - doi: 10.1016/j.virol.2004.06.001
  - doi: 10.1128/jb.173.20.6507-6514.1991
  - doi: 10.1038/newbio231037a0
---

# Pif

## Description

The Pif system (Phage Inhibition by F factors) was first described by Morisson & Malamy :ref{doi=10.1038/newbio231037a0}. The Pif region of the F plasmid inhibits the phage T7 through abortive infection :ref{doi=10.1038/newbio231037a0,10.1007/BF00327934}.  

## Molecular mechanisms

PifA was later identified as the effector of the abortive infection :ref{doi=10.1128/jb.173.20.6507-6514.1991,10.1016/j.virol.2004.06.001} through nucleotide depletion by increased membrane permeability (also known as "leakage").

## Example of genomic structure

The Pif is composed of 2 proteins: PifA and PifC.

Here is an example found in the RefSeq database:

![pif](/pif/Pif.svg){max-width=750px}

The Pif system in *Shigella flexneri* (GCF_022353545.1, NZ_CP055168) is composed of 2 proteins PifC (WP_000952217.1) PifA (WP_000698737.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Cheng_2004[<a href='https://doi.org/10.1016/j.virol.2004.06.001'>Cheng et al., 2004</a>] --> Origin_0
    Origin_0[Escherichia coli F-plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/WP_028985935.1'>WP_028985935.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_028985936.1'>WP_028985936.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Cheng_2004
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

