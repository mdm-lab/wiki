---
title: Rst_2TM_1TM_TIR
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13676
contributors:
    - Alba Herrero del Valle

relevantAbstracts:
    - doi: 10.1016/j.chom.2022.02.018
---

# Rst_2TM_1TM_TIR

## Description

The Rst_2TM_1TM_TIR system is carried by the P2-like phage AC1 and protects *Escherichia coli* C against lambda, LF82_P8 and P2 phages. This system is composed of three proteins, Rst_TIR_tm, which contains a TIR (Toll/interleukin-1 receptor) domain, Rst_1TM_TIR, which contains a transmembrane helix (TM) and Rst_2TM_TIR, that contains two TMs :ref{doi=10.1016/j.chom.2022.02.018}. Rousset et al. suggested that the TIR-containing protein could generate a nucleotide messenger that in turn could activate the associated transmembrane proteins.

## Molecular mechanisms

As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The Rst_2TM_1TM_TIR is composed of 3 proteins: Rst_2TM_TIR, Rst_TIR_tm and Rst_1TM_TIR.

Here is an example found in the RefSeq database:

![rst_2tm_1tm_tir](/rst_2tm_1tm_tir/Rst_2TM_1TM_TIR.svg){max-width=750px}

The Rst_2TM_1TM_TIR system in *Escherichia coli* (GCF_004006575.1, NZ_CP034787) is composed of 3 proteins Rst_TIR_tm (WP_023140578.1) Rst_1TM_TIR (WP_001534953.1) Rst_2TM_TIR (WP_023140577.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli C
<a href='https://ncbi.nlm.nih.gov/protein/WP_001534952.1'>WP_001534952.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001534953.1'>WP_001534953.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001534955.1'>WP_001534955.1</a>] --> Expressed_0[Escherichia coli C]
    Expressed_0[Escherichia coli C] ----> Lambda & LF82_P8 & P2 & SIAC10 & SID07 & DC1 & AC1
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        Lambda
        LF82_P8
        P2
        SIAC10
        SID07
        DC1
        AC1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
