---
title: GAPS6
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.03.28.534373
      abstract: |
        Bacteria are found in ongoing conflicts with rivals and predators, which lead to an evolutionary arms race and the development of innate and adaptive immune systems. Although diverse bacterial immunity mechanisms have been recently identified, many remain unknown, and their dissemination within bacterial populations is poorly understood. Here, we describe a widespread genetic element, defined by the Gamma-Mobile-Trio (GMT) proteins, that serves as a mobile bacterial weapons armory. We show that GMT islands have cargo comprising various combinations of secreted antibacterial toxins, anti-phage defense systems, and secreted anti-eukaryotic toxins. This finding led us to identify four new anti-phage defense systems encoded within GMT islands and reveal their active domains and mechanisms of action. We also find the phage protein that triggers the activation of one of these systems. Thus, we can identify novel toxins and defense systems by investigating proteins of unknown function encoded within GMT islands. Our findings imply that the concept of defense islands may be broadened to include other types of bacterial innate immunity mechanisms, such as antibacterial and anti-eukaryotic toxins that appear to stockpile with anti-phage defense systems within GMT weapon islands.
contributors:
    - Jean Cury
relevantAbstract:
    - doi: 10.1101/2023.03.28.534373
---

# GAPS6

## Description

GAPS (GMT-encoded Anti-Phage System) antiphage systems were discovered on newly described Gamma-Mobile-Trio elements. 

GAPS6 is composed of two proteins, [GAPS6a](https://www.ncbi.nlm.nih.gov/protein/WP_248387294.1/) and [GAPS6b](https://www.ncbi.nlm.nih.gov/protein/WP_248387295.1/). These two proteins are encoded together in diverse Gram-negative bacteria.

## Molecular mechanism

GAPS6b is essential for the defense phenotype, however it is not known whether GAPS6b could be sufficient.
GAPS6b is composed of TPR repeats at the N-terminus, possibly allowing ligand binding and a predicted RNAse domain (PINc, PF08745.14) at the C-terminus. PINc domains have been implicated as toxins in bacterial toxin-antitoxin modules :ref{doi=10.1093/protein/gzq081}. The PINc domain is required for the anti-phage defense activity of GAPS6.


## Example of genomic structure

The GAPS6 is composed of 2 proteins: GAPS6a and GAPS6b.

Here is an example found in the RefSeq database:

![gaps6](/gaps6/GAPS6.svg){max-width=750px}

The GAPS6 system in *Escherichia coli* (GCF_013372365.1, NZ_CP054227) is composed of 2 proteins GAPS6b (WP_096985931.1) GAPS6a (WP_000346990.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Mahata_2023[<a href='https://doi.org/10.1101/2023.03.28.534373'>Mahata et al., 2023</a>] --> Origin_0
    Origin_0[Vibrio parahaemolyticus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_248387294.1'>WP_248387294.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_248387295.1'>WP_248387295.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7 & T4 & P1-vir & Lambda-vir
    subgraph Title1[Reference]
        Mahata_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T4
        Lambda-vir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
