---
title: Lamassu-Fam
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Diverse (Nucleic acid degrading (?), Nucleotide modifying (?), Membrane disrupting (?))
    PFAM: PF00753, PF02463, PF05057, PF12532, PF13175, PF13289, PF13476, PF14130  
contributors: 
    - Matthieu Haudiquet
    - Aude Bernheim
relevantAbstracts:
    - doi: 10.1126/science.aar4120
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1093/nar/gkab883
    - doi: 10.1038/s41586-022-04546-y 
    - doi: 10.1101/2022.11.18.517080 
---

# Lamassu-Fam
## Description

The original types of Lamassu systems are Lamassu Type 1 :ref{doi=10.1126/science.aar4120} and Type 2 :ref{doi=10.1016/j.chom.2022.09.017}. They both necessarily comprise two genes *lmuA* and *lmuB*, to which a third gene (*lmuC*) is added in the case of Lamassu Type 2.  

Lamassu has been suggested to be a large family of defense systems, that can be classified into multiple subtypes :ref{doi=10.1016/j.chom.2022.09.017}. 

These systems all encode the *lmuB* gene, and in most cases also comprise *lmuC*. In addition to these two core genes, Lamassu systems of various subtypes encode a third protein, hypothesized to be the Abi effector protein :ref{doi=10.1101/2022.05.11.491447}. This effector  can be proteins encoding endonuclease domains, SIR2-domains, or even hydrolase domains :ref{doi=10.1016/j.chom.2022.09.017}. Systems of the extended Lamassu family can be found in 10% of prokaryotic genomes :ref{doi=10.1016/j.chom.2022.09.017}.

Lamassu were also described as DdmABC in *Vibrio cholerae* :ref{doi=10.1038/s41586-022-04546-y,10.1101/2022.11.18.517080}. They were found to be anti plasmids and thus to eliminate plasmids from the seventh pandemic _Vibrio_ cholerae* :ref{doi=10.1038/s41586-022-04546-y}. The DdmABC system corresponds to a Lamassu-Fam Cap4 nuclease system.

## Molecular mechanism

Lamassu systems function through abortive infection (Abi), but the molecular mechanism remains to be described. It was shown that, in *Vibrio cholerae* palindromic DNA sequences that are predicted to form stem-loop hairpin trigger the system :ref{doi=10.1101/2022.11.18.517080}. 

## Example of genomic structure

The majority of the Lamassu-Fam systems are composed of 3 proteins: LmuA, LmuB and, an accessory LmuC proteins.
LmuA is the effector and encode for different domains depending on the subsystem.

A total of 11 subsystems have been described for the Lamassu-Fam system.

Here are some examples found in the RefSeq database:

![lamassu-amidase](/lamassu-fam/Lamassu-Amidase.svg){max-width=750px}

The Lamassu-Amidase system in *Variovorax sp. PBL-H6* (GCF_901827155.1, NZ_LR594659) is composed of 2 proteins LmuA_effector_Amidase (WP_162575880.1) LmuB_SMC_Amidase (WP_162575881.1) 

![lamassu-cap4_nuclease](/lamassu-fam/Lamassu-Cap4_nuclease.svg){max-width=750px}

The Lamassu-Cap4_nuclease system in *Opitutus terrae* (GCF_000019965.1, NC_010571) is composed of 3 proteins LmuA_effector_Cap4_nuclease_II (WP_012377261.1) LmuC_acc_Lipase (WP_012377262.1) LmuB_SMC_Lipase (WP_012377263.1) 

![lamassu-fmo](/lamassu-fam/Lamassu-FMO.svg){max-width=750px}

The Lamassu-FMO system in *Mesorhizobium sp. B2-1-8* (GCF_006442545.2, NZ_CP083952) is composed of 3 proteins LmuA_effector_FMO (WP_181178269.1) LmuC_acc_FMO (WP_140755520.1) LmuB_SMC_FMO (WP_181178271.1) 

![lamassu-hydrolase_protease](/lamassu-fam/Lamassu-Hydrolase_Protease.svg){max-width=750px}

The Lamassu-Hydrolase_Protease system in *Deefgea piscis* (GCF_013284055.1, NZ_CP054143) is composed of 4 proteins LmuA_effector_Hydrolase (WP_173533336.1) LmuA_effector_Protease (WP_173533337.1) LmuC_acc_hydrolase_protease (WP_173533338.1) LmuB_SMC_Hydrolase_protease (WP_173533339.1) 

![lamassu-hypothetical](/lamassu-fam/Lamassu-Hypothetical.svg){max-width=750px}

The Lamassu-Hypothetical system in *Dehalococcoides mccartyi* (GCF_002007905.1, NZ_CP019866) is composed of 3 proteins LmuA_effector_hypothetical (WP_077975422.1) LmuC_acc_hypothetical (WP_077975423.1) LmuB_SMC_hypothetical (WP_077975424.1) 

![lamassu-lipase](/lamassu-fam/Lamassu-Lipase.svg){max-width=750px}

The Lamassu-Lipase system in *Klebsiella michiganensis* (GCF_022869885.1, NZ_CP094999) is composed of 3 proteins LmuB_SMC_Lipase (WP_016157240.1) LmuC_acc_Lipase (WP_032931759.1) LmuA_effector_Lipase (WP_042945865.1) 

![lamassu-mrr](/lamassu-fam/Lamassu-Mrr.svg){max-width=750px}

The Lamassu-Mrr system in *Agrobacterium tumefaciens* (GCF_022760295.1, NZ_CP092895) is composed of 3 proteins LmuA_effector_Mrr (WP_223215665.1) LmuC_acc_Mrr (WP_220318798.1) LmuB_SMC_Mrr (WP_220318797.1) 

![lamassu-pddexk](/lamassu-fam/Lamassu-PDDEXK.svg){max-width=750px}

The Lamassu-PDDEXK system in *Corynebacterium pseudodiphtheriticum* (GCF_023509295.1, NZ_CP091864) is composed of 3 proteins LmuC_acc_PDDEXK (WP_249620350.1) LmuB_SMC_PDDEXK (WP_249620351.1) LmuA_effector_PDDEXK (WP_249620352.1) 

![lamassu-protease](/lamassu-fam/Lamassu-Protease.svg){max-width=750px}

The Lamassu-Protease system in *Enterococcus hirae* (GCF_014218175.1, NZ_CP042289) is composed of 3 proteins LmuA_effector_Protease (WP_034866244.1) LmuC_acc_hydrolase_protease (WP_181916367.1) LmuB_SMC_Hydrolase_protease (WP_034866249.1) 

![lamassu-sir2](/lamassu-fam/Lamassu-Sir2.svg){max-width=750px}

The Lamassu-Sir2 system in *Enterococcus faecalis* (GCF_023375525.1, NZ_CP091235) is composed of 3 proteins LmuA_effector_Sir2 (WP_231436660.1) LmuC_acc_Sir2 (WP_231436659.1) LmuB_SMC_Sir2 (WP_231436658.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus sp. NIO-1130 
<a href='https://ncbi.nlm.nih.gov/protein/SCC38433.1'>SCC38433.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/SCC38445.1'>SCC38445.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> phi3T & SpBeta & SPR
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/EJR12435.1'>EJR12435.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR12434.1'>EJR12434.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> SpBeta
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[ LmuB+LmuC+Hydrolase Protease
Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999254'>2624999254</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999255'>2624999255</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999257'>2624999257</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999256'>2624999256</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T4
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[ LmuB+LmuC+Hydrolase Protease
Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999254'>2624999254</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999255'>2624999255</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999257'>2624999257</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999256'>2624999256</a>] --> Expressed_3[Bacillus subtilis]
    Expressed_3[Bacillus subtilis] ----> SpBeta & phi105 & Rho14 & SPP1 & phi29
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_3
    Origin_3[ LmuB+LmuC+Mrr endonuclease
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=646870492'>646870492</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=646870494'>646870494</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=646870493'>646870493</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> LambdaVir & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_4
    Origin_4[ LmuB+LmuC+PDDEXK nuclease
Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2729045962'>2729045962</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2729045961'>2729045961</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2729045960'>2729045960</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> LambdaVir
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_5
    Origin_5[ LmuB+LmuC+PDDEXK nuclease
Bacillus sp. UNCCL81 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2595091739'>2595091739</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2595091740'>2595091740</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2595091741'>2595091741</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> LambdaVir
    Payne_2021[<a href='https://doi.org/10.1093/nar/gkab883'>Payne et al., 2021</a>] --> Origin_6
    Origin_6[ LmuA+LmuC+LmuB 
Janthinobacterium agaricidamnosum 
<a href='https://ncbi.nlm.nih.gov/protein/WP_038488270.1'>WP_038488270.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_038488273.1'>WP_038488273.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_156484105.1'>WP_156484105.1</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T1 & T3 & T7 & LambdaVir & PVP-SE1
    Jaskolska_2022[<a href='https://doi.org/10.1038/s41586-022-04546-y'>Jaskólska et al., 2022</a>] --> Origin_7
    Origin_7[ DdmABC
Vibrio cholerae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000466016.1'>WP_000466016.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000917213.1'>WP_000917213.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_000654528.1'>WP_000654528.1</a>] --> Expressed_8[Escherichia coli]
    Expressed_8[Escherichia coli] ----> P1 & Lambda
    subgraph Title1[Reference]
        Doron_2018
        Millman_2022
        Payne_2021
        Jaskolska_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_2
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
        Expressed_8
end
    subgraph Title4[Protects against]
        phi3T
        SpBeta
        SPR
        SpBeta
        T4
        SpBeta
        phi105
        Rho14
        SPP1
        phi29
        LambdaVir
        SECphi27
        LambdaVir
        LambdaVir
        T1
        T3
        T7
        LambdaVir
        PVP-SE1
        P1
        Lambda
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

