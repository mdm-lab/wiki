---
title: Druantia
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00145, PF00270, PF00271, PF04851, PF09369, PF14236
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1126/science.aar4120
---

# Druantia

## Description

The Druantia system was described by Doron et al. 2018 :ref{doi=10.1126/science.aar4120} and includes multiple subtypes, including type I (DruABCDE), type II (DruMFGE), and Type III (DruHE). Druantia is a particularly large system (~12 kb) and was named after the Gallic tree goddess. Type III was further tested by Wang et al. 2023 :ref{doi=10.1128/jvi.00599-23}.

## Molecular mechanisms

As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

A total of 3 subsystems have been described for the Druantia system.

Here is some examples found in the RefSeq database:

![druantia_i](/druantia/Druantia_I.svg){max-width=750px}

The Druantia_I system in *Serratia fonticola* (GCF_005489985.1, NZ_CP040182) is composed of 5 proteins DruA (WP_071682717.1) DruB (WP_071682718.1) DruC (WP_071682719.1) DruD (WP_071682720.1) DruE_1 (WP_071682721.1) 

![druantia_ii](/druantia/Druantia_II.svg){max-width=750px}

The Druantia_II system in *Klebsiella michiganensis* (GCF_018604105.1, NZ_CP075881) is composed of 4 proteins DruM (WP_045338337.1) DruF (WP_152403866.1) DruG (WP_214632470.1) DruE_2 (WP_152403875.1) 

![druantia_iii](/druantia/Druantia_III.svg){max-width=750px}

The Druantia_III system in *Citrobacter werkmanii* (GCF_002025225.1, NZ_CP019986) is composed of 2 proteins DruE_3 (WP_079225583.1) DruH (WP_079225584.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/ERA40829.1'>ERA40829.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ERA40830.1'>ERA40830.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ERA40831.1'>ERA40831.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ERA40832.1'>ERA40832.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ERA40833.1'>ERA40833.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & P1 & Lambda & T3 & T7 & PhiV-1 & Lambdavir & SECphi18 & SECphi27
    subgraph Title1[Reference]
        Gao_2020
        Doron_2018
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T5
        P1
        Lambda
        T3
        T7
        PhiV-1
        Lambdavir
        SECphi18
        SECphi27
        T2
        T4
        T5
        P1
        Lambda
        T3
        T7
        PhiV-1
        Lambdavir
        SECphi18
        SECphi27
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

