---
title: Dsr
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Sensing phage protein
    Activator: Direct
    Effector: Nucleotide modifying
    PFAM: PF13289
relevantAbstracts:
    - doi: 10.1038/s41564-022-01207-8
    - doi: 10.1126/science.aba0372
---

# Dsr
## Example of genomic structure

A total of 2 subsystems have been described for the Dsr system.

Here are some examples found in the RefSeq database:

![dsr_i](/dsr/Dsr_I.svg){max-width=750px}

The Dsr_I system in *Pseudohalocynthiibacter aestuariivivens* (GCF_011040495.1, NZ_CP049037) is composed of 1 protein: Dsr1 (WP_165194530.1) 

![dsr_ii](/dsr/Dsr_II.svg){max-width=750px}

The Dsr_II system in *Deinococcus deserti* (GCF_000020685.1, NC_012526) is composed of 1 protein: Dsr2 (WP_083764220.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ DSR1
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_029488749.1'>WP_029488749.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T3 & T7 & PhiV-1
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_1
    Origin_1[ DSR2
Cronobacter sakazakii 
<a href='https://ncbi.nlm.nih.gov/protein/WP_015387030.1'>WP_015387030.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> Lambda
    Garb_2022[<a href='https://doi.org/10.1038/s41564-022-01207-8'>Garb et al., 2022</a>] --> Origin_2
    Origin_2[ DSR2
Bacillus subtilis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_029317421.1'>WP_029317421.1</a>] --> Expressed_2[Bacillus subtilis ]
    Expressed_2[Bacillus subtilis ] ----> SPR
    Garb_2022[<a href='https://doi.org/10.1038/s41564-022-01207-8'>Garb et al., 2022</a>] --> Origin_3
    Origin_3[ DSR1
Bacillus subtilis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_053564071.1'>WP_053564071.1</a>] --> Expressed_3[Bacillus subtilis]
    Expressed_3[Bacillus subtilis] ----> phi29
    subgraph Title1[Reference]
        Gao_2020
        Garb_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Protects against]
        T3
        T7
        PhiV-1
        Lambda
        SPR
        phi29
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

