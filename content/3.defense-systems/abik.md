---
title: AbiK
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00078
contributors:
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1023/A:1002027321171
  - doi: 10.1016/j.mib.2005.06.006
  - doi: 10.1093/nar/gkac467
  - doi: 10.1128/aem.63.4.1274-1283.1997
---

# AbiK

## Description


The AbiK defense system was discovered in 1997 in *Lactococcus lactis* :ref{doi=10.1128/aem.63.4.1274-1283.1997}. AbiK is a single gene system with a size of around 650 amino acids.

AbiK is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiK is classified as abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

Since it was discovered a similarity in amino acids was found with [AbiA](/defense-systems/abia). 
However, with the discovery of dozens of new systems, it was categorized as one of the UG/Abi defense systems :ref{doi=10.1093/nar/gkac467} along with [DRT](/defense-systems/drt) different subsystems, [AbiA](/defense-systems/abia), [AbiP2](/defense-systems/abip2) and [Rst_RT_Nitralase_TM](/defense-systems/rst_rt-nitrilase-tm).

Those systems are characterized by the presence of a reverse transcriptase domain of the "Unknown Group RT".

## Molecular mechanism
To our knowledge, the molecular mechanism is unknown. Similarly, for the other systems of this family, the molecular mechanism remains unknown.

## Example of genomic structure

The AbiK is composed of 1 protein: AbiK.

Here is an example found in the RefSeq database:

![abik](/abik/AbiK.svg){max-width=750px}

The AbiK system in *Lactobacillus ultunensis* (GCF_016647595.1, NZ_CP059829) is composed of 1 protein: AbiK (WP_007126124.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAB53491.2'>AAB53491.2</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

