---
title: MqsRAC
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.02.25.529695
      abstract: |
        Myriad bacterial anti-phage systems have been described and often the mechanism of programmed cell death is invoked for phage inhibition. However, there is little evidence of ‘suicide’ under physiological conditions for these systems. Instead of death to stop phage propagation, we show here that persister cells, i.e., transiently-tolerant, dormant, antibiotic-insensitive cells, are formed and survive using the Escherichia coli C496_10 tripartite toxin/antitoxin system MqsR/MqsA/MqsC to inhibit T2 phage. Specifically, MqsR/MqsA/MqsC inhibited T2 phage by one million-fold and reduced T2 titers by 500-fold. During T2 phage attack, in the presence of MqsR/MqsA/MqsC, evidence of persistence include the single-cell physiological change of reduced metabolism (via flow cytometry), increased spherical morphology (via transmission electron microscopy), and heterogeneous resuscitation. Critically, we found restriction-modification systems (primarily EcoK McrBC) work in concert with the toxin/antitoxin system to inactivate phage, likely while the cells are in the persister state. Phage attack also induces persistence in Klebsiella and Pseudomonas spp. Hence, phage attack invokes a stress response similar to antibiotics, starvation, and oxidation, which leads to persistence, and this dormant state likely allows restriction/modification systems to clear phage DNA.
         
contributors:
    - Héloïse Georjon
    
relevantAbstracts:
    - doi: 10.1038/s41564-022-01219-4
    - doi: 10.1101/2023.02.25.529695 

---

# MqsRAC
## Description
MqsRAC is a toxin-antitoxin-chaperone (TAC) system shown to have anti-phage activity.

## Molecular mechanisms
As far as we are aware, the molecular mechanism of MqsRAC is unknown.

## Example of genomic structure

The MqsRAC is composed of 2 proteins: mqsC and mqsR.

Here is an example found in the RefSeq database:

![mqsrac](/mqsrac/MqsRAC.svg){max-width=750px}

The MqsRAC system in *Shigella dysenteriae* (GCF_002949935.1, NZ_CP026827) is composed of 2 proteins mqsR (WP_024222007.1) mqsC (WP_157899945.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation
<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli strain C496_10 
<a href='https://ncbi.nlm.nih.gov/protein/WP_157899945.1'>WP_157899945.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_021568457.1'>WP_021568457.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_024222007.1'>WP_024222007.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


