---
title: Thoeris
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Signaling system
    Effector: Nucleotide modifying
    PFAM: PF08937, PF13289, PF18185
contributors: 
  - Marie Guillaume
  - Helena Shomar
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1038/s41586-021-04098-7
  - doi: 10.1126/science.aar4120
  - doi: 10.1038/s41467-020-16703-w
---

# Thoeris

## Description
Thoeris is a two-gene defense system identified in more than 2000 bacterial genomes. It consists of the genes ThsA and thsB. Its anti-phage function was experimentally validated in *Bacillus subtilis* :ref{doi=10.1126/science.aar4120}. In response to phage infection, it produces an isomer of cyclic ADP-ribose, which leads to the depletion of NAD+ and results in abortive infection.

ThsA contains the sirtuin-like domain which binds to nicotinamide adenine dinucleotide (NAD) metabolites. The N112A point mutation neutralizes the Thoeris defense system and abolishes the NAD+ hydrolase activity of thsA :ref{doi=10.1126/science.aar4120}. It lacks a N-terminal transmembrane domain and is predicted to be cytoplasmic. 

ThsB contains a TIR domain :ref{doi=10.1126/science.aar4120} is proposed to participate in the recognition of phage infection, as various thsB proteins sense different phage components.ThsB is found in more than 50% of Thoeris systems in multiple diverse copies :ref{doi=10.1126/science.aar4120}.

## Molecular mechanism

The Thoeris system functions by degrading NAD+ (a cofactor of central metabolism) to stop the growth of phage-infected cells and prevent the transmission of the phage to neighboring bacteria :ref{doi=10.1038/s41467-020-16703-w}.

The protein ThsB, featuring the TIR domain, plays a crucial role in identifying phage invasion. Upon detecting the infection, the TIR domain becomes enzymatically active, initiating the synthesis of a cADPR isomer molecule :ref{doi=10.1038/s41586-021-04098-7}. This molecule acts as a signal, binding to the ThsA effector, likely through its C-terminal SLOG domain, thereby activating its NADase activity :ref{doi=10.1038/s41586-021-04098-7}. Consequently, the NADase effector reduces NAD+ cellular levels, creating an environment unsuitable for phage replication :ref{doi=10.1038/s41586-021-04098-7,10.1038/s41467-020-16703-w}.

## Example of genomic structure

The Thoeris is composed of 2 proteins: ThsA and ThsB.

Here is an example found in the RefSeq database:

![thoeris_i](/thoeris/Thoeris_I.svg){max-width=750px}

The Thoeris_I system in *Sneathia vaginalis* (GCF_000973085.1, NZ_CP011280) is composed of 4 proteins ThsA_new_grand (WP_046328157.1) ThsB_Global (WP_046328158.1) ThsB_Global (WP_052727636.1) ThsB_Global (WP_046328159.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus amyloliquefaciens 
<a href='https://ncbi.nlm.nih.gov/protein/AFJ62118.1'>AFJ62118.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AFJ62119.1'>AFJ62119.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SPO1 & SBSphiJ & SBSphiC
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Ofir_2021[<a href='https://doi.org/10.1038/s41586-021-04098-7'>Ofir et al., 2021</a>] --> Origin_1
    Origin_1[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/EJR09241.1'>EJR09241.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR09240.1'>EJR09240.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> phi29 & SBSphiC & SPO1 & SBSphiJ
    Ofir_2021[<a href='https://doi.org/10.1038/s41586-021-04098-7'>Ofir et al., 2021</a>] --> Origin_2
    Origin_2[Bacillus dafuensis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_057775117.1'>WP_057775117.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_057775115.1'>WP_057775115.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_057775113.1'>WP_057775113.1</a>] --> Expressed_2[Bacillus subtilis]
    Expressed_2[Bacillus subtilis] ----> phi3T & SPBeta & SPR & SBSphi11 & SBSphi13 & phi29 & SBSphiJ & SPO1
    subgraph Title1[Reference]
        Doron_2018
        Ofir_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_1
        Expressed_2
end
    subgraph Title4[Protects against]
        SPO1
        SBSphiJ
        SBSphiC
        phi29
        SBSphiC
        SPO1
        SBSphiJ
        phi29
        SBSphiC
        SPO1
        SBSphiJ
        phi3T
        SPBeta
        SPR
        SBSphi11
        SBSphi13
        phi29
        SBSphiJ
        SPO1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

