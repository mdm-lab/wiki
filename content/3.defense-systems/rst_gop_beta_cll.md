---
title: Rst_gop_beta_cll
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF14350
contributors:
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1016/j.chom.2022.02.018
---

# Rst_gop_beta_cll

## Description

The gop-beta-cll system was shown to have an antiphage activity in 2022 :ref{doi=10.1016/j.chom.2022.02.018} against Escherichia coli phage lambda and P1.

The gop-beta-cll was first described as a nonessential region of the bacteriophage P4 :ref{doi=10.1128/jvi.64.1.24-36.1990}.

This system is composed of 3 genes: gop, beta and cll.

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown.

The production of the gop protein without beta is toxic :ref{doi=10.1128/jvi.64.1.24-36.1990} suggesting a toxin antitoxin mechanism :ref{doi=10.1016/j.chom.2022.02.018}.


## Example of genomic structure

The Rst_gop_beta_cll is composed of 3 proteins: beta, cll and gop.

Here is an example found in the RefSeq database:

![rst_gop_beta_cll](/rst_gop_beta_cll/Rst_gop_beta_cll.svg){max-width=750px}

The Rst_gop_beta_cll system in *Klebsiella grimontii* (GCF_019334465.1, NZ_CP079754) is composed of 3 proteins gop (WP_189095150.1) beta (WP_189095151.1) cll (WP_219192256.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Enterobacteria phage P4 
<a href='https://ncbi.nlm.nih.gov/protein/WP_105493229.1'>WP_105493229.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ENG39111.1'>ENG39111.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/NP_042032.1'>NP_042032.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & P1
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        Lambda
        P1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

