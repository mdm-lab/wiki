---
title: Gao_Ape
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors:
    - Hugo Vaysset
relevantAbstracts:
    - doi: 10.1126/science.aba0372
    - doi: 10.1186/1745-6150-8-15
---

# ApeA

## Description
ApeA is a defense system composed of one protein, initially described as a member of the HEPN superfamily which groups proteins with nucleotide binding activity ([CL0291](https://www.ebi.ac.uk/interpro/set/pfam/CL0291/)). 

ApeA is predicted to act *via* an abortive infection mechanism.

## Molecular mechanism
As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The Gao_Ape is composed of 1 protein: ApeA.

Here is an example found in the RefSeq database:

![gao_ape](/gao_ape/Gao_Ape.svg){max-width=750px}

The Gao_Ape system in *Enterobacter mori* (GCF_018638795.1, NZ_CP071064) is composed of 1 protein: ApeA (WP_215231222.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000706972.1'>WP_000706972.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & T3 & T7 & PhiV-1
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T5
        T3
        T7
        PhiV-1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

