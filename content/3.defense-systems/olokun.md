---
title: Olokun
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01602, PF18742

contributors: 
  - Helena Shomar
  - Marie Guillaume
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.09.017

---

# Olokun

## Description

The system Olokun is composed of 2 genes OloA (Adaptin_N domain) and OloB (nuclease domain) of unknown function.
This system was experimentally validated in _Escherichia coli_ and protects against LambdaVir and SECphi27 infection.

The system is named after a revered deity of the Yoruba religion, associated with the deep sea and depicted as an enormously powerful figure. They are believed to possess immense wealth, are associated with health, fertility and prosperity, and are revered for their ability to induce profound transformation and renewal. They are frequently depicted as a mermaid or mermen, with both masculine and feminine aspects, reflecting the diversity and depth of the ocean.

## Molecular mechanisms
To our knowledge, the molecular mechanism is unknown. Please update.

## Example of genomic structure

The Olokun is composed of 2 proteins: OloA and OloB.

Here is an example found in the RefSeq database:

![olokun](/olokun/Olokun.svg){max-width=750px}

The Olokun system in *Klebsiella quasipneumoniae* (GCF_015286025.1, NZ_CP063902) is composed of 2 proteins OloA (WP_227667429.1) OloB (WP_194418165.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2610314205'>2610314205</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2610314206'>2610314206</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi27
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        LambdaVir
        SECphi27
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

