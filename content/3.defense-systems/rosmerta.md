---
title: RosmerTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01381, PF06114, PF12844, PF13443, PF13560
contributors: 
  - Lucas Paoli
  - Gemma Atkinson
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.09.017
---

# RosmerTA

## Description

The RosmerTA system was found to have antiviral activity and was named after the Gallo-Roman goddess of fertility by Millman et al. 2022 :ref{doi=10.1016/j.chom.2022.09.017}.

## Molecular mechanisms

The RosmerTA system encodes a Zn-peptidase and a toxin of unknown function :ref{doi=10.1016/j.chom.2022.09.017}. One toxin representative has been found to induce membrane depolarization :ref{doi=10.1073/pnas.2305393120}.

## Example of genomic structure

The RosmerTA is composed of 2 proteins: RmrT and RmrA. The toxin can vary in sequence and structure.

Here is an example found in the RefSeq database:

![rosmerta](/rosmerta/RosmerTA.svg){max-width=750px}

The RosmerTA system in *Paucibacter sp. KCTC 42545* (GCF_001477625.1, NZ_CP013692) is composed of 2 proteins RmrT_2662548665 (WP_231741552.1) RmrA_2662548665 (WP_058719324.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2658042940'>2658042940</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2658042941'>2658042941</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & LambdaVir
    Ernits_2023[<a href='https://www.pnas.org/doi/10.1073/pnas.2305393120'>Ernits et al., 2023</a>] --> Origin_1
    Origin_1[Gordonia phage Kita 
<a href='https://www.ncbi.nlm.nih.gov/protein/YP_009301394.1/'>YP_009301394.1</a>, <a href='https://www.ncbi.nlm.nih.gov/protein/YP_009301394.1'>YP_009301394.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> Bas54 & Bas59
    subgraph Title1[Reference]
        Millman_2022
        Ernits_2023
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        P1
        LambdaVir
        Bas54
        Bas59
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
