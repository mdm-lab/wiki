---
title: Stk2
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2016.08.010
      abstract: |
        Organisms from all domains of life are infected by viruses. In eukaryotes, serine/threonine kinases play a central role in antiviral response. Bacteria, however, are not commonly known to use protein phosphorylation as part of their defense against phages. Here we identify Stk2, a staphylococcal serine/threonine kinase that provides efficient immunity against bacteriophages by inducing abortive infection. A phage protein of unknown function activates the Stk2 kinase. This leads to the Stk2-dependent phosphorylation of several proteins involved in translation, global transcription control, cell-cycle control, stress response, DNA topology, DNA repair, and central metabolism. Bacterial host cells die as a consequence of Stk2 activation, thereby preventing propagation of the phage to the rest of the bacterial population. Our work shows that mechanisms of viral defense that rely on protein phosphorylation constitute a conserved antiviral strategy across multiple domains of life.
    Sensor: Sensing of phage protein
    Activator: Direct
    Effector: Other (protein modifying)
    PFAM: PF00069, PF07714
contributors:
    - Héloïse Georjon
    - Hugo Vaysset
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1016/j.chom.2016.08.010
---

# Stk2
## Description

Eukaryotic-like serine/threonine kinases have a variety of functions in prokaryotes. Recently, a single-gene system (Stk2) encoding for a Serine/threonine kinase from *Staphylococcus epidermidis* has been found to have anti-phage activity both in its native host and in a heterologous S.aureus host :ref{doi=10.1016/j.chom.2016.08.010}.

## Molecular mechanism

Stk2 is an Abortive infection system, which triggers cell death upon phage infection, probably through phosphorylation of diverse essential cellular pathways :ref{doi=10.1016/j.chom.2016.08.010}. Stk2 was shown to detect a phage protein named Pack, which was proposed to be involved in phage genome packaging :ref{doi=10.1016/j.chom.2016.08.010}.


## Example of genomic structure

The Stk2 is composed of 1 protein: Stk2.

Here is an example found in the RefSeq database:

![stk2](/stk2/Stk2.svg){max-width=750px}

The Stk2 system in *Bacillus rugosus* (GCF_023238245.1, NZ_CP096590) is composed of 1 protein: Stk2 (WP_248602500.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Depardieu_2016[<a href='https://doi.org/10.1016/j.chom.2016.08.010'>Depardieu et al., 2016</a>] --> Origin_0
    Origin_0[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001001347.1'>WP_001001347.1</a>] --> Expressed_0[Staphylococcus epidermidis]
    Expressed_0[Staphylococcus epidermidis] ----> CNPx
    Depardieu_2016[<a href='https://doi.org/10.1016/j.chom.2016.08.010'>Depardieu et al., 2016</a>] --> Origin_0
    Origin_0[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001001347.1'>WP_001001347.1</a>] --> Expressed_1[Staphylococcus aureus]
    Expressed_1[Staphylococcus aureus] ----> phage80alpha & phage85 & phiNM1 & phiNM2 & phiNM4
    subgraph Title1[Reference]
        Depardieu_2016
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        CNPx
        phage80alpha
        phage85
        phiNM1
        phiNM2
        phiNM4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

