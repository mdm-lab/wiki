---
title: Shango
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00270, PF00271, PF05099, PF10923, PF13208, PF15615
contributors: 
  - Hugo Vaysset
  - Aude Bernheim
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.09.017
  - doi: 10.1093/nar/gkad317
---

# Shango

## Description
Shango is a three genes defense system that was discovered in parallel in two works in both *E.coli* and *P.aeruginosa* and was shown to have antiphage activity against the Lambda-phage in *E.coli* :ref{doi=10.1016/j.chom.2022.09.017} and against diverse podo- and siphoviridae in *P.aeruginosa* :ref{doi=10.1093/nar/gkad317}.

Shango is composed of (i) a TerB-like domain, (ii) a Helicase and (iii) an ATPase. The TerB domain was previously shown to be associated with the periplasmic membrane of bacteria :ref{doi=10.4149/gpb_2011_03_286}. 

## Molecular mechanism

The exact mechanism of action of the Shango defense has not yet been characterized, but it was shown that the TerB domain and the catalytic activity of the ATPase and the Helicase are required to provide antiviral defense. The fact that TerB domains are known to be associated with the periplasmic membrane could indicate that Shango might be involved in membrane surveillance :ref{doi=10.1016/j.chom.2022.09.017}.


## Example of genomic structure

The Shango is composed of 3 proteins: SngA, SngB and SngC.

Here is an example found in the RefSeq database:

![shango](/shango/Shango.svg){max-width=750px}

The Shango system in *Anoxybacillus sp. B2M1* (GCF_001634265.1, NZ_CP015435) is composed of 3 proteins SngA (WP_044741979.1) SngB (WP_044741980.1) SngC (WP_044741981.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2538938181'>2538938181</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2538938182'>2538938182</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2538938183'>2538938183</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi18
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        LambdaVir
        SECphi18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


