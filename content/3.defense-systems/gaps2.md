---
title: GAPS2
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.03.28.534373
      abstract: |
        Bacteria are found in ongoing conflicts with rivals and predators, which lead to an evolutionary arms race and the development of innate and adaptive immune systems. Although diverse bacterial immunity mechanisms have been recently identified, many remain unknown, and their dissemination within bacterial populations is poorly understood. Here, we describe a widespread genetic element, defined by the Gamma-Mobile-Trio (GMT) proteins, that serves as a mobile bacterial weapons armory. We show that GMT islands have cargo comprising various combinations of secreted antibacterial toxins, anti-phage defense systems, and secreted anti-eukaryotic toxins. This finding led us to identify four new anti-phage defense systems encoded within GMT islands and reveal their active domains and mechanisms of action. We also find the phage protein that triggers the activation of one of these systems. Thus, we can identify novel toxins and defense systems by investigating proteins of unknown function encoded within GMT islands. Our findings imply that the concept of "defense islands" may be broadened to include other types of bacterial innate immunity mechanisms, such as antibacterial and anti-eukaryotic toxins that appear to stockpile with anti-phage defense systems within GMT weapon islands.
    PFAM: PF00533, PF01653, PF03119, PF03120, PF12826, PF14520
contributors: 
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1101/2023.03.28.534373
---

# GAPS2
## Description
The GAPS2 system is composed of a single protein. It was found in Gamma-Mobile-Trio (GMT) protein containing genomic island in Vibrio, and cloned into E. coli K-12 :ref{doi=10.1101/2023.03.28.534373}. The name GAPS derives from the "GMT-encoded Anti-Phage System" acronym.

GAPS2 is composed of a single protein with a DNA BRCT domains :ref{doi=10.4161/cc.10.15.16312}.

## Molecular mechanisms

As far as we are aware, the molecular mechanism is unknown.

## Example of genomic structure

The GAPS2 is composed of 1 protein: GAPS2.

Here is an example found in the RefSeq database:

![gaps2](/gaps2/GAPS2.svg){max-width=750px}

The GAPS2 system in *Mannheimia sp. USDA-ARS-USMARC-1261* (GCF_000521605.1, NZ_CP006942) is composed of 1 protein: GAPS2 (WP_025236539.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Mahata_2023[<a href='https://doi.org/10.1101/2023.03.28.534373'>Mahata et al., 2023</a>] --> Origin_0
    Origin_0[Vibrio parahaemolyticus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_174208646.1'>WP_174208646.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1-vir & Lambda-vir
    subgraph Title1[Reference]
        Mahata_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        P1-vir
        Lambda-vir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

