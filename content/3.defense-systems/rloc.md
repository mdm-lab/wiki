---
title: RloC
layout: article
tableColumns:
    article:
      doi: 10.1111/j.1365-2958.2008.06387.x
      abstract: |
        The conserved bacterial protein RloC, a distant homologue of the tRNA(Lys) anticodon nuclease (ACNase) PrrC, is shown here to act as a wobble nucleotide-excising and Zn(++)-responsive tRNase. The more familiar PrrC is silenced by a genetically linked type I DNA restriction-modification (R-M) enzyme, activated by a phage anti-DNA restriction factor and counteracted by phage tRNA repair enzymes. RloC shares PrrC's ABC ATPase motifs and catalytic ACNase triad but features a distinct zinc-hook/coiled-coil insert that renders its ATPase domain similar to Rad50 and related DNA repair proteins. Geobacillus kaustophilus RloC expressed in Escherichia coli exhibited ACNase activity that differed from PrrC's in substrate preference and ability to excise the wobble nucleotide. The latter specificity could impede reversal by phage tRNA repair enzymes and account perhaps for RloC's more frequent occurrence. Mutagenesis and functional assays confirmed RloC's catalytic triad assignment and implicated its zinc hook in regulating the ACNase function. Unlike PrrC, RloC is rarely linked to a type I R-M system but other genomic attributes suggest their possible interaction in trans. As DNA damage alleviates type I DNA restriction, we further propose that these related perturbations prompt RloC to disable translation and thus ward off phage escaping DNA restriction during the recovery from DNA damage.
    Sensor: Monitor the integrity of the bacterial cell machinery
    Activator: Unknown
    Effector: Nucleic acid degrading
    PFAM: PF13166
contributors: 
  - Aude Bernheim
  - Florian Tesson
relevantAbstracts:
  - doi: 10.1111/j.1365-2958.2008.06387.x
---

# RloC

## Description

RloC is a distant homolog of the tRNALys anticodon nuclease (ACNase) [PrrC](/defense-systems/prrc). RloC shares the ABC ATPase motifs and catalytic ACNase triad with PrrC, but it possesses a unique zinc-hook/coiled-coil insert. This distinctive feature results in its ATPase domain bearing similarity to Rad50 and other DNA repair proteins.Contrary to PrrC RloC is rarely linked to a type I R-M system. RloC activity was not directly demonstrated on phages but hypothesized through its similar features to PrrC :ref{doi=10.1111/j.1365-2958.2008.06387.x}.

This system was not experimentally validated but is still considered as a defense system because of its homology with [PrrC](/defense-systems/prrc) and its abundance in [defense islands](/general-concepts/defense-islands). Of the RloC system detected by DefenseFinder around 40% are close to at least one known defense system (20 proteins on each side).

## Molecular Mechanism

RloC was shown to function by tRNA cleavage (wobble nucleotide-excising and Zn++-responsive tRNase) :ref{doi=10.1006/jmbi.1995.0343}. However, such activity was not directly shown on a phage
 Zinc hook seems to regulate the nuclease function.
## Example of genomic structure

The RloC is composed of 1 protein: RloC.

Here is an example found in the RefSeq database:

![rloc](/rloc/RloC.svg){max-width=750px}

The RloC system in *Pseudomonas aeruginosa* (GCF_000981825.1, NZ_CP011317) is composed of 1 protein: RloC (WP_010794466.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

To our knowledge, there is no experimental validation for the RloC system.


