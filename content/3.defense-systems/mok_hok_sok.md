---
title: Mok_Hok_Sok
layout: article
tableColumns:
    article:
      doi: 10.1128/jb.178.7.2044-2050.1996
      abstract: |
        The hok (host killing) and sok (suppressor of killing) genes (hok/sok) efficiently maintain the low-copy-number plasmid R1. To investigate whether the hok/sok locus evolved as a phage-exclusion mechanism, Escherichia coli cells that contain hok/sok on ...
    Sensor: Monitoring of the host cell machinery (?)
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01848
relevantAbstracts:
    - doi: 10.1128/jb.178.7.2044-2050.1996
    - doi: 10.1016/0022-2836(92)90714-u 
contributors:
    - Jean Cury
---

# Mok_Hok_Sok

## Description

The Mok Hok Sok system was discovered as a type 1 toxin-antitoxin system to stabilize plasmid R1 :ref{doi=10.1128/jb.161.1.292-298.1985}. Sok (Suppression of Killing) is an RNA and serves as the antitoxin. Hok (Host killing) is the toxin and Mok (Modulation of killing) is required for the expression of Hok :ref{doi=10.1016/j.mib.2007.03.003,10.1093/nar/gkl750}.
Hok/sok are not related to the T4 head protein Hoc and Soc.
This system defends against T4 phages only, as far as we currently know.

## Molecular mechanism

Upon infection of phage T4, the transcription is halted by the phage, which leads to a decreasing level of the antitoxin Sok within a few minutes. The Hok proteins manage to be processed in their active form and trigger cell death by depolarization of the membrane :ref{doi=10.1006/jmbi.1995.0186} before the later stage of the phage infection (assembly, packaging and lysis).


## Example of genomic structure

The Mok_Hok_Sok is composed of 2 proteins: Mok and Hok.

Here is an example found in the RefSeq database:

![mok_hok_sok](/mok_hok_sok/Mok_Hok_Sok.svg){max-width=750px}

The Mok_Hok_Sok system in *Escherichia fergusonii* (GCF_013819565.1, NZ_CP057221) is composed of 1 protein: Hok (WP_096937776.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Pecota_1996[<a href='https://doi.org/10.1128/jb.178.7.2044-2050.1996'>Pecota and  Wood, 1996</a>] --> Origin_0
    Origin_0[R1 plasmid of Salmonella paratyphi 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001372321.1'>WP_001372321.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4
    subgraph Title1[Reference]
        Pecota_1996
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


