---
title: DISARM
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-017-0051-0
      abstract: |
        The evolutionary pressure imposed by phage predation on bacteria and archaea has resulted in the development of effective anti-phage defence mechanisms, including restriction-modification and CRISPRâ€“Cas systems. Here, we report on a new defence system, DISARM (defence island system associated with restriction-modification), which is widespread in bacteria and archaea. DISARM is composed of five genes, including a DNA methylase and four other genes annotated as a helicase domain, a phospholipase D (PLD) domain, a DUF1998 domain and a gene of unknown function. Engineering the Bacillus paralicheniformis 9945a DISARM system into Bacillus subtilis has rendered the engineered bacteria protected against phages from all three major families of tailed double-stranded DNA phages. Using a series of gene deletions, we show that four of the five genes are essential for DISARM-mediated defence, with the fifth (PLD) being redundant for defence against some of the phages. We further show that DISARM restricts incoming phage DNA and that the B. paralicheniformis DISARM methylase modifies host CCWGG motifs as a marker of self DNA akin to restriction-modification systems. Our results suggest that DISARM is a new type of multi-gene restriction-modification module, expanding the arsenal of defence systems known to be at the disposal of prokaryotes against their viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00145, PF00176, PF00271, PF04851, PF09369, PF13091
contributors:
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1038/s41467-022-30673-1
    - doi: 10.1038/s41564-017-0051-0
---

# DISARM
## Description

DISARM (Defense Island System Associated with Restriction-Modification) is a defense system widespread in prokaryotes, encoded by a 5-gene cassette. DISARM provides broad protection against double-stranded DNA phages, including siphophages, myophages, and podophages :ref{doi=10.1038/s41564-017-0051-0,10.1101/2021.12.28.474362}.

 It was reported to restrict incoming phage DNA and methylate the bacterial host DNA, which could be responsible for self from non-self discrimination :ref{doi=10.1038/s41564-017-0051-0}. This suggests a [Restriction-Modification](/defense-systems/rm)-like (RM-like) mechanism, yet some pieces of experimental evidence hint that DISARM acts through a novel and uncharacterized molecular mechanism :ref{doi=10.1038/s41564-017-0051-0,10.1038/s41467-022-30673-1}.

## Molecular mechanism

DISARM allows phage adsorption but prevents phage replication. DISARM is thought to cause intracellular phage DNA decay :ref{doi=10.1038/s41564-017-0051-0}, but the molecular of this potential DNA degradation remains unknown.

The *drmMII* gene of the DISARM system from *Bacillus paralicheniformis* was shown to methylate bacterial DNA at CCWGG motifs when expressed in Bacillus subtilis, and in the absence of *drmMII,* this DISARM system appears toxic to the cells :ref{doi=10.1038/s41564-017-0051-0}. These observations are consistent with an RM-like mechanism, where nucleic acid degradation targets specific DNA motifs, that are methylated in the bacterial chromosome to prevent auto-immunity. 

Yet this system was also shown to protect against phages whose genomes are exempt from CCWGG motifs :ref{doi=10.1038/s41564-017-0051-0}. Moreover, a recent study reports that the absence of methylases (DrmMI or DrmMII) of the DISARM system from a *Serratia sp.* does not result in autoimmunity :ref{doi=10.1101/2021.12.28.474362}. Both these results suggest additional phage DNA recognition mechanisms. 

Hints of these additional mechanisms can be found in recent structural studies, which show that DrmA and DrmB form a complex that can bind single-stranded DNA :ref{doi=10.1038/s41467-022-30673-1}. Moreover, the DrmAB complex seems to exhibit strong ATPase activity in the presence of unmethylated DNA, and reduced ATPase activity in the presence of a methylated DNA substrate :ref{doi=10.1038/s41467-022-30673-1}. Finally, binding of unmethylated single-stranded DNA appears to mediate major conformational change of the complex, which was hypothesized to be responsible for downstream DISARM activation :ref{doi=10.1038/s41467-022-30673-1}.


## Example of genomic structure

DISARM is encoded by three core genes: *drmA* (encoding for a protein containing a putative helicase domain)*,* *drmB* (encoding for a protein containing a putative helicase-associated domain), and *drmC* (encoding for a protein containing a phospholipase D/nuclease domain) :ref{doi=10.1038/s41564-017-0051-0}

These three core genes are accompanied by a methyltransferase, which can be either an adenine methylase (*drmMI*) for class 1 DISARM systems or a cytosine methylase (*drmMII*) for DISARM class 2. Both classes also encode an additional gene (*drmD* for class 1, and *drmE* for class 2). 

A total of 2 subsystems have been described for the DISARM system.

Here are some examples found in the RefSeq database:

![disarm_1](/disarm/DISARM_1.svg){max-width=750px}

The DISARM_1 system in *Burkholderia pseudomallei* (GCF_001887555.1, NZ_CP016910) is composed of 5 proteins drmD (WP_043276582.1) drmMI (WP_071897987.1) drmA (WP_024430133.1) drmB (WP_043276578.1) drmC (WP_229202442.1) 

![disarm_2](/disarm/DISARM_2.svg){max-width=750px}

The DISARM_2 system in *Bacillus halotolerans* (GCF_018417515.1, NZ_CP070976) is composed of 5 proteins drmMII (WP_213418185.1) drmC (WP_213418186.1) drmB (WP_213418187.1) drmA (WP_213418188.1) drmE (WP_213418189.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Ofir_2017[<a href='https://doi.org/10.1038/s41564-017-0051-0'>Ofir et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus paralicheniformis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_020450479.1'>WP_020450479.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_020450481.1'>WP_020450481.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_020450482.1'>WP_020450482.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_025810358.1'>WP_025810358.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_020450478.1'>WP_020450478.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SPO1 & phi3T & SpBeta & SPR & phi105 & rho14 & SPP1 & phi29 & Nf
    Aparicio-Maldonado_2021[<a href='https://doi.org/10.1101/2021.12.28.474362'>Aparicio-Maldonado et al., 2021</a>] --> Origin_1
    Origin_1[Serratia sp. SCBI 
<a href='https://ncbi.nlm.nih.gov/protein/WP_071883521.1'>WP_071883521.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_255352645.1'>WP_255352645.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_304413583.1'>WP_304413583.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_198036332.1'>WP_198036332.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_042783584.1'>WP_042783584.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_071883524.1'>WP_071883524.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_071883525.1'>WP_071883525.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T1 & Nami & T7 & M13
    subgraph Title1[Reference]
        Doron_2018
        Ofir_2017
        Aparicio-Maldonado_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        SPO1
        phi3T
        SpBeta
        SPR
        phi105
        rho14
        SPP1
        phi29
        Nf
        SPO1
        phi3T
        SpBeta
        SPR
        phi105
        rho14
        SPP1
        phi29
        Nf
        T1
        Nami
        T7
        M13
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


