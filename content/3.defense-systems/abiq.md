---
title: AbiQ
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13958
contributors: 
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1128/AEM.64.12.4748-4756.1998
    - doi: 10.1111/mmi.12129
---

# AbiQ

## Description

AbiQ was discovered in 1998 on *Lactococcus lactis* plasmid :ref{doi=10.1128/AEM.64.12.4748-4756.1998}.

AbiQ is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiQ is classified as abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

AbiQ is composed of a single protein AbiQ and an RNA antitoxin (antiQ) :ref{doi=10.1111/mmi.12129}.

## Molecular mechanism

AbiQ act as an anti-toxin type III. AbiQ is an RNAase that will bind its antitoxin antiQ :ref{doi=10.1111/mmi.12129}.

The AbiQ is constitutively expressed and bind to its antiQ RNA resulting in an inactivated AbiQ.
To get activated, AbiQ needs the concentration of antiQ to decrease.
However, during phage infection, the expression of the antiQ is constant and the authors do not know how the AbiQ is activated :ref{doi=10.1111/mmi.12129}.

## Example of genomic structure

The AbiQ is composed of 1 protein: AbiQ.

Here is an example found in the RefSeq database:

![abiq](/abiq/AbiQ.svg){max-width=750px}

The AbiQ system in *Planococcus faecalis* (GCF_002009235.1, NZ_CP019401) is composed of 1 protein: AbiQ (WP_078080483.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAC98713.1'>AAC98713.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

