---
title: RnlAB
layout: article
tableColumns:
    article:
      doi: 10.1534/genetics.110.121798
      abstract: |
        RNase LS was originally identified as a potential antagonist of bacteriophage T4 infection. When T4 dmd is defective, RNase LS activity rapidly increases after T4 infection and cleaves T4 mRNAs to antagonize T4 reproduction. Here we show that rnlA, a structural gene of RNase LS, encodes a novel toxin, and that rnlB (formally yfjO), located immediately downstream of rnlA, encodes an antitoxin against RnlA. Ectopic expression of RnlA caused inhibition of cell growth and rapid degradation of mRNAs in ?rnlAB cells. On the other hand, RnlB neutralized these RnlA effects. Furthermore, overexpression of RnlB in wild-type cells could completely suppress the growth defect of a T4 dmd mutant, that is, excess RnlB inhibited RNase LS activity. Pull-down analysis showed a specific interaction between RnlA and RnlB. Compared to RnlA, RnlB was extremely unstable, being degraded by ClpXP and Lon proteases, and this instability may increase RNase LS activity after T4 infection. All of these results suggested that rnlA-rnlB define a new toxin-antitoxin (TA) system.
    Sensor: Monitor the integrity of the bacterial cell machinery
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF15933, PF15935, PF18869, PF19034
contributors: 
  - Lucas Paoli
relevantAbstracts:
  - doi: 10.1534/genetics.110.121798
---

# RnlAB

## Description

RnlAB is a type II toxin-antitoxin system, in which RnlA is the toxin and RnlB the antitoxin :ref{doi=10.1534/genetics.110.121798}.

## Molecular mechanisms

The RnlA toxin has an RNase activity. RnlB (formerly yfjO) is the antitoxin and suppresses the RNase LS activity :ref{doi=10.1534/genetics.110.121798}.

## Example of genomic structure

The RnlAB is composed of 2 proteins: RnlA and RnlB.

Here is an example found in the RefSeq database:

![rnlab](/rnlab/RnlAB.svg){max-width=750px}

The RnlAB system in *Bacteroides caecimuris* (GCF_001688725.2, NZ_CP015401) is composed of 2 proteins RnlA (WP_004327332.1) RnlB (WP_004327330.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Koga_2011[<a href='https://doi.org/10.1534/genetics.110.121798'>Koga et al., 2011</a>] --> Origin_0
    Origin_0[Escherichia coli] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4
    subgraph Title1[Reference]
        Koga_2011
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
