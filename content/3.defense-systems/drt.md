---
title: DRT
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00078
contributors: 
  - Helena Shomar
  - Marie Guillaume
relevantAbstracts:
  - doi: 10.1093/nar/gkac467
  - doi: 10.1126/science.aba0372
---

# DRT

## Description
DRT stands for Defense-associated Reverse Transcriptases.
DRTs are a widespread and highly diverse family of defense systems, characterized by reverse transcriptase (RTs) components with antiphage properties. These RTs belong to the so-called group of unknown RTs (UG) and are closely related to the Abortive Infection system (Abi) RTs.

DRT systems were experimentally validated in _Escherichia coli_ and demonstrated to be effective against an array of diverse phages.
So far, DRTs have been classified in 9 different types. Essential components of each DRT type are:
- DRT Type 1:   UG1  (RT-nitrilaseTM domains)
- DRT Type 2:   UG2  (RT domain)
- DRT Type 3:   UG3  (RT domain), UG8 (RT domain) and ncRNA
- DRT Type 4:   UG15 (RT-alphaRep domains)
- DRT Type 5:   UG16 (RT domain)
- DRT Type 6:   UG12 (RT-alphaRep domains)
- DRT Type 7:   UG10 (PrimS-RT-alphaRep domains)
- DRT Type 8:   UG7  (RT-alphaRep-PDDExK domains)
- DRT Type 9:   UG28 (RT-alphaRep domains) and ncRNA


## Molecular mechanism
To our knowledge, the molecular mechanism is unknown. 
Similarly, for the other systems of this family, the molecular mechanism remains unknown. 

## Example of genomic structure

A total of 9 subsystems have been described for the DRT system.

Here are some examples found in the RefSeq database:

![drt6](/drt/DRT6.svg){max-width=750px}

The DRT6 system in *Polaribacter sp. L3A8* (GCF_009796785.1, NZ_CP047026) is composed of 1 protein: DRT6 (WP_158838312.1) 

![drt7](/drt/DRT7.svg){max-width=750px}

The DRT7 system in *Bacillus thuringiensis* (GCF_020809145.1, NZ_CP083117) is composed of 1 protein: DRT7 (WP_016090556.1) 

![drt8](/drt/DRT8.svg){max-width=750px}

The DRT8 system in *Vibrio natriegens* (GCF_001680065.1, NZ_CP016349) is composed of 2 proteins DRT8 (WP_065299195.1) DRT8b (WP_155759966.1) 

![drt9](/drt/DRT9.svg){max-width=750px}

The DRT9 system in *Escherichia coli* (GCF_004801575.1, NZ_CP039298) is composed of 1 protein: DRT9 (WP_211092143.1) 

![drt_1](/drt/DRT_1.svg){max-width=750px}

The DRT_1 system in *Vibrio parahaemolyticus* (GCF_013393865.1, NZ_CP040101) is composed of 2 proteins drt1a (WP_179000524.1) drt1b (WP_179000525.1) 

![drt_2](/drt/DRT_2.svg){max-width=750px}

The DRT_2 system in *Chryseobacterium indoltheticum* (GCF_003815915.1, NZ_CP033929) is composed of 1 protein: drt2 (WP_228421416.1) 

![drt_3](/drt/DRT_3.svg){max-width=750px}

The DRT_3 system in *Enterobacter sp. JBIWA008* (GCF_019968765.1, NZ_CP074149) is composed of 2 proteins drt3a (WP_223562544.1) drt3b (WP_223562545.1) 

![drt_4](/drt/DRT_4.svg){max-width=750px}

The DRT_4 system in *Vibrio diabolicus* (GCF_002953355.1, NZ_CP014133) is composed of 1 protein: drt4 (WP_104973818.1) 

![drt_5](/drt/DRT_5.svg){max-width=750px}

The DRT_5 system in *Klebsiella pasteurii* (GCF_018139045.1, NZ_CP073236) is composed of 1 protein: drt5 (WP_211811523.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ RT-nitrilase UG1 Type 1
Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_115196278.1'>WP_115196278.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_040189938.1'>WP_040189938.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_1
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_1
    Origin_1[ RT UG2 Type 2
Salmonella enterica 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012737279.1'>WP_012737279.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T5 & T2
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_2
    Origin_2[ RT UG3 + RT UG8 type 3
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_087902017.1'>WP_087902017.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_062891751.1'>WP_062891751.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T5 & Lambda
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_3
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_3
    Origin_3[ RT UG15 Type 4
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/GCK53192.1'>GCK53192.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T5 & T3 & T7 & Phi-V1 & ZL-19
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_4
    Origin_4[ RT UG16 Type 5
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_001524904.1'>WP_001524904.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> T2
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_5
    Origin_5[ RT UG10 Type 7
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_096936834.1'>WP_096936834.1</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> T2 & T5 & ZL-19
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_6
    Origin_6[ RT UG7 Type 8
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_123894217.1'>WP_123894217.1</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> T2 & T5
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_7
    Origin_7[ RT UG28 Type 9
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/EAO1508900.1'>EAO1508900.1</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T2 & T5 & ZL-19
    subgraph Title1[Reference]
        Gao_2020
        Mestre_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_1
        Origin_2
        Origin_3
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
end
    subgraph Title4[Protects against]
        T2
        T4
        T5
        T5
        T2
        T5
        T2
        T2
        T5
        Lambda
        T5
        T3
        T7
        Phi-V1
        ZL-19
        T5
        T3
        T7
        Phi-V1
        ZL-19
        T2
        T2
        T5
        ZL-19
        T2
        T5
        T2
        T5
        ZL-19
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



