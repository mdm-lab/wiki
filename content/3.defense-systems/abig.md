---
title: AbiG
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF10899, PF16873
contributors:
  - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1128/aem.62.9.3075-3082.1996
---

# AbiG

## Description

AbiG was discovered in 1996 on the plasmid pCI750 of *Lactococcus lactis* :ref{doi=10.1128/aem.62.9.3075-3082.1996}.

AbiG is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiG is classified as abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

AbiG is composed of two genes AbiGi and AbiGii. 

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown.


## Example of genomic structure

The AbiG is composed of 2 proteins: AbiGi and AbiGii.

Here is an example found in the RefSeq database:

![abig](/abig/AbiG.svg){max-width=750px}

The AbiG system in *Staphylococcus simulans* (GCF_900474685.1, NZ_LS483313) is composed of 2 proteins AbiGi (WP_103364194.1) AbiGii (WP_070462993.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAB38312.1'>AAB38312.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AAB38313.1'>AAB38313.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

