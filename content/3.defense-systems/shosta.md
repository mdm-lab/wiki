---
title: ShosTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF02481
contributors:
    - Florian Tesson
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.09.017
  - doi: 10.1016/j.chom.2022.02.018
  - doi: 10.1101/gr.133850.111
---

# ShosTA

## Description

ShosTA system was first described as a Toxin/Antitoxin system in 2012 :ref{doi=10.1101/gr.133850.111} without demonstration of antiphase activity. In 2022, a paper described the same system as "DprA + PRTase" inside P2-like prophages and proved its antiphage activity. Finally, the antiphage activity was also proved in another study with the original name ShosTA :ref{doi=10.1016/j.chom.2022.09.017}.

This system is composed of two proteins: ShosT and ShosA encoding for Hydrolase/PRTase and DprA (nucleotide binding) respectively.

## Molecular mechanism
The ShosTA system is a toxin (ShosT) antitoxin (ShosA) system. The domains of ShosT (Hydrolase and PRTase) allow us to hypothesize toxicity linked to host protein degradation.

## Example of genomic structure

The ShosTA is composed of 2 proteins: ShosA and ShosT.

Here is an example found in the RefSeq database:

![shosta](/shosta/ShosTA.svg){max-width=750px}

The ShosTA system in *Enterobacter cloacae* (GCF_009707405.1, NZ_CP046116) is composed of 2 proteins ShosT (WP_129253192.1) ShosA (WP_129253194.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2564403099'>2564403099</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2564403100'>2564403100</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi4 & SECphi6 & SECphi18 & T7
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_1
    Origin_1[Escherichia coli P2 loci] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> Lambda & T7
    subgraph Title1[Reference]
        Millman_2022
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        LambdaVir
        SECphi4
        SECphi6
        SECphi18
        T7
        Lambda
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



