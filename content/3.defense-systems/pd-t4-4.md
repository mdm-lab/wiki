---
title: PD-T4-4
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13175, PF13304
contributors:
    - Nathalie Bechon
relevantAbstracts:
    - doi: 10.1038/s41564-022-01219-4
---

# PD-T4-4
## Description
PD-T4-4 is a defense system composed of two proteins, a P-loop NTPase and a nuclease, that is most likely protecting the bacterial population through abortive infection. It was identified from an ICE in an *E. coli* genome.

## Molecular mechanisms
PD-T4-4 molecular mechanism is currently unknown, although it is most likely an abortive infection system.

## Example of genomic structure

The PD-T4-4 is composed of 2 proteins: PD-T4-4_A and PD-T4-4_B.

Here is an example found in the RefSeq database:

![pd-t4-4](/pd-t4-4/PD-T4-4.svg){max-width=750px}

The PD-T4-4 system in *Thiothrix subterranea* (GCF_016772315.1, NZ_CP053482) is composed of 2 proteins PD-T4-4_A (WP_202716711.1) PD-T4-4_B (WP_202716712.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCO57999.1'>RCO57999.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/RCO57988.1'>RCO57988.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & SECphi17
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T2
        T4
        T6
        SECphi17
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


