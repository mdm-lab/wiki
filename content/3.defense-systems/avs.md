---
title: Avs
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Sensing of phage protein
    Activator: Direct binding
    Effector: Diverse effectors (Nucleic acid degrading, putative Nucleotide modifying, putative Membrane disrupting)

    PFAM: PF00753, PF13289, PF13365
contributors: 
    - Alex Linyi Gao
    - Nathalie Bechon
relevantAbstracts:
    - doi: 10.1126/science.aba0372
    - doi: 10.1126/science.abm4096
---

# Avs

## Description 
Avs proteins are members of the STAND (signal transduction ATPase with numerous domains) superfamily of P-loop NTPases, which play essential roles in innate immunity and programmed cell death in eukaryotes :ref{doi=10.1038/sj.cdd.4400991,10.1016/j.jmb.2004.08.023}. STAND ATPases include nucleotide-binding oligomerization domain-like receptors (NLRs) in animal inflammasomes and plant resistosomes. Bacterial Avs share a common tripartite domain architecture with eukaryotic NLR, typically consisting of a central ATPase, a C-terminal sensor with superstructure-forming repeats, and an N-terminal effector involved in inflammation or cell death. They are very similar to other bacterial defense systems: [bNACHT](/defense-systems/nlr), [CARD_NLR](/defense-systems/card_nlr) , [Rst_TIR-NLR](/defense-systems/rst_tir-nlr). 

## Molecular mechanism

::info
Two classifications of Avs systems were proposed. The first one :ref{doi=10.1126/science.aba0372} distinguishes 5 types of Avs based on their effector domain. This is the classification used in Defense Finder right now, and in the following wiki entry unless stated otherwise. Considering the modular aspect of the effector domain, a new classification based on the homology of the NTPase and C terminal sensor domain, and not on the effector domain, has been proposed more recently :ref{doi=10.1126/science.abm4096} and is the one used in this description of the mechanism. This second classification defines 4 different types, that do not represent the whole diversity of Avs proteins but only the 4 characterized types.
::

Similar to their eukaryotic counterparts, Avs proteins utilize their C-terminal sensor domains to bind to pathogen-associated molecular patterns (PAMPs). Specifically, Avs1, Avs2, and Avs3 bind to monomers of the large terminase subunit of tailed phages, which account for approximately 96% of all phages, whereas Avs4 binds to monomers of the portal protein. The helical sensor domains of Avs1-4 can recognize diverse variants of terminase or portal proteins, with less than 5% sequence identity in some cases. Binding is mediated by shape complementarity across an extended interface, indicating fold recognition. Additionally, Avs3 directly recognizes active site residues and the ATP ligand of the large terminase.

Upon binding to their cognate phage protein, Avs1-4 assemble into tetramers that activate their N-terminal effector domains, which are often non-specific dsDNA endonucleases. The effector domains are thought to induce abortive infection to disrupt the production of progeny phage.

Avs systems sometimes include additional essential small genes on top of the canonical Avs gene, but the way they contribute to defense is not currently described.


## Example of genomic structure

The Avs system has been described in a total of 5 subsystems (in the old classification).

Here are some examples found in the RefSeq database:

![avs_i](/avs/Avs_I.svg){max-width=750px}

The Avs_I system in *Priestia aryabhattai* (GCF_022811825.1, NZ_CP064098) is composed of 3 proteins Avs1C (WP_243495694.1) Avs1B (WP_243495695.1) Avs1A (WP_243495696.1) 

![avs_ii](/avs/Avs_II.svg){max-width=750px}

The Avs_II system in *Haloferax volcanii* (GCF_000025685.1, NC_013967) is composed of 1 protein: Avs2A (WP_013035348.1) 

![avs_iii](/avs/Avs_III.svg){max-width=750px}

The Avs_III system in *Chryseobacterium indologenes* (GCF_002208925.2, NZ_CP022058) is composed of 2 proteins Avs3B (WP_002978689.1) Avs3A (WP_088583894.1) 

![avs_iv](/avs/Avs_IV.svg){max-width=750px}

The Avs_IV system in *Dysosmobacter welbionis* (GCF_005121165.3, NZ_CP034413) is composed of 1 protein: Avs4A (WP_136890703.1) 

![avs_v](/avs/Avs_V.svg){max-width=750px}

The Avs_V system in *Klebsiella variicola* (GCF_015287155.1, NZ_CP063912) is composed of 1 protein: Avs5A (WP_131026359.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation


<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[ SIR2-STAND
Escherichia fergusonii's PICI EfCIRHB19-C05 
<a href='https://ncbi.nlm.nih.gov/protein/QML19490.1'>QML19490.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4 & Lambda & HK97 & HK544 & HK578 & T7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[ SIR2-STAND
Escherichia fergusonii's PICI EfCIRHB19-C05 
<a href='https://ncbi.nlm.nih.gov/protein/QML19490.1'>QML19490.1</a>] --> Expressed_1[Salmonella enterica]
    Expressed_1 ----> P22 & BTP1 & ES18 & det7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[ SIR2-STAND
Escherichia fergusonii's PICI EfCIRHB19-C05 
<a href='https://ncbi.nlm.nih.gov/protein/QML19490.1'>QML19490.1</a>] --> Expressed_2[Klebsiella pneumoniae]
    Expressed_2[Klebsiella pneumoniae] ----> Pokey
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_1
    Origin_1[ Metallo beta-lactamase + protease + STAND type 1
Erwinia piriflorinigrans  
<a href='https://ncbi.nlm.nih.gov/protein/WP_023654314.1'>WP_023654314.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_084007836.1'>WP_084007836.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_023654316.1'>WP_023654316.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> P1
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_2
    Origin_2[ STAND type 2 
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_063118745.1'>WP_063118745.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> T4 & P1
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_2
    Origin_2[ EcAvs2
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_063118745.1'>WP_063118745.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> T7 & PhiV-1 & P1 & T4 & T5 & ZL-19
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_3
    Origin_3[ DUF4297-STAND type 3
Salmonella enterica 
<a href='https://ncbi.nlm.nih.gov/protein/WP_126523998.1'>WP_126523998.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_126523997.1'>WP_126523997.1</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> T2 & T3 & T7 & PhiV-1
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_4
    Origin_4[ Mrr-STAND type 4 
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_044068927.1'>WP_044068927.1</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> T3 & T7 & PhiV-1
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_4
    Origin_4[ EcAvs4
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_044068927.1'>WP_044068927.1</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> T7 & PhiV-1 & ZL-19
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_5
    Origin_5[ SIR2-STAND type 5
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001515187.1'>WP_001515187.1</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T2
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_6
    Origin_6[ SeAvs1
Salmonella enterica 
<a href='https://ncbi.nlm.nih.gov/protein/ECC9552481.1'>ECC9552481.1</a>] --> Expressed_8[Escherichia coli]
    Expressed_8[Escherichia coli] ----> P1 & ZL-19
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_7
    Origin_7[ EcAvs1
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_060615938.1'>WP_060615938.1</a>] --> Expressed_9[Escherichia coli]
    Expressed_9[Escherichia coli] ----> ZL-19
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_8
    Origin_8[ EpAvs1
Erwinia piriflorinigrans 
<a href='https://ncbi.nlm.nih.gov/protein/WP_048696970.1'>WP_048696970.1</a>] --> Expressed_10[Escherichia coli]
    Expressed_10[Escherichia coli] ----> P1 & Lambda & ZL-19
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_9
    Origin_9[ SeAvs3
Salmonella enterica 
<a href='https://ncbi.nlm.nih.gov/protein/WP_126523998.1'>WP_126523998.1</a>] --> Expressed_11[Escherichia coli]
    Expressed_11[Escherichia coli] ----> T7 & PhiV-1 & ZL-19
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_10
    Origin_10[ KvAvs3
Klebsiella variicola 
<a href='https://ncbi.nlm.nih.gov/protein/WP_139964370.1'>WP_139964370.1</a>] --> Expressed_12[Escherichia coli]
    Expressed_12[Escherichia coli] ----> P1 & ZL-19
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_11
    Origin_11[ Ec2Avs2
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001017806.1'>WP_001017806.1</a>] --> Expressed_13[Escherichia coli]
    Expressed_13[Escherichia coli] ----> P1
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_12
    Origin_12[ Ec2Avs4
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/EEW5978513.1'>EEW5978513.1</a>] --> Expressed_14[Escherichia coli]
    Expressed_14[Escherichia coli] ----> T7 & PhiV-1 & ZL-19
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_13
    Origin_13[ KpAvs4
Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_087775949.1'>WP_087775949.1</a>] --> Expressed_15[Escherichia coli]
    Expressed_15[Escherichia coli] ----> ZL-19
    Gao_2022[<a href='https://doi.org/10.1126/science.abm4096'>Gao et al., 2022</a>] --> Origin_14
    Origin_14[ CcAvs4
Corallococcus coralloides 
<a href='https://ncbi.nlm.nih.gov/protein/WP_083892326.1'>WP_083892326.1</a>] --> Expressed_16[Escherichia coli]
    Expressed_16[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Fillol-Salom_2022
        Gao_2020
        Gao_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_0
        Origin_1
        Origin_2
        Origin_2
        Origin_3
        Origin_4
        Origin_4
        Origin_5
        Origin_6
        Origin_7
        Origin_8
        Origin_9
        Origin_10
        Origin_11
        Origin_12
        Origin_13
        Origin_14
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_6
        Expressed_7
        Expressed_8
        Expressed_9
        Expressed_10
        Expressed_11
        Expressed_12
        Expressed_13
        Expressed_14
        Expressed_15
        Expressed_16
end
    subgraph Title4[Protects against]
        T4
        Lambda
        HK97
        HK544
        HK578
        T7
        P22
        BTP1
        ES18
        det7
        Pokey
        P1
        T4
        P1
        T7
        PhiV-1
        P1
        T4
        T5
        ZL-19
        T2
        T3
        T7
        PhiV-1
        T3
        T7
        PhiV-1
        T7
        PhiV-1
        ZL-19
        T2
        P1
        ZL-19
        ZL-19
        P1
        Lambda
        ZL-19
        T7
        PhiV-1
        ZL-19
        P1
        ZL-19
        P1
        T7
        PhiV-1
        ZL-19
        ZL-19
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


