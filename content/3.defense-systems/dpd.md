---
title: Dpd
layout: article
tableColumns:
    article:
      doi: 10.1073/pnas.1518570113
      abstract: |
        The discovery of ?20-kb gene clusters containing a family of paralogs of tRNA guanosine transglycosylase genes, called tgtA5, alongside 7-cyano-7-deazaguanine (preQ0) synthesis and DNA metabolism genes, led to the hypothesis that 7-deazaguanine derivatives are inserted in DNA. This was established by detecting 2’-deoxy-preQ0 and 2’-deoxy-7-amido-7-deazaguanosine in enzymatic hydrolysates of DNA extracted from the pathogenic, Gram-negative bacteria Salmonella enterica serovar Montevideo. These modifications were absent in the closely related S. enterica serovar Typhimurium LT2 and from a mutant of S. Montevideo, each lacking the gene cluster. This led us to rename the genes of the S. Montevideo cluster as dpdA-K for 7-deazapurine in DNA. Similar gene clusters were analyzed in ?150 phylogenetically diverse bacteria, and the modifications were detected in DNA from other organisms containing these clusters, including Kineococcus radiotolerans, Comamonas testosteroni, and Sphingopyxis alaskensis. Comparative genomic analysis shows that, in Enterobacteriaceae, the cluster is a genomic island integrated at the leuX locus, and the phylogenetic analysis of the TgtA5 family is consistent with widespread horizontal gene transfer. Comparison of transformation efficiencies of modified or unmodified plasmids into isogenic S. Montevideo strains containing or lacking the cluster strongly suggests a restriction-modification role for the cluster in Enterobacteriaceae. Another preQ0 derivative, 2’-deoxy-7-formamidino-7-deazaguanosine, was found in the Escherichia coli bacteriophage 9g, as predicted from the presence of homologs of genes involved in the synthesis of the archaeosine tRNA modification. These results illustrate a deep and unexpected evolutionary connection between DNA and tRNA metabolism.
    PFAM: PF00176, PF00270, PF00271, PF01227, PF01242, PF04055, PF04851, PF06508, PF13091, PF13353, PF13394, PF14072
relevantAbstracts:
    - doi: 10.1073/pnas.1518570113
---

# Dpd
## Example of genomic structure

The Dpd is composed of 15 proteins: DpdA, DpdB, DpdC, DpdD, DpdE, DpdF, DpdG, DpdH, DpdI, DpdJ, DpdK, QueC, QueD, QueE and FolE.

Here is an example found in the RefSeq database:

![dpd](/dpd/Dpd.svg){max-width=750px}

The Dpd system in *Colwellia sp. PAMC 20917* (GCF_001767295.1, NZ_CP014944) is composed of 15 proteins FolE (WP_070374315.1) QueD (WP_070374318.1) DpdC (WP_070374319.1) DpdA (WP_070374320.1) DpdB (WP_070374321.1) QueC (WP_070374322.1) DpdD (WP_083277897.1) DpdK (WP_070374324.1) DpdJ (WP_070374325.1) DpdI (WP_070374326.1) DpdH (WP_070374327.1) DpdG (WP_070374328.1) DpdF (WP_070374329.1) DpdE (WP_070374330.1) QueE (WP_197517659.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

::info
This section is empty
::