---
title: BstA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2021.09.002
      abstract: |
        Temperate phages are pervasive in bacterial genomes, existing as vertically inherited islands termed prophages. Prophages are vulnerable to predation of their host bacterium by exogenous phages. Here, we identify BstA, a family of prophage-encoded phage-defense proteins in diverse Gram-negative bacteria. BstA localizes to sites of exogenous phage DNA replication and mediates abortive infection, suppressing the competing phage epidemic. During lytic replication, the BstA-encoding prophage is not itself inhibited by BstA due to self-immunity conferred by the anti-BstA (aba) element, a short stretch of DNA within the bstA locus. Inhibition of phage replication by distinct BstA proteins from Salmonella, Klebsiella, and Escherichia prophages is generally interchangeable, but each possesses a cognate aba element. The specificity of the aba element ensures that immunity is exclusive to the replicating prophage, preventing exploitation by variant BstA-encoding phages. The BstA protein allows prophages to defend host cells against exogenous phage attack without sacrificing the ability to replicate lytically.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: ""
contributors: 
    - Rachel Lavenir
relevantAbstracts:
 - doi: 10.1016/j.chom.2021.09.002
---

# BstA
## Description

BstA is a family of defense systems. BtsA systems from *Salmonella enterica subsp. enterica*, *Klebsiella pneumoniae* and *Escherichia coli* have been shown to provide resistance against a large diversity of phages when expressed in a *S. enterica* or *E.coli* host :ref{doi=10.1016/j.chom.2021.09.002}.

The majority of BstA systems appear to be prophage-encoded, as 79% of BstA homologs found in a set of Gram-negative bacterial genomes were associted with phage genes :ref{doi=10.1016/j.chom.2021.09.002}.

Interestingly, part of the BstA locus appears to encode an anti-BstA genetic element (*aba*), which prevents auto-immunity for prophages encoding the BstA locus. The aba element appears to be specific to a given BstA locus, as replacing the aba element from a BstA locus with the aba element from another BstA system does not prevent auto-immunity :ref{doi=10.1016/j.chom.2021.09.002}. 

## Molecular mechanism

The defense mechanism encoded by BstA remains to be elucidated. Experimental observation suggests that BtsA could act through an abortive infection mechanism. Fluorescence microscopy experiments suggest that the BstA protein colocalizes with phage DNA. The BstA protein appears to inhibit phage DNA replication during lytic phage infection cycles :ref{doi=10.1016/j.chom.2021.09.002}.

## Example of genomic structure

The BstA is composed of 1 protein: BstA.

Here is an example found in the RefSeq database:

![bsta](/bsta/BstA.svg){max-width=750px}

The BstA system in *Vibrio harveyi* (GCF_021397735.1, NZ_CP090179) is composed of 1 protein: BstA (WP_038899043.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_0
    Origin_0[Salmonella Typhimurium's BTP1 prophage 
<a href='https://ncbi.nlm.nih.gov/protein/CBG23363.1'>CBG23363.1</a>] --> Expressed_0[Salmonella Typhimurium]
    Expressed_0[Salmonella Typhimurium] ----> P22 & ES18 & 9NA
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_0
    Origin_0[Salmonella Typhimurium's BTP1 prophage 
<a href='https://ncbi.nlm.nih.gov/protein/CBG23363.1'>CBG23363.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> Lambda & Phi80 & P1vir & T7
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_1
    Origin_1[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/CDO13226.1'>CDO13226.1</a>] --> Expressed_1[Salmonella Typhimurium]
    Expressed_1[Salmonella Typhimurium] ----> BTP1 & P22 & ES18 & P22HT & 9NA & FelixO1
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_2
    Origin_2[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000026554.1'>WP_000026554.1</a>] --> Expressed_2[Salmonella Typhimurium]
    Expressed_2[Salmonella Typhimurium] ----> BTP1 & P22 & ES18 & P22HT & 9NA
    subgraph Title1[Reference]
        Owen_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_3
        Expressed_1
        Expressed_2
end
    subgraph Title4[Protects against]
        P22
        ES18
        9NA
        Lambda
        Phi80
        P1vir
        T7
        BTP1
        P22
        ES18
        P22HT
        9NA
        FelixO1
        BTP1
        P22
        ES18
        P22HT
        9NA
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

