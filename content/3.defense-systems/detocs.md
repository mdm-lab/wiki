---
title: Detocs
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2023.07.020
      abstract: |
        During viral infection, cells can deploy immune strategies that deprive viruses of molecules essential for their replication. Here, we report a family of immune effectors in bacteria that, upon phage infection, degrade cellular adenosine triphosphate (ATP) and deoxyadenosine triphosphate (dATP) by cleaving the N-glycosidic bond between the adenine and sugar moieties. These ATP nucleosidase effectors are widely distributed within multiple bacterial defense systems, including cyclic oligonucleotide-based antiviral signaling systems (CBASS), prokaryotic argonautes, and nucleotide-binding leucine-rich repeat (NLR)-like proteins, and we show that ATP and dATP degradation during infection halts phage propagation. By analyzing homologs of the immune ATP nucleosidase domain, we discover and characterize Detocs, a family of bacterial defense systems with a two-component phosphotransfer-signaling architecture. The immune ATP nucleosidase domain is also encoded within diverse eukaryotic proteins with immune-like architectures, and we show biochemically that eukaryotic homologs preserve the ATP nucleosidase activity. Our findings suggest that ATP and dATP degradation is a cell-autonomous innate immune strategy conserved across the tree of life.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleotide modifying
    PFAM: PF01048, PF18742
contributors: 
    - François Rousset
    - Nathalie Bechon
relevantAbstracts: 
    - doi: 10.1016/j.cell.2023.07.020

---


# Detocs

## Description
Detocs (**De**fensive **T**w**o**-**C**omponent **S**ystem) is a family of 3-gene defense systems. Upon phage recognition, Detocs degrades ATP, which can lead to premature phage lysis or abortive infection depending on the infecting phage, in a way that is currently not fully understood. Detocs shares homology with the bacterial two-component system, a well-known bacterial gene regulation system composed of an environment sensor and a cytosolic response regulator mediating gene expression.

## Molecular mechanism

Detocs is a family of 3-gene systems that resembles bacterial two-component systems. Two-component systems are common prokaryotic gene regulation modules made of two components, a sensor kinase and a response regulator. The sensor typically senses an environmental signal through its N-terminal domain, leading to the autophosphorylation of a conserved histidine in its kinase C-terminal domain. This phosphate group is then transferred to the N-terminal receiver domain of the response regulator, which activates the response regulator's C-terminal domain, usually a DNA binding domain involved in gene regulation. This allows bacteria to modify gene expression based on environmental cues.

Detocs DtcA resembles an intracellular sensor kinase. Its N-terminal end comprises tetratricopeptide repeats that are usually involved in protein/protein interactions and are believed to be responsible for sensing phage infection, while its C-terminal end possesses a kinase domain. Detocs DtcC resembles a response regulator. It has an N-terminal receiver domain, and its C-terminal is variable depending on the systems and it always contains a predicted effector domain (PNP, nuclease, transmembrane, hydrolase...). Unlike a two-component system, Detocs encodes an additional third protein, DtcB, with a standalone receiver domain that is not linked to any effector domain. A point mutation in the receiving aspartate of DtcB is toxic, while overexpression of DtcB impairs the defense capacity of Detocs. Therefore, DtcB likely serves as a “buffer” protein that absorbs phosphate signals that result from inadvertent leaky activation of DtcA in the absence of phage infection, thus preventing autoimmunity.

The best-described Detocs system uses a PNP effector, which was shown to specifically cleave ATP molecules into adenine and ribose-5’-triphosphate, both in vitro and during phage infection. Detocs activity leads to a drastic reduction in ATP and dATP levels during infection and to an accumulation of adenine. In parallel, ADP, AMP, dADP and dAMP levels are also reduced, likely in an indirect manner. Detocs induces growth arrest of T5-infected cells, but not of SECphi27-infected cells, suggesting that the outcome of infection following ATP degradation is phage-specific. The exact way in which this leads to defense against phages is not yet clear, but is believed to be a form of abortive infection. While PNP effectors represent 80% of Detocs operons, other cell-killing effectors can be found in a minority of Detocs systems. A Detocs operon with a transmembrane α/β hydrolase effector from *Enterobacter cloacae* JD6301 was able to efficiently protect *E. coli* against diverse phages :ref{doi=10.1016/j.cell.2023.07.020}.

## Example of genomic structure

A total of 4 subsystems have been described for the Detocs system.

Here are some examples found in the RefSeq database:

![detocs](/detocs/Detocs.svg){max-width=750px}

The Detocs system in *Vibrio anguillarum* (GCF_002287545.1, NZ_CP023054) is composed of 3 proteins dtcC (WP_019283384.1) dtcB (WP_019283385.1) dtcA (WP_198303352.1) 

![detocs_rease](/detocs/Detocs_REase.svg){max-width=750px}

The Detocs_REase system in *Winogradskyella sp. HaHa_3_26* (GCF_019278425.1, NZ_CP058981) is composed of 3 proteins dtcA (WP_179313105.1) dtcB (WP_179313104.1) dtcC_REase (WP_179313103.1) 

![detocs_toprim](/detocs/Detocs_TOPRIM.svg){max-width=750px}

The Detocs_TOPRIM system in *Kaistella flava (ex Peng et al. 2021)* (GCF_015191005.1, NZ_CP040442) is composed of 3 proteins dtcA (WP_193813510.1) dtcB (WP_193813511.1) dtcC_TOPRIM (WP_193813512.1) 

![detocs_hydrolase](/detocs/Detocs_hydrolase.svg){max-width=750px}

The Detocs_hydrolase system in *Yersinia canariae* (GCF_009831415.1, NZ_CP043727) is composed of 3 proteins dtcA (WP_159677463.1) dtcB (WP_159677464.1) dtcC_hydrolase (WP_159677465.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Rousset_2023[<a href='https://doi.org/10.1016/j.cell.2023.07.020'>Rousset et al., 2023</a>] --> Origin_0
    Origin_0[Vibrio alginolyticus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2645761408'>2645761408</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2645761407'>2645761407</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2645761406'>2645761406</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & T6
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_1
    Origin_1[ Detocs Hydrolase
Enterobacter cloacae 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2540965173'>2540965173</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2540965172'>2540965172</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2540965171'>2540965171</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> SECPhi27 & T5 & SECPhi18 & SECPhi6 & T2 & T4 & T7
    subgraph Title1[Reference]
        Rousset_2023
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Protects against]
        T2
        T4
        T5
        T6
        SECPhi27
        T5
        SECPhi18
        SECPhi6
        T2
        T4
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


