---
title: DarTG
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01153-5
      abstract: |
        Toxin-antitoxin (TA) systems are broadly distributed, yet poorly conserved, genetic elements whose biological functions are unclear and controversial. Some TA systems may provide bacteria with immunity to infection by their ubiquitous viral predators, bacteriophages. To identify such TA systems, we searched bioinformatically for those frequently encoded near known phage defence genes in bacterial genomes. This search identified homologues of DarTG, a recently discovered family of TA systems whose biological functions and natural activating conditions were unclear. Representatives from two different subfamilies, DarTG1 and DarTG2, strongly protected E. coli MG1655 against different phages. We demonstrate that for each system, infection with either RB69 or T5 phage, respectively, triggers release of the DarT toxin, a DNA ADP-ribosyltransferase, that then modifies viral DNA and prevents replication, thereby blocking the production of mature virions. Further, we isolated phages that have evolved to overcome DarTG defence either through mutations to their DNA polymerase or to an anti-DarT factor, gp61.2, encoded by many T-even phages. Collectively, our results indicate that phage defence may be a common function for TA systems and reveal the mechanism by which DarTG systems inhibit phage infection.
    Sensor: Unknown
    Activator: Direct binding to ssDNA
    Effector: Nucleic acid degrading (ADP-ribosylation)
    PFAM: PF01661, PF14487
contributors: 
    - Ernest Mordret
relevantAbstracts:
    - doi: 10.1038/s41564-022-01153-5
    - doi: 10.1016/j.molcel.2016.11.014
    - doi: 10.1016/j.celrep.2020.01.014 
    - doi: 10.1038/s41586-021-03825-4
---

# DarTG

## Description

The DarTG defense system is a toxin-antitoxin (TA) system that provides defense against bacteriophages by ADP-ribosylating viral DNA, thereby preventing replication and the production of mature virions. This system consists of two subfamilies, DarTG1 and DarTG2, which protect against different phages. When infected by specific phages, the DarT toxin, a DNA ADP-ribosyltransferase, is released, modifying viral DNA and inhibiting replication.

## Molecular mechanism

DarT uses NAD+ to ADP-ribosylates thymidine on ssDNA, while DarG catalyzes the reverse reaction. ADP-ribosylation of ssDNA prevents DNA replication and triggers the cell's SOS response. While initially proposed to work on bacterial ssDNA as a TA system :ref{doi=10.1016/j.molcel.2016.11.014}, Leroux et al. :ref{doi=10.1038/s41564-022-01153-5} show that it mostly modifies viral DNA and therefore blocks viral replication and perturb the transcription of phage genes. They conclude that "DarTG does not ultimately kill the host cell as in a conventional Abi mechanism, but instead acts to thwart phage replication directly."

## Example of genomic structure

The DarTG is composed of 2 proteins: DarT and DarG.

Here is an example found in the RefSeq database:

![dartg](/dartg/DarTG.svg){max-width=750px}

The DarTG system in *Leptodesmis sichuanensis* (GCF_021379005.1, NZ_CP075171) is composed of 2 proteins DarT (WP_233744308.1) DarG (WP_233744309.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Leroux_2022[<a href='https://doi.org/10.1038/s41564-022-01153-5'>LeRoux et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli strain C7] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> RB69 & T5 & SECphi18 & Lust
    subgraph Title1[Reference]
        Leroux_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        RB69
        T5
        SECphi18
        Lust
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
::


