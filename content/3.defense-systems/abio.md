---
title: AbiO
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01443, PF09848
contributors:
    - Florian Tesson
relevantAbstracts:
  - doi: 10.1023/A:1002027321171
  - doi: 10.1016/j.mib.2005.06.006
  - doi: 10.3168/jds.S0022-0302(98)75713-3
---

# AbiO

## Description

AbiO is a single gene defense system discovered in 1998 :ref{doi=10.3168/jds.S0022-0302(98)75713-3}.

AbiO is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. Even though, their name corresponds to an abortive infection, their mechanism of action does not necessarily correspond to an abortive infection. AbiO is classified as a possible Abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.


This system is only composed of one single gene containing a helicase domain.

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown.

## Example of genomic structure

The AbiO is composed of 1 protein: AbiO.

Here is an example found in the RefSeq database:

![abio](/abio/AbiO.svg){max-width=750px}

The AbiO system in *Pantoea agglomerans* (GCF_004117135.1, NZ_CP034477) is composed of 1 protein: AbiO (WP_129063585.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Prevots_1998[<a href='https://doi.org/10.3168/jds.S0022-0302(98)75713-3'>Prevots et al., 1998</a>] --> Origin_0
    Origin_0[lactococcal plasmid  
<a href='https://ncbi.nlm.nih.gov/protein/I61427.1'>In the plasmid : I61427.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2
    subgraph Title1[Reference]
        Prevots_1998
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



