---
title: Gao_RL
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00176, PF00271, PF04465, PF04851, PF06634, PF12635, PF13091, PF13287, PF13290

contributors: 
  - Aude Bernheim
relevantAbstracts:
  - doi: 10.1126/science.aba0372
---

# Gao_RL

## Description

The Gao_RL system is composed of 4 proteins: RL_D, RL_C, RL_B and, RL_A. It bears similarity with restriction systems :ref{doi=10.1126/science.aba0372}

## Molecular Mechanism
As far as we are aware, the molecular mechanism is unknown.

## Example of genomic structure

The Gao_RL is composed of 4 proteins: RL_A, RL_B, RL_C and RL_D.

Here is an example found in the RefSeq database:

![gao_rl](/gao_rl/Gao_RL.svg){max-width=750px}

The Gao_RL system in *Methylocystis heyeri* (GCF_004802635.2, NZ_CP046053) is composed of 4 proteins RL_A (WP_136498122.1) RL_B (WP_136498123.1) RL_C (WP_154331773.1) RL_D (WP_136498124.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000860009.1'>WP_000860009.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001044652.1'>WP_001044652.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001207938.1'>WP_001207938.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000985714.1'>WP_000985714.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & Lambda & M13
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        P1
        Lambda
        M13
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

