---
title: Rst_Hydrolase-3Tm
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13242, PF13419
relevantAbstracts:
    - doi: 10.1016/j.chom.2022.02.018
---

# Rst_Hydrolase-3Tm
## Example of genomic structure

The Rst_Hydrolase-3Tm is composed of 2 proteins: Hydrolase and Hydrolase-Tm.

Here is an example found in the RefSeq database:

![rst_hydrolase-tm](/rst_hydrolase-3tm/Rst_Hydrolase-Tm.svg){max-width=750px}

The Rst_Hydrolase-Tm system in *Pectobacterium versatile* (GCF_003031305.1, NZ_CP024842) is composed of 2 proteins Hydrolase (WP_107333258.1) Hydrolase-Tm (WP_107333259.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000754434.1'>WP_000754434.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001401335.1'>WP_001401335.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

