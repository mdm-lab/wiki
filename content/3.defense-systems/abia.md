---
title: AbiA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00078, PF18160, PF18732
contributors:
    - Florian Tesson
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1093/nar/gkac467
---

# AbiA 
The AbiA defense system was discovered in 1990 in a lactococcal plasmid :ref{doi=10.1128/aem.56.7.2255-2258.1990}.

AbiA is one of the so-called "Abi" systems for "Abortive infection" discovered in the 90's in research related to the dairy industry :ref{doi=10.1016/j.mib.2005.06.006}. AbiA is classified as a possible abortive infection in :ref{doi=10.1016/j.mib.2023.102312}.

Since it was discovered a similarity in amino acids was found with [AbiK](/defense-systems/abik). 

However, with the discovery of dozens of new systems, it was categorized as one of the UG/Abi defense systems :ref{doi=10.1093/nar/gkac467} along with [DRT](/defense-systems/drt) different subsystems, [AbiK](/defense-systems/abik), [AbiP2](/defense-systems/abip2) and [Rst_RT_Nitralase_TM](/defense-systems/rst_rt-nitrilase-tm).

Those systems are characterized by the presence of a reverse transcriptase domain of the "Unknown Group RT".

## Molecular mechanism

To our knowledge, the molecular mechanism is unknown. Similarly, for the other systems of this family, the molecular mechanism remains unknown.


## Example of genomic structure

A total of 2 subsystems have been described for the AbiA system.

Here is some examples found in the RefSeq database:

![abia_large](/abia/AbiA_large.svg){max-width=750px}

The AbiA_large system in *Staphylococcus nepalensis* (GCF_002442935.1, NZ_CP017466) is composed of 1 protein: AbiA_large (WP_096808013.1) 

![abia_small](/abia/AbiA_small.svg){max-width=750px}

The AbiA_small system in *Alicyclobacillus sp. SO9* (GCF_016406125.1, NZ_CP066339) is composed of 2 proteins AbiA_small (WP_198850075.1) AbiA_SLATT (WP_198850076.1) 


## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation


<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid  
<a href='https://ncbi.nlm.nih.gov/protein/AAA65072.1'>AAA65072.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

