---
title: AbiB
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors:
    - Nathalie Bechon
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1128/aem.57.12.3547-3551.1991
    - doi: 10.1046/j.1365-2958.1996.371896.x
---

# AbiB

## Description
AbiB is a single-protein abortive infection defense system from *Lactococcus* that degrades mRNA.

## Molecular mechanism
AbiB system is still poorly understood. It is a single-protein system that was described as an abortive infection system. Upon phage infection, AbiB activation leads to a strong degradation of mRNAs :ref{doi=10.1046/j.1365-2958.1996.371896.x} which is expected to be the mechanism of phage inhibition. AbiB expression is constitutive and does increase during phage infection. It is only activated during phage infection, most likely through the recognition of an early phage protein. Which protein, and whether this activation is direct or indirect remains to be elucidated.

## Example of genomic structure

The AbiB is composed of 1 protein: AbiB.

Here is an example found in the RefSeq database:

![abib](/abib/AbiB.svg){max-width=750px}

The AbiB system in *Lactococcus cremoris* (GCF_000312685.1, NC_019430) is composed of 1 protein: AbiB (WP_144019851.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAA25158.1'>AAA25158.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        936
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



