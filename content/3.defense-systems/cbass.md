---
title: CBASS
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-020-0777-y
      abstract: |
        Cyclic-oligonucleotide-based anti-phage signalling systems (CBASS) are a family of defence systems against bacteriophages (hereafter phages) that share ancestry with the cGAS-STING innate immune pathway in animals. CBASS systems are composed of an oligonucleotide cyclase, which generates signalling cyclic oligonucleotides in response to phage infection, and an effector that is activated by the cyclic oligonucleotides and promotes cell death. Cell death occurs before phage replication is completed, therefore preventing the spread of phages to nearby cells. Here, we analysed 38,000 bacterial and archaeal genomes and identified more than 5,000 CBASS systems, which have diverse architectures with multiple signalling molecules, effectors and ancillary genes. We propose a classification system for CBASS that groups systems according to their operon organization, signalling molecules and effector function. Four major CBASS types were identified, sharing at least six effector subtypes that promote cell death by membrane impairment, DNA degradation or other means. We observed evidence of extensive gain and loss of CBASS systems, as well as shuffling of effector genes between systems. We expect that our classification and nomenclature scheme will guide future research in the developing CBASS field.
    Sensor: Unknown
    Activator: Signaling molecules
    Effector: Divers (Nucleic acid degrading, Nucleotide modifying, Membrane disrupting)
    PFAM: PF00004, PF00027, PF00899, PF01048, PF01734, PF06508, PF10137, PF14461, PF14464, PF18134, PF18138, PF18144, PF18145, PF18153, PF18159, PF18167, PF18173, PF18178, PF18179, PF18186, PF18303, PF18967
relevantAbstracts:
    - doi: 10.1016/j.molcel.2019.12.009
    - doi: 10.1016/j.molcel.2021.10.020
    - doi: 10.1038/s41564-020-0777-y
    - doi: 10.1038/s41586-019-1605-5
    - doi: 10.1038/s41586-020-2719-5

---

# CBASS
## Example of genomic structure

A total of 4 subsystems have been described for the CBASS system.

Here are some examples found in the RefSeq database:

![cbass_i](/cbass/CBASS_I.svg){max-width=750px}

The CBASS_I system in *Vibrio fluvialis* (GCF_018140575.1, NZ_CP073273) is composed of 2 proteins Effector_4TM_S_4TM (WP_212571187.1) Cyclase_SMODS (WP_203534033.1) 

![cbass_ii](/cbass/CBASS_II.svg){max-width=750px}

The CBASS_II system in *Klebsiella oxytoca* (GCF_002072655.1, NZ_CP020358) is composed of 4 proteins Sensing_SAVED (WP_080528222.1) Jab (WP_071681958.1) AG_E1_ThiF (WP_080528223.1) Cyclase_II (WP_061351239.1) 

![cbass_iii](/cbass/CBASS_III.svg){max-width=750px}

The CBASS_III system in *Conexibacter sp. DBS9H8* (GCF_023330685.1, NZ_CP097128) is composed of 5 proteins Sensing_SAVED (WP_249010498.1) Cyclase_II (WP_249010499.1) bacHORMA_2 (WP_249010500.1) HORMA (WP_249010501.1) TRIP13 (WP_249010502.1) 

![cbass_iv](/cbass/CBASS_IV.svg){max-width=750px}

The CBASS_IV system in *Flavobacterium alkalisoli* (GCF_008000935.1, NZ_CP042831) is composed of 5 proteins OGG (WP_147582669.1) Cyclase_II (WP_147582670.1) TGT (WP_147582671.1) QueC (WP_147582672.1) Effector_2TM_Sa_NUDIX (WP_147582673.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Cohen_2019[<a href='https://doi.org/10.1038/s41586-019-1605-5'>Cohen et al., 2019</a>] --> Origin_0
    Origin_0[Vibrio cholerae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001901330.1'>WP_001901330.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001133548.1'>WP_001133548.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001884104.1'>WP_001884104.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_223225479.1'>WP_223225479.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & T2
    Cohen_2019[<a href='https://doi.org/10.1038/s41586-019-1605-5'>Cohen et al., 2019</a>] --> Origin_1
    Origin_1[Escherichia coli] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> P1 & T2 & T4 & T5 & T6 & LambdaVir
    Lowey_2020[<a href='https://doi.org/10.1016/j.cell.2020.05.019'>Lowey et al., 2020</a>] --> Origin_2
    Origin_2[Enterobacter cloacae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_032676399.1'>WP_032676399.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_032676400.1'>WP_032676400.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_050010101.1'>WP_050010101.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_157903655.1'>WP_157903655.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T7
    Huiting_2022[<a href='https://doi.org/10.1016/j.cell.2022.12.041'>Huiting et al., 2023</a>] --> Origin_3
    Origin_3[Pseudomonas aeruginosa] --> Expressed_3[Pseudomonas aeruginosa]
    Expressed_3[Pseudomonas aeruginosa] ----> PaMx41 & PaMx33 & PaMx35 & PaMx43
    subgraph Title1[Reference]
        Cohen_2019
        Lowey_2020
        Huiting_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Protects against]
        P1
        T2
        P1
        T2
        T4
        T5
        T6
        LambdaVir
        T2
        T7
        PaMx41
        PaMx33
        PaMx35
        PaMx43
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

