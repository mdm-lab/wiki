---
title: PD-Lambda-1
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF10544, PF13250, PF13455
contributors:
    - Alba Herrero del Valle
relevantAbstracts:
    - doi: 10.1038/s41564-022-01219-4

---

# PD-Lambda-1

## Description

The PD-Lambda-1 system is composed of one protein, PD-Lambda-1, and confers resistance to LambdaVir :ref{doi=10.1038/s41564-022-01219-4}.

## Molecular mechanism

As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The PD-Lambda-1 is composed of 1 protein: PD-Lambda-1.

Here is an example found in the RefSeq database:

![pd-lambda-1](/pd-lambda-1/PD-Lambda-1.svg){max-width=750px}

The PD-Lambda-1 system in *Gemmata massiliana* (GCF_901538265.1, NZ_LR593886) is composed of 1 protein: PD-Lambda-1 (WP_197909633.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCP76574.1'>RCP76574.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        LambdaVir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



