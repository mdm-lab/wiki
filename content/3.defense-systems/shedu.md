---
title: Shedu
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF14082
contributors: 
  - Aude Bernheim
relevantAbstracts:
  - doi: 10.1126/science.aar4120
  - doi: 10.1101/2023.08.10.552762
  - doi: 10.1101/2023.08.10.552793
---

# Shedu

## Description
The Shedu antiphage system consists of a single protein, SduA, which acts as a nuclease with a conserved DUF4263 domain belonging to the PD-(D/E)XK nuclease superfamily. The system was named after an Assyrian Mythical Deity. The N-terminal domain is very diverse including diverse nucleic acid binding, enzymatic, and other domains. 


## Molecular Mechanism
The Shedu protein is proposed to act as a nuclease, and its N-terminal domain inhibits its activation until triggered by phage infection.
The activation of the protein was described in :ref{doi=10.1101/2023.08.10.552793}.
In B. cereus Shedu, *"a key catalytic residue in Shedu’s nuclease domain is sequestered away from the catalytic site. Activation involves a conformational change that completes the active site and promotes assembly of a homo-octamer for coordinated double-strand DNA cleavage. Removal of Shedu’s N-terminal domain ectopically activates the enzyme, suggesting that this domain allosterically inhibits Shedu in the absence of infection."* 
The nuclease activity and specific sensing of an E. coli Shedu was described in :ref{doi=10.1101/2023.08.10.552762}
*"The N-terminal domains of SduA form a clamp that recognizes free DNA ends. End binding positions the DNA over the PD/ExK nuclease domain, resulting in dsDNA nicking at a fixed distance from the 5’ end. The end-directed DNA nicking activity of Shedu prevents propagation of linear DNA in vivo"*. In E. coli, T6 phages can escape Shedu immunity by suppressing their recombination-dependent DNA replication pathway.


## Example of genomic structure

The Shedu is composed of 1 protein: SduA.

Here is an example found in the RefSeq database:

![shedu](/shedu/Shedu.svg){max-width=750px}

The Shedu system in *Bradyrhizobium ottawaense* (GCF_002278135.2, NZ_CP029425) is composed of 1 protein: SduA (WP_225004618.1) 

## Distribution of the system among prokaryotes

::article-system-distribution-plot
::

## Structure

::article-structure
::

## Experimental validation

<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/ACK61957.1'>ACK61957.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> phi105 & rho14 & SPP1 & phi29
    subgraph Title1[Reference]
        Doron_2018
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Protects against]
        phi105
        rho14
        SPP1
        phi29
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


