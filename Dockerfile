### Stage Install ###
FROM node:21.1-bookworm-slim as install



WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package*.json ./

RUN npm install


### STAGE: Dev
FROM node:21.1-bookworm-slim as dev
ARG BASE_URL=/
ARG MEILI_HOST=http://localhost:7700
ARG MEILI_API_KEY=api_key
ARG HOST_URL
ENV NUXT_APP_BASE_URL=${BASE_URL}
# nuxt module
ENV NUXT_PUBLIC_MEILISEARCH_CLIENT_HOST_URL=${MEILI_HOST}
ENV NUXT_PUBLIC_MEILISEARCH_CLIENT_SEARCH_API_KEY=${MEILI_API_KEY}
ENV NUXT_PUBLIC_HOST_URL=${HOST_URL}
# ENV NUXT_PUBLIC_MEILI_HOST=${MEILI_HOST}
# ENV NUXT_PUBLIC_MEILI_API_KEY=${MEILI_API_KEY}


WORKDIR /usr/src/app
COPY --from=install /usr/src/app ./
COPY . /usr/src/app

EXPOSE 3000 24678 4000
CMD ["npm", "run", "dev"]


### STAGE: Build ###
FROM node:21.1-bookworm-slim as build

WORKDIR /usr/src/app
COPY --from=install /usr/src/app ./
COPY . /usr/src/app
RUN npm run build

### STAGE: serve ###
FROM node:21.1-bookworm-slim as serve
ARG BASE_URL=/
ARG MEILI_HOST=http://localhost:7700
ARG MEILI_API_KEY
ARG HOST_URL

ENV NUXT_APP_BASE_URL=${BASE_URL}

# nuxt module
ENV NUXT_PUBLIC_MEILISEARCH_CLIENT_HOST_URL=${MEILI_HOST}
ENV NUXT_PUBLIC_MEILISEARCH_CLIENT_SEARCH_API_KEY=${MEILI_API_KEY}
ENV NUXT_PUBLIC_HOST_URL=${HOST_URL}

WORKDIR /usr/src/app
COPY  --chown=node:node --from=build /usr/src/app/.output ./
USER node
CMD [ "node", "server/index.mjs"]


### STAGE: Generate ###
FROM node:21.1-bookworm-slim as generate
ARG BASE_URL=/
ARG MEILI_HOST=http://localhost:7700
ARG MEILI_API_KEY
ARG HOST_URL
ARG SHORT_SHA

ENV NODE_OPTIONS=--max_old_space_size=12288
ENV NUXT_APP_BASE_URL=${BASE_URL}
ENV NUXT_PUBLIC_SHORT_SHA=${SHORT_SHA}
# nuxt module
ENV NUXT_PUBLIC_MEILISEARCH_CLIENT_HOST_URL=${MEILI_HOST}
ENV NUXT_PUBLIC_MEILISEARCH_CLIENT_SEARCH_API_KEY=${MEILI_API_KEY}
ENV NUXT_PUBLIC_HOST_URL=${HOST_URL}


# ENV NUXT_PUBLIC_MEILI_HOST=${MEILI_HOST}
# ENV NUXT_PUBLIC_MEILI_API_KEY=${MEILI_API_KEY}


WORKDIR /usr/src/app
COPY --from=install /usr/src/app ./
COPY . /usr/src/app

RUN npm run generate

### STAGE: NGINX ###
FROM nginx:1.25-bookworm
# RUN rm -rf /usr/share/nginx/html/*
RUN apt update -y && apt install rsync -y
RUN mkdir /public-website
RUN chown nginx:nginx /public-website
COPY nginx.conf /etc/nginx/nginx.conf
RUN chown nginx:nginx /usr/share/nginx/html
COPY --chown=nginx:nginx --from=generate /usr/src/app/.output/public /usr/share/nginx/html

# RUN chmod -R nginx:nginx /usr/share/nginx/html/
USER nginx
COPY --chown=nginx:nginx --from=generate /usr/src/app/.output/public /public-website
CMD ["nginx", "-g", "daemon off;"]